﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Relatorios
{
    public class RelatorioDisponibilidade
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Senioridade { get; set; }
        public string DataInicioContrato { get; set; }
        public string DataTerminoContrato { get; set; }
        public string TipoContratacao { get; set; }
        public string Especialidade { get; set; }
        public string Empresa { get; set; }
        public Guid EmpresaId { get; set; }
        public string Gestor { get; set; }
        public string Email { get; set; }
        public int Ativo { get; set; }
        public List<AgendaProgramada> Agenda {get;set;}
        public int HorasDisponiveis { get; set ; }
        public int HorasNaoDisponiveis { get; set; }
    }

    public class AgendaProgramada
    {
        public Guid Id { get; set; }
        public StatusAgenda StatusAgenda { get;set;}
        public DateTime Dia { get; set; }
        public int Periodo { get; set; }
        public string DescricaoAtividade { get; set; }        
    }

    public class DiasPeriodo
    {
        public DateTime Dia { get; set; }
        public StatusDia StatusDia { get; set; }
    }
    public enum StatusAgenda
    {
        Livre = 1,
        Ocupada =2
    }

    public enum StatusDia
    {
        DiaUtil = 1,
        FimDeSemana = 2,
        Feriado =3
    }
}

    

