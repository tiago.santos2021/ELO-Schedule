﻿namespace Schedule.Application.ViewModel.Auth
{
    public class RedefinicaoSenhaViewModel
    {
        public string Token { get; set; }
        public string NovaSenha { get; set; }
    }
}
