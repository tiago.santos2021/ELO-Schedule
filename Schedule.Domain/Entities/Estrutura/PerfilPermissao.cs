﻿using Schedule.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities.Estrutura
{
  public  class PerfilPermissao : EntidadeBase
    {

        public Guid Perfilid { get; set; }
        public Guid PermissaoId { get; set; }
    }
}
