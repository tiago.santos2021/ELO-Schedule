﻿using Schedule.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities.Estrutura
{
    public class PerfilMenu : EntidadeBase
    {

        public Guid Perfilid { get; set; }
        public Guid MenuId { get; set; }

    }
}
