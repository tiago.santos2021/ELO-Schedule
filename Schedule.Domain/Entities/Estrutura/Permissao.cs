﻿using Schedule.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities.Estrutura
{
   public class Permissao : EntidadeBase
    {
        public string Tipo { get; set; }
        public string Descricao { get; set; }


    }
}
