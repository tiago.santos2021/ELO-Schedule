﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities.Estrutura
{
    public class UsuarioDetalhes
    {
        public string Empresa { get; set; }
        public string Login { get; set; }
        public string Nome { get; set; }
        public string Perfil { get; set; }
        public string Status { get; set; }
        public string Especialidade { get; set; }
        public string Senioridade { get; set; }
        public string Contratacao { get; set; }
        public string DataInicioContrato { get; set; }
        public string DataTerminoContrato { get; set; }
        public string Observacao { get; set; }
       


        
    }
}
