﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities.Base
{
    public abstract class EntidadeBase 
    {
        protected EntidadeBase()
        {
            Id = Guid.NewGuid();
        }
       

        public Guid Id { get;  set; }
        public string Usuario { get;  set; }
        public DateTime DataAlteracao { get;  set; }
    }
}
