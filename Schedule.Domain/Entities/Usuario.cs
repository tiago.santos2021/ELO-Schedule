﻿using Schedule.Domain.Entities.Base;
using System;

namespace Schedule.Domain.Entities
{
    public class Usuario : EntidadeBase
    {
       
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Email { get; set; }
        public string UF { get; set; }
        public string Telefone1 { get; set; }
        public string DDD1 { get; set; }
        public string Telefone2 { get; set; }
        public string DDD2 { get; set; }
        public string Observacao { get; set; }
        public Guid PerfilId { get; set; }
        public DateTime DataInicioContrato { get; set; }
        public DateTime? DataTerminoContrato { get; set; }
        public Guid? EmpresaId { get; set; }
        public Guid? SenioridadeId { get; set; }
        public Guid? GestorImediatoId { get; set; }
        public Guid? TipoContratacaoId { get; set; }
        public decimal? ValorHora { get; set; }
        public decimal? ValorMensal { get; set; }
        public bool Ativo { get; set; }
    }
}
