﻿using Schedule.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities
{
  public  class CustoMedio : EntidadeBase
    {

        public Guid EspecialidadeId { get; set; }
        public decimal Valor { get; set; }

    }
}
