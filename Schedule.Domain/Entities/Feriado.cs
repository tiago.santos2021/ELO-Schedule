﻿using Schedule.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities
{
    public class Feriado : EntidadeBase
    {
        public Guid Id { get; set; }
        public string Descricao { get; set; }
        public DateTime DataFeriado { get; set; }
        public string Usuario { get; set; }
        public DateTime DataAlteracao { get; set; }
    }
}
