﻿using Schedule.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities
{
  public  class TipoContratacao : EntidadeBase
    {
        public string Codigo { get; set; }
        public string Nome { get; set; }
    }
}
