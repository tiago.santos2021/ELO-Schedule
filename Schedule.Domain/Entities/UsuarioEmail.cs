﻿namespace Schedule.Domain.Entities
{
    public class UsuarioEmail
    {
        public string Nome { get; set; }
        public string Usuario { get; set; }
        public string Email { get; set; }
        public bool Ativo { get; set; }
    }
}
