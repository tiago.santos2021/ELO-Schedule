﻿using Schedule.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities
{
   public class CentroCusto : EntidadeBase
    {

        public Guid EmpresaId { get; set; }
        public string Nome { get; set; }
        public decimal Taxa { get; set; }
        public Guid ResponsavelId { get; set; }
        public Guid ClienteId { get; set; }
        public Guid TipoCentroCustoId { get; set; }
        public DateTime DataInicioContrato { get; set; }
        public DateTime DataFinalContrato { get; set; }
        public bool Ativo { get; set; }




    }
}
