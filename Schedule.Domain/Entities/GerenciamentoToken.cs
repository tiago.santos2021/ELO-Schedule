﻿using Schedule.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities
{
    public class GerenciamentoToken: EntidadeBase
    {
        public string Token { get; set; }
        public DateTime DataSolicitacao { get; set; }
        public string Login { get; set; }
        public bool Utilizado { get; set; }
        public DateTime DataValidade { get; set; }
    }
}
