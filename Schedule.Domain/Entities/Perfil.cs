﻿using Schedule.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities
{
   public class Perfil : EntidadeBase
    {

        public string Grupo { get; set; }
        public string Nome { get; set; }

    }
}
