﻿using Schedule.Domain.Entities.Base;
using Schedule.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities
{
  public  class Agenda : EntidadeBase
    {
        public Guid UsuarioId { get; set; }
        public string FuncionarioNome { get; set; }
        public Guid? CentroCustoId { get; set; }
        public string CentroCustoDescricao { get; set; }
        public Guid EmpresaId { get; set; }
        public string EmpresaDescricao { get; set; }
        public DateTime DataAgenda { get; set; }
        public int TipoAgenda { get; set; }
        public int Periodo { get; set; }
        public  int ? Feriado { get; set; }
    }
}
