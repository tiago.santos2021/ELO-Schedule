﻿using Schedule.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities
{
    public class TemplateEmail: EntidadeBase
    {
        public int Tipo { get; set; }
        public string Descricao { get; set; }
        public string Html { get; set; }
    }
}
