﻿namespace Schedule.Domain.Enum
{
    public enum TipoEmailEnum
    {
        RedefinicaoSenha = 1,
        ConflitoAlocacao = 2
    }
}
