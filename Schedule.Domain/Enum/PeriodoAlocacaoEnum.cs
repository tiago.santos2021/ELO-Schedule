﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Enum
{
    public enum PeriodoAlocacaoEnum
    {
        Manha = 1,
        Tarde = 2
    }
}
