﻿using Schedule.Domain.Entities;
using Schedule.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schedule.Domain.Interfaces
{
    public interface IAlocacaoRepository
    {
        void Incluir(Agenda entidade);
        IQueryable<Agenda> Listar();
        Tuple<string, string> ValidarConflitos(Guid usuarioId, DateTime dataAgenda, int periodo, string usuarioLogado);
        void Deletar(Agenda agenda);
        string BuscarTemplateEmail(TipoEmailEnum tipoEmail);
        public List<Agenda> PesquisaAlocacao(DateTime dataInicio, DateTime dataFinal, string consultores, string centroCusto, bool IncluirReservaFeriado);

        public List<Usuario> ListaConsultoresComConflitos(DateTime DataInicial, DateTime DataFinal);
        public List<Agenda> ListaConflitosPorConsultor(Guid ConsultorId, DateTime DataInicial, DateTime DataFinal);

        public bool ExcluirConflitos(List<Guid> ListaExclusao);
    }
}
