﻿using Schedule.Domain.Entities;
using Schedule.Domain.Relatorios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Interfaces.Repository.Relatorio
{
    public interface IRelatorioRepository
    {
        public List<VisaoRelatorioPlanejado> RetornaRelatorioPlanejado(DateTime dataInicio,DateTime dataFinal, string consultores, string centroCusto);
        public List<RelatorioDisponibilidade> RelatorioColaboradores(DateTime dataInicio, DateTime dataFinal, string nome);
        public List<RelatorioDisponibilidade> RelatorioColaboradores(DateTime dataInicio, DateTime dataFinal, string[] idConsultores = null, string[] idEspecialidades = null);
        public List<Feriado> ConsultarFeriados(DateTime dataInicio, DateTime dataFinal);
        public List<AgendaProgramada> RelatorioAgendasColaboradores(DateTime dataInicio, DateTime dataFinal, string nome);
        public List<AgendaProgramada> RelatorioAgendasColaboradores(DateTime dataInicio, DateTime dataFinal, string[] idConsultores);
    }
}