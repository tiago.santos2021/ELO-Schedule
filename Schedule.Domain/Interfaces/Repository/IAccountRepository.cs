﻿using Schedule.Domain.Entities;
using Schedule.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Interfaces.Repository
{
    public interface IAccountRepository
    {

        UsuarioEmail BuscarEmailUsuario(string login);
        string BuscarTemplateEmail(TipoEmailEnum tipoEmail);
        void AdicionarToken(GerenciamentoToken entidade);
        GerenciamentoToken ValidarTokenRedefinicaoSenha(string token);
        void AtualizarTokenUsado(string token);
        void EditarSenha(string login, string senha);

    }
}
