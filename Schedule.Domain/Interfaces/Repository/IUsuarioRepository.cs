﻿using Schedule.Domain.Entities;
using Schedule.Domain.Enum;
using Schedule.Domain.Interfaces.Repository.Base;
using System;
using System.Collections.Generic;

namespace Schedule.Domain.Interfaces.Repository
{
    public interface IUsuarioRepository : IRepositoryBase<Usuario, Guid>
    {

        void Adicionar(Usuario entidade, List<Guid> ListaEspecialidades);
        void Editar(Usuario entidade, List<Guid> ListaEspecialidades);
        public void DesativarUsuario(Guid Id, string usuarioAlteracao);
        List<object> ListaUsuarioDetalhes();
    }
}
