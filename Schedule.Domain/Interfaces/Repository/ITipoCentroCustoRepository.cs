﻿using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Interfaces.Repository
{
   public interface ITipoCentroCustoRepository : IRepositoryBase<TipoCentroCusto, Guid>
    {
    }
}
