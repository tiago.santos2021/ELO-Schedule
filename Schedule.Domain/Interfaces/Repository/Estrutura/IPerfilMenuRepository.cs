﻿using Schedule.Domain.Entities.Estrutura;
using Schedule.Domain.Interfaces.Repository.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Interfaces.Repository.Estrutura
{
   public interface IPerfilMenuRepository : IRepositoryBase<PerfilMenu, Guid>
    {
    }
}
