﻿using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Interfaces.Repository
{
   public interface ICentroDeCustoRepository : IRepositoryBase<CentroCusto, Guid>
    {
        public void Desativar(Guid Id, string usuarioAlteracao);
    }
}
