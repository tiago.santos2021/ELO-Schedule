using NUnit.Framework;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using Schedule.Util.GeradorExcel;
using Schedule.Domain.Entities.Estrutura;
using System.Collections.Generic;

namespace Testes
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TesteGeracaoPlanilha()
        {
            List<UsuarioDetalhes> usuarios = new List<UsuarioDetalhes>()
            {
                new UsuarioDetalhes(){ Nome = "Zezinho" },
                new UsuarioDetalhes(){ Nome = "Huguinho" },
                new UsuarioDetalhes(){ Nome = "Bernadinho" }
            };

            Schedule.Util.GeradorExcel.GeradorExcel.RelatorioUsuarios<UsuarioDetalhes>(usuarios, "Planilha de Usu�rios");
        }
    }
}