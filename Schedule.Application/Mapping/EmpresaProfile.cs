﻿using AutoMapper;

using Schedule.Application.ViewModel;
using Schedule.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.Mapping
{
   public class EmpresaProfile : Profile
    {
        public EmpresaProfile()
        {
            CreateMap<Empresa, EmpresaViewModel>().ReverseMap();
                
        }




    }
}
