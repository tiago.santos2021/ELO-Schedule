﻿using Schedule.Application.ViewModel;
using System;
using System.Collections.Generic;

namespace Schedule.Application.Interfaces.Service
{
    public interface IUsuarioService : IServiceBase<UsuarioViewModel>   
    {

        List<UsuarioViewModel> ListarGestores(List<Guid> listaFiltroPerfil);
        public List<UsuarioViewModel> ListarConsultores(List<Guid> listaTiposConsultores);
        public List<UsuarioViewModel> ListarConsultoresAtivos(List<Guid> listaTiposConsultores);
        void Adicionar(UsuarioViewModel entidade,bool TipoConsultor);
        void Alterar(UsuarioViewModel entidade, bool TipoConsultor);        
        void DesativarUsuario(Guid Id, string usuario);
        List<UsuarioViewModel> ListaUsuarioDetalhes();
    }
}
