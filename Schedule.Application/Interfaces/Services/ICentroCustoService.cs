﻿using Schedule.Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.Interfaces.Service
{
    public interface ICentroCustoService :IServiceBase<CentroCustoViewModel>
    {
        public void Desativar(Guid Id, string usuario);
    }
}
