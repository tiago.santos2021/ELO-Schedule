﻿using Schedule.Application.ViewModel;
using Schedule.Application.ViewModel.Agenda;
using Schedule.Application.ViewModel.Agenda.Conflito;
using System;
using System.Collections.Generic;

namespace Schedule.Application.Interfaces.Services
{
    public interface IAlocacaoService
    {
        Tuple<bool, string> Incluir(AlocacaoViewModel entidade);
        Tuple<bool, string> Deletar(AlocacaoViewModel entidade);

        void ValidarInclusao(AlocacaoViewModel entidade);
        void ValidarExclusao(AlocacaoViewModel entidade);
        PlanejamentoAlocacaoViewModel PesquisarAlocacao(AgendaPesquisaViewModel parametroAlocacao);

        public List<UsuarioViewModel> ListaConsultoresComConflitosPorPeriodo(DateTime DataInicial, DateTime DataFinal);
        public RespostaPesquisaConflitoViewModel ListaConsultoresConflitos(PesquisaConflitoViewModel parametroPesquisa);

        public GestaoConflitoViewModel ListaConflitosPorConsultor(Guid ConsultorId, DateTime DataInicial, DateTime DataFinal);
        public bool ExcluirConflitos(List<Guid> ListaExclusao);
    }
}