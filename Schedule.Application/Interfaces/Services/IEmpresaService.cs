﻿
using Schedule.Application.ViewModel;
using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.Interfaces.Service
{
    public interface IEmpresaService : IServiceBase<EmpresaViewModel>
    {

    }
}
