﻿using Schedule.Application.ViewModel;
using Schedule.Application.ViewModel.Auth;

using System;

namespace Schedule.Application.Interfaces.Service
{
    public interface IAccountService
    {
        UsuarioViewModel GetUsuario(AuthViewModel model);
        Tuple<bool, string> EnviarEmailRedefinicaoSenha(string login, string urlRedefinicaoSenha);
        Tuple<bool, string> EditarSenha(string token, string novaSenha);
    }
}
