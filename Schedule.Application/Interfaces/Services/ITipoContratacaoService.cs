﻿using Schedule.Application.ViewModel;
using Schedule.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.Interfaces.Service
{
   public interface ITipoContratacaoService : IServiceBase<TipoContratacaoViewModel>
    {
    }
}
