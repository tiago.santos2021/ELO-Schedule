﻿using Schedule.Domain.Relatorios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.Interfaces.Services
{
    public interface IRelatorioDisponibilidadeService
    {
        public List<RelatorioDisponibilidade> RelatorioDisponibilidade(DateTime dataInicio, DateTime dataFinal, string[] idConsultores = null, string[] idEspecialidades = null, List<Guid> idEmpresas  = null);

        public List<RelatorioDisponibilidade> RelatorioDisponibilidade(DateTime dataInicio, DateTime dataFinal, string nome, 
            List<RelatorioDisponibilidade> listaColaboradores,
            List<AgendaProgramada> listaAgendaProgramada,
            List<AgendaProgramada> listaAgendaVazia);
        public List<DateTime> ConsultaDiasUteis(DateTime dataInicio, DateTime dataFinal);
        public List<DiasPeriodo> ConsultarDiasPeriodo(DateTime dataInicial, DateTime dataFinal);
    }
}
