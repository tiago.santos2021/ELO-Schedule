﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.Interfaces.Service
{
   public interface IServiceBase <TEntidade> where TEntidade : class
    {
        void Adicionar(TEntidade entidade);
        void Alterar(TEntidade entidade);
        void Excluir(Guid id);
        List<TEntidade> Listar();
        TEntidade RetornaPorId(Guid id);

        bool VerificaSeRegistroExisteNaBase(Guid id);
    }
}
