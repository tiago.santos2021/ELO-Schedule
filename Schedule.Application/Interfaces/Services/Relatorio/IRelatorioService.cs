﻿using Schedule.Application.ViewModel.Relatorio;
using Schedule.Application.ViewModel.Visao;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.Interfaces.Services.Relatorio
{
  public  interface IRelatorioService
    {

        RespostaSolicitacaoImpressaoPlanejadoViewModel RetornaRelatorioPlanejado(SolicitacaoImpressaoRelPlanejadoViewModel data);
    
    
    }
}
