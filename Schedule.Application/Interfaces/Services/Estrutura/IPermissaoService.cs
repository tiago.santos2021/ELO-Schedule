﻿using Schedule.Application.ViewModel.Estrutura;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.Interfaces.Services.Estrutura
{
   public interface IPermissaoService
    {
        List<PermissaoViewModel> RetornaListaDePermissoesPorPerfil(Guid PerfilId);
    }
}
