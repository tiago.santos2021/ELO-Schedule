﻿using Schedule.Application.ViewModel.Estrutura;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.Interfaces.Services.Estrutura
{
    public interface IPerfilMenuServico
    {

        List<PerfilMenuViewModel> RetornaListaPorPerfil(Guid PerfilId);

    }
}
