﻿using Schedule.Application.Resorces;
using System;
using System.Collections.Generic;
using System.Resources;
using System.Text;

namespace Schedule.Application.Resouces
{
   public static class ScheduleResorce
    {
        public static string RetornaValor(string Chave)
        {
            ResourceManager RM = new ResourceManager(typeof(Resource));
            var Valor = RM.GetString(Chave);
            return Valor;
        }
    
    
    }
}
