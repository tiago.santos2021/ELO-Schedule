﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Schedule.Application.ViewModel
{
   public class PerfilViewModel :  BaseViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Grupo é Obrigatório")]
        [MaxLength(20, ErrorMessage = " quantidade máxima permitida é de 20 caracteres")]
        public string Grupo { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Descrição é Obrigatório")]
        [MaxLength(50, ErrorMessage = "Quantidade máxima permitida é de 100 caracteress")]
        public string Nome { get; set; }

    }
}
