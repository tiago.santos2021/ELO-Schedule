﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Schedule.Application.ViewModel
{
   public class CentroCustoViewModel :BaseViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Empresa é Obrigatório")]
        public Guid EmpresaId { get; set; }
        public string EmpresaNome { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Descrição é Obrigatório")]
        [MaxLength(30, ErrorMessage = "Quantidade máxima permitida é de 30 caracteres")]
        public string Nome { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Taxa é Obrigatório")]
        public decimal Taxa { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Responsável é Obrigatório")]
        public Guid ResponsavelId { get; set; }
        public string ResponsavelNome { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Cliente é Obrigatório")]
        public Guid ClienteId { get; set; }
        public string ClienteNome { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Tipo de Centro de Custo é Obrigatório")]
        public Guid TipoCentroCustoId { get; set; }
        public string TipoCentroCustoNome { get; set; }
        [Required( ErrorMessage = "O campo Data de Inicio de Contrato é Obrigatório")]

        public string DataInicioContrato { get; set; }
        [Required( ErrorMessage = "O campo Data de Término de Contrato é Obrigatório")]      
        public string DataFinalContrato { get; set; }


        public bool Ativo { get; set; }
        public string StatusDescricao { get { return Ativo == true ? "Ativo" : "Inativo"; } }
    }
}
