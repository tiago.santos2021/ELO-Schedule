﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Schedule.Application.ViewModel
{
   public class EspecialidadeViewModel   : BaseViewModel
    {

        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Unidade de Serviço é Obrigatório")]
        [MaxLength(40, ErrorMessage = " quantidade máxima permitida é de 40 caracteres")]
        public string UnidadeServico { get; set; }
    
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Competência é Obrigatório")]
        [MaxLength(40, ErrorMessage = " quantidade máxima permitida é de 40 caracteres")]
        public string Competencia { get; set; }
        public string DescricaoEspecialidade { get { return this.UnidadeServico + "/" + this.Competencia; } }

        public bool Selecionada { get; set; }

    }
}
