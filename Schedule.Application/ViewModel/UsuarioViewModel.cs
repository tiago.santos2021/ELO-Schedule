﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Schedule.Application.ViewModel
{
    public class UsuarioViewModel : BaseViewModel
    {
        public UsuarioViewModel()
        {
            ListaEspecialidadesId = new List<Guid>();
        }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Nome é Obrigatório")]
        public string Nome { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Login é Obrigatório")]
        public string Login { get; set; }
        public string Senha { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Email é Obrigatório")]
        [EmailAddress(ErrorMessage ="Email no Formato invalido")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Estado é Obrigatório")]
        public string UF { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Telefone é Obrigatório")]
        public string Telefone1 { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo DDD é Obrigatório")]
        public string DDD1 { get; set; }
        public string Telefone2 { get; set; }
        public string DDD2 { get; set; }
        public Guid? EmpresaId { get; set; }
        public string EmpresaDescricao { get; set; }
        public Guid? SenioridadeId { get; set; }
        public Guid? TipoContratacaoId { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Perfil é Obrigatório")]
        public Guid PerfilId { get; set; }
        public string PerfilDescricao { get; set; }
        public Guid? GestorImediatoId { get; set; }
        public string GestorImediatoNome{ get; set; }
        public List<Guid>  ListaEspecialidadesId { get; set; }
        public string Especialidade { get; set; }
        public string Senioridade { get; set; }         
        public string TipoContratacao { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Data de Incio é Obrigatório")]
        public string DataInicioContrato { get; set; }
        public string DataTerminoContrato { get; set; }
        public decimal ? ValorHora { get; set; }
        public decimal? ValorMensal { get; set; }
        public bool Ativo { get; set; }
        public string Observacao { get; set; }

        public string StatusDescricao { get { return Ativo == true ? "Ativo" : "Inativo"; } }
    }
}
