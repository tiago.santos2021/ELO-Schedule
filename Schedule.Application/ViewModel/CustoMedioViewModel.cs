﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Schedule.Application.ViewModel
{
  public  class CustoMedioViewModel : BaseViewModel
    {
        //[Required(AllowEmptyStrings = false, ErrorMessage = "O campo Especialidade é Obrigatório")]
        public Guid ? EspecialidadeId { get; set; }
        public string EspecialidadeNome { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Valor é Obrigatório")]

        //[Range(0, 99999999, ErrorMessage = "The value must be greater than 0")]
        public decimal Valor { get; set; }
       



    }
}
