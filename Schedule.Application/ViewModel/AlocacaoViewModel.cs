﻿using Schedule.Application.ViewModel.Agenda;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Schedule.Application.ViewModel
{
    public class AlocacaoViewModel
    {
        public AlocacaoViewModel()
        {
            ListaConsultoresId = new List<Guid>();
            ListaCentroCustos = new List<Guid>();
            Consultores = new List<ConsultorViewModel>();
            ListaCentroDeCustos = new List<CentroDeCustoViewModel>();
            ListaDiasSemana = new List<int>();
        }

        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Empresa é Obrigatório")]
        public Guid EmpresaId { get; set; }

        public string EmpresaDescricao { get; set; }

        public List<ConsultorViewModel> Consultores { get; set; }

        public List<Guid> ListaConsultoresId { get; set; }

        public List<Guid> ListaEspecialidades { get; set; }

        public List<Guid> ListaCentroCustos { get; set; }

        public Guid? CentroCustoId { get; set; }

        public string CentroCustoDescricao { get; set; }

        public List<CentroDeCustoViewModel> ListaCentroDeCustos { get; set; }

        public DateTime DataInicio { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Data de Inicio é Obrigatório")]
        public string DataIniciotexto { get; set; }

        public DateTime DataFim { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Data Final é Obrigatório")]
        public string DataFimtexto { get; set; }

        public bool Periodo1 { get; set; }

        public bool Periodo2 { get; set; }

        public bool Reserva { get; set; }

        public bool Feriado { get; set; }

        public bool Segunda { get; set; }
        public bool Terca { get; set; }
        public bool Quarta { get; set; }
        public bool Quinta { get; set; }
        public bool Sexta { get; set; }
        public bool Sabado { get; set; }
        public bool Domingo { get; set; }
        public List<int> ListaDiasSemana { get; set; }
        public string Usuario { get; set; }

        public string Observacao { get; set; }

        public PlanejamentoAlocacaoViewModel PlanejamentoAlocacao { get; set; }

    


    }






    public class CentroDeCustoViewModel
    {
        public CentroDeCustoViewModel()
        {
                
        }
        public Guid? CentroCustoId { get; set; }
        public string CentroCustoDescricao { get; set; }

    }
}
