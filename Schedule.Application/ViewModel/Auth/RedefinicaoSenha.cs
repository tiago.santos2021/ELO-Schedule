﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Domain.Entities
{
    public class RedefinicaoSenhaViewModel
    {
        public string Token { get; set; }
        public string NovaSenha { get; set; }
    }
}
