﻿using System.ComponentModel.DataAnnotations;

namespace Schedule.Application.ViewModel.Auth
{
    public class AuthViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Campo obrigatório")]
        public string Login { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Campo obrigatório")]
        public string Senha { get; set; }

          
    }
}
