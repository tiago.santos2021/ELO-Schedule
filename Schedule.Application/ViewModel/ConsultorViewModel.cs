﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.ViewModel
{
  public  class ConsultorViewModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public bool Selecionado { get; set; }
    }
}
