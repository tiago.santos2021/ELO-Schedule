﻿using Schedule.Application.ViewModel.Visao;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.ViewModel.Relatorio
{
   public class RespostaSolicitacaoImpressaoPlanejadoViewModel
    {
        public string Erro { get; set; }
        public List<RelPlanejadoDataViewModel> Dados { get; set; }

    }
}
