﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Schedule.Application.ViewModel.Relatorio
{
   public class SolicitacaoImpressaoRelPlanejadoViewModel
    {
        public int ValorPeriodo { get; set; }

        public List<Guid> ListaEmpresas { get; set; }
        public List<Guid> ListaCentroCustos { get; set; }
        public List<Guid> ListaConsultores { get; set; }
        public List<Guid> ListaEspecialidades { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Descrição é Obrigatório")]
        public DateTime DataInicio { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Descrição é Obrigatório")]
        public DateTime DataFinal { get; set; }

    }
}
