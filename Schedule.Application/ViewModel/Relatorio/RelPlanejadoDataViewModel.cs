﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.ViewModel.Visao
{
  public  class RelPlanejadoDataViewModel
    {
        public int Mes { get; set; }
        public int Ano { get; set; }
        public int Ordem { get; set; }
        public string Nome { get; set; }
        public decimal Quantidade { get; set; }
        public string CentroCustoDescricao { get; set; }
        public Guid FuncionarioId { get; set; }
        public string MesAno { get; set; }
        public string Empresa { get; set; }
        public string Senioridade { get; set; }
        public string Especialidade { get; set; }
        public string TipoContratacao { get; set; }
        public string Custo { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Gestor { get; set; }
        public DateTime? DataInicioContrato { get; set; }
        public string DataInicioContratoExibicao { get; set; }
        public DateTime? DataFimContrato { get; set; }

        public string DataFimContratoExibicao { get; set; }
        
    }
   

}
