﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.ViewModel.Estrutura
{
   public class MenuViewModel : BaseViewModel
    {


        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int Ordem { get; set; }
        public int SubOrdem { get; set; }



    }
}
