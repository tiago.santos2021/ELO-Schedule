﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Schedule.Application.ViewModel
{
   public class SenioridadeViewModel : BaseViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Código é Obrigatório")]
        [MaxLength(40, ErrorMessage = " quantidade máxima permitida é de 40 caracteres")]
        public string Codigo { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Descrição é Obrigatório")]
        [MaxLength(100, ErrorMessage = "Quantidade máxima permitida é de 100 caracteres")]
        public string Nome { get; set; }

    }
}
