﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.Resorces.UF
{
   public class EstadoViewModel
    {
        public string Sigla { get; set; }
        public string Descricao { get; set; }
    }
}
