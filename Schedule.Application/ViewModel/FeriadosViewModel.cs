﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Schedule.Application.ViewModel
{
    public class FeriadosViewModel : BaseViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo Descrição é Obrigatório")]
        [MaxLength(40, ErrorMessage = " quantidade máxima permitida é de 40 caracteres")]
        public string Descricao { get; set; }

        public DateTime DataFeriado { get; set; }
        public string Usuario { get; set; }
        public DateTime DataAlteracao { get; set; }

    }
}
