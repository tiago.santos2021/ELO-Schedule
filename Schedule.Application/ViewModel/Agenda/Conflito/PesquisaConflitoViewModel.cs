﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.ViewModel.Agenda.Conflito
{
   public class PesquisaConflitoViewModel
    {

        public PesquisaConflitoViewModel()
        {
            ListaConsultoresId = new List<Guid>();
            ListaCentroCustos = new List<Guid>();
            
        }
        public Guid EmpresaId { get; set; }

        public List<Guid> ListaConsultoresId { get; set; }
        public List<Guid> ListaCentroCustos { get; set; }
        public List<Guid> ListaEspecialidades { get; set; }
        public Guid ConsultoresId { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public string Mensagem { get; set; }
        public bool Pesquisar { get; set; }
        public bool LeituraValida { get; set; }



    }
}
