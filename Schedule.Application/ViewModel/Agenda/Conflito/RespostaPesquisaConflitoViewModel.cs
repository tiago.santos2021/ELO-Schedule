﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.ViewModel.Agenda.Conflito
{
   public class RespostaPesquisaConflitoViewModel
    {
        public bool Sucesso { get; set; }
        public string Mensagem { get; set; }
        public List<UsuarioViewModel> UsuariosConflitos { get; set; }
        public DateTime DataPesquisaInicial { get; set; }
        public DateTime DataPesquisaFinal { get; set; }

        public string Html { get; set; }
    }
}
