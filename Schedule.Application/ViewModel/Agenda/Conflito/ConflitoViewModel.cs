﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.ViewModel.Agenda.Conflito
{
   public class ConflitoViewModel
    {
        public ConflitoViewModel()
        {
            AgendaConflitos = new List<AgendaConflitoViewModel>();
        }

        public DateTime Data { get; set; }
        public List<AgendaConflitoViewModel> AgendaConflitos { get; set; }

    }
}
