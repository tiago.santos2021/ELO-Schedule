﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.ViewModel.Agenda.Conflito
{
  public  class AgendaConflitoViewModel : BaseViewModel
    {

        public int Periodo { get; set; }
        public string UsuarioCriacao { get; set; }
        public string CentroCusto { get; set; }
        public bool conflito { get; set; }
        public bool Selecionado { get; set; }
    }
}
