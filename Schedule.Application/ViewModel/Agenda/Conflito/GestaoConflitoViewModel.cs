﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.ViewModel.Agenda.Conflito
{
  public  class GestaoConflitoViewModel
    {
        public GestaoConflitoViewModel()
        {
            Conflitos = new List<ConflitoViewModel>();
        }
        public string ConsultorNome { get; set; }
        public string EmailConsultor { get; set; }
        public string TelefoneConsultor { get; set; }
        public string GestorImediato { get; set; }
        public string EmailGestorImediato { get; set; }
        public string TelefoneGestorImediato { get; set; }
        public DateTime DataInicial { get; set; }
        public DateTime DataFinal { get; set; }
        public Guid UsuarioId { get; set; }
        public List<ConflitoViewModel> Conflitos { get; set; }

    }
}
