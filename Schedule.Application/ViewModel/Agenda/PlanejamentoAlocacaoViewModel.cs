﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.ViewModel.Agenda
{
  public  class PlanejamentoAlocacaoViewModel
    {

        public bool Sucesso { get; set; }
        public string Mensagem { get; set; }
        public DateTime DataInicioCabecalho { get; set; }
        public DateTime DataTerminoCabecalho { get; set; }
        public List<AlocacaoAgendaViewModel> Alocacao { get; set; }

    }
}
