﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.ViewModel.Agenda
{
    public class AlocacaoAgendaViewModel
    {

        public AlocacaoAgendaViewModel()
        {
            ListaDomingos1 = new List<AgendaViewModel>();
            ListaDomingos2 = new List<AgendaViewModel>();
            ListaSegundas1 = new List<AgendaViewModel>();
            ListaSegundas2 = new List<AgendaViewModel>();
            ListaTercas1 = new List<AgendaViewModel>();
            ListaTercas2 = new List<AgendaViewModel>();

            ListaQuartas1 = new List<AgendaViewModel>();
            ListaQuartas2 = new List<AgendaViewModel>();


            ListaQuintas1 = new List<AgendaViewModel>();
            ListaQuintas2 = new List<AgendaViewModel>();

            ListaSextas1 = new List<AgendaViewModel>();
            ListaSextas2 = new List<AgendaViewModel>();

        }
        public int Periodo { get; set; }
        public string ConsultorNome { get; set; }
        public bool PossuiConflito { get; set; }
        public string ClasseEstiloConflito { get; set; }
        public string ConsultorNomeFormatado { get { return FormataTexto(this.ConsultorNome); } }

        public string Especialidade { get; set; }
        public string EspecialidadeFormatado { get { return FormataTexto(this.Especialidade); } }
        public int Ordem { get; set; }
        public bool zebrar { get; set; }
        public string ClasseEstiloTabela { get; set; }
        public Guid ConsultorId { get; set; }

        public List<AgendaViewModel> ListaSabados1 { get; set; }
        public List<AgendaViewModel> ListaSabados2 { get; set; }
        public List<AgendaViewModel> ListaDomingos1 { get; set; }
        public List<AgendaViewModel> ListaDomingos2 { get; set; }
        public List<AgendaViewModel> ListaSegundas1 { get; set; }
        public List<AgendaViewModel> ListaSegundas2 { get; set; }

        public List<AgendaViewModel> ListaTercas1 { get; set; }
        public List<AgendaViewModel> ListaTercas2 { get; set; }

        public List<AgendaViewModel> ListaQuartas1 { get; set; }
        public List<AgendaViewModel> ListaQuartas2 { get; set; }

        public List<AgendaViewModel> ListaQuintas1{ get; set; }
        public List<AgendaViewModel> ListaQuintas2 { get; set; }
        public List<AgendaViewModel> ListaSextas1 { get; set; }
        public List<AgendaViewModel> ListaSextas2 { get; set; }

        private string FormataTexto(string valor)
        {
            if(valor.Length >= 29)
            {
                var ultimaPosicao = valor.LastIndexOf('-');
                if(ultimaPosicao == 24)
                {
                    valor = valor.Substring(0, 25);
                    valor = valor + "....";
                }
                else
                {
                    valor = valor.Substring(0, 26);
                    valor = valor + "...";
                }
               
               
            }
            
                return valor;
            
        }

    }
}