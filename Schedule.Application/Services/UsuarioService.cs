﻿using AutoMapper;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository;
using Schedule.Domain.Interfaces.Repository.Estrutura;
using Schedule.Util.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Schedule.Application.Services
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _repositorio;
        private readonly IUsuarioEspecialidadeRepository _usuarioEspecialidadeRepository;
        private readonly IEmpresaRepository _empresaRepository;
        private readonly IPerfilRepository _perfilRepository;
        private readonly ISenioridadeRepository _senioridadeRepository;
       // private 
        private readonly IMapper _mapper;
        public UsuarioService(IUsuarioRepository repositorio, 
            IUsuarioEspecialidadeRepository usuarioEspecialidadeRepository, 
            IEmpresaRepository EmpresaRepository, 
            IPerfilRepository perfilRepository,
            ISenioridadeRepository senioridadeRepository,
            IMapper mapper
            
            
            )
        {
            this._repositorio = repositorio;
            this._usuarioEspecialidadeRepository = usuarioEspecialidadeRepository;
            this._mapper = mapper;
            this._empresaRepository = EmpresaRepository;
            this._perfilRepository = perfilRepository;
            this._senioridadeRepository = senioridadeRepository;
        }    
        public void Adicionar(UsuarioViewModel entidade)
        {
            throw new NotImplementedException();
        }       
        private void ValidarConsultor(UsuarioViewModel entidade)
        {
            if (entidade.EmpresaId == null || entidade.EmpresaId == Guid.Empty)
                throw new DomainException("Campo Empresa é Obrigatorio");
            if (entidade.SenioridadeId == null || entidade.SenioridadeId == Guid.Empty)
                throw new DomainException("Campo Senioridade é Obrigatorio");
            if (entidade.TipoContratacaoId == null || entidade.TipoContratacaoId == Guid.Empty)
                throw new DomainException("Campo Tipo de Contratação é Obrigatorio");
            if (entidade.GestorImediatoId == null || entidade.GestorImediatoId == Guid.Empty)
                throw new DomainException("Campo Gestor Imediato é Obrigatorio");
            if (entidade.ListaEspecialidadesId == null || entidade.ListaEspecialidadesId.Count() == 0)
                throw new DomainException("Informe pelo menos uma Especialdiade para o Consultor");
            if ((entidade.ValorHora == null || entidade.ValorHora == 0) && (entidade.ValorMensal == null || entidade.ValorMensal == 0))
                throw new DomainException("Informe o valor hora ou valor Mensal para do Consultor");
            if (!string.IsNullOrWhiteSpace(entidade.DataInicioContrato) &&!Validacao.VerificaData(entidade.DataInicioContrato) 
                || (!string.IsNullOrWhiteSpace(entidade.DataTerminoContrato) && !Validacao.VerificaData(entidade.DataTerminoContrato)))
            {
                throw new DomainException("Verifique os campos data inicio de contrato/data término Contrato");
            }
            if (!string.IsNullOrEmpty(entidade.DataInicioContrato))
                Validacao.ValidaLimitePermetidoData(Convert.ToDateTime(entidade.DataInicioContrato));
            if (!string.IsNullOrEmpty(entidade.DataTerminoContrato))
                Validacao.ValidaLimitePermetidoData(Convert.ToDateTime(entidade.DataTerminoContrato));
        }    
        public void Adicionar(UsuarioViewModel entidade, bool TipoConsultor)

        {
            if (_repositorio.Existe(x => x.Login.ToLower() == entidade.Login.ToLower()))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Login"));
            }
            if (TipoConsultor)
            {
                ValidarConsultor(entidade);
            }

            var usuario = _mapper.Map<Usuario>(entidade);
            usuario.Senha = entidade.Senha;
            usuario.Ativo = entidade.Ativo;

            if (TipoConsultor)
            {
                _repositorio.Adicionar(usuario, entidade.ListaEspecialidadesId);
            }
            else
            {

                _repositorio.Adicionar(usuario);
            }



        }      
        public void Alterar(UsuarioViewModel entidade)
        {
            throw new NotImplementedException();
        }    
        public void Alterar(UsuarioViewModel entidade, bool TipoConsultor)
        {
            if (_repositorio.Existe(x => x.Login.ToLower() == entidade.Login.ToLower() && x.Id != entidade.id))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Login"));
            }
            if (TipoConsultor)
            {
                ValidarConsultor(entidade);
            }

            var usuario = _mapper.Map<Usuario>(entidade);

            if (TipoConsultor)
            {
                _repositorio.Editar(usuario, entidade.ListaEspecialidadesId);
            }
            else
            {
                var entidadeAlteracao = _repositorio.ObterPorId(usuario.Id);
                entidadeAlteracao.DataAlteracao = DateTime.Now;
                entidadeAlteracao.Nome = entidade.Nome;
                entidadeAlteracao.Login = entidade.Login;
                entidadeAlteracao.UF = entidade.UF;
                entidadeAlteracao.Telefone1 = entidade.Telefone1;
                entidadeAlteracao.DDD1 = entidade.DDD1;
                entidadeAlteracao.Telefone2 = entidade.Telefone2;
                entidadeAlteracao.DDD2 = entidade.DDD2;
                entidadeAlteracao.Email = entidade.Email;
                entidadeAlteracao.DataInicioContrato = Convert.ToDateTime(entidade.DataInicioContrato);
                entidadeAlteracao.DataTerminoContrato = entidade.DataTerminoContrato != null ? (DateTime?)Convert.ToDateTime(entidade.DataTerminoContrato) : null;
                entidadeAlteracao.PerfilId = entidade.PerfilId;
                //entidadeAlteracao.EmpresaId = null;
                //entidadeAlteracao.SenioridadeId = null;
                //entidadeAlteracao.GestorImediatoId = null;
                //entidadeAlteracao.TipoContratacaoId = null;
                //entidadeAlteracao.ValorHora = null;
                //entidadeAlteracao.ValorMensal = null;
                entidadeAlteracao.Usuario = entidade.Usuario;
                entidadeAlteracao.Ativo = entidade.Ativo;
                _repositorio.Editar(entidadeAlteracao);
            }
        }    
        public void DesativarUsuario(Guid Id, string usuario)
        {
            _repositorio.DesativarUsuario(Id, usuario);
        }     
        public void Excluir(Guid id)
        {
            var usuario = _repositorio.ObterPorId(id);
            usuario.DataAlteracao = DateTime.Now;

            _repositorio.Editar(usuario);
        }

        public List<UsuarioViewModel> Listar()
        {
            IQueryable<Usuario> lista = _repositorio.Listar();

            var resultado = _mapper.Map<List<UsuarioViewModel>>(lista);
            if (resultado != null && resultado.Count() > 0)
            {
                var listaEmpresas = this._empresaRepository.Listar().ToList();
                var listaPerfis = this._perfilRepository.Listar().ToList();

                foreach (var item in resultado)
                {
                    if (item.EmpresaId != null)
                    {
                        item.EmpresaDescricao = RetornaDescricaoEmpresa(listaEmpresas, (Guid)item.EmpresaId);
                    }
                    if (item.PerfilId != null)
                    {
                        item.PerfilDescricao = RetornaDescricaoPerfil(listaPerfis, (Guid)item.PerfilId);
                    }

                }

            }


            return resultado;
        }

        public List<UsuarioViewModel> ListaUsuarioDetalhes()
        {
            var res = new List<UsuarioViewModel>();

            foreach (var item in _repositorio.ListaUsuarioDetalhes())
            {
                res.Add(new UsuarioViewModel()
                {
                    Nome = item.GetType().GetProperty("Nome").GetValue(item, null).ToString(),
                    Login = item.GetType().GetProperty("Login").GetValue(item, null).ToString(),
                    Ativo = (bool)item.GetType().GetProperty("Ativo").GetValue(item, null),
                    PerfilDescricao = item.GetType().GetProperty("PerfilDescricao").GetValue(item, null).ToString(),
                    Senioridade = item.GetType().GetProperty("Senioridade").GetValue(item, null).ToString(),
                    DataInicioContrato = item.GetType().GetProperty("DataInicioContrato").GetValue(item, null).ToString(),
                    DataTerminoContrato = item.GetType().GetProperty("DataTerminoContrato").GetValue(item, null).ToString(),
                    TipoContratacao = item.GetType().GetProperty("TipoContratacao").GetValue(item, null).ToString(),
                    Especialidade = item.GetType().GetProperty("Especialidade").GetValue(item, null).ToString(),
                    id = (Guid)item.GetType().GetProperty("Id").GetValue(item, null),
                    Observacao = item.GetType().GetProperty("Observacao").GetValue(item, null).ToString(),
                    EmpresaDescricao = item.GetType().GetProperty("Empresa").GetValue(item, null).ToString(),
                });
            }

            return res;
        }

        private string RetornaDescricaoEmpresa(List<Empresa> lista, Guid id)
        {
            string retorno = "";

            if (lista != null && lista.Count() > 0)
            {
                var empresa = lista.Where(e => e.Id == id).FirstOrDefault();
                if (empresa != null)
                    retorno = empresa.Codigo;
            }
            return retorno;
        }
        private string RetornaDescricaoPerfil(List<Perfil> lista, Guid id)
        {
            string retorno = "";

            if (lista != null && lista.Count() > 0)
            {
                var perfil = lista.Where(e => e.Id == id).FirstOrDefault();
                if (perfil != null)
                    retorno = perfil.Nome;
            }
            return retorno;
        }
           
        public UsuarioViewModel RetornaPorId(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            var resultado = _mapper.Map<UsuarioViewModel>(entidade);
            var ListaEspecialidades = this._usuarioEspecialidadeRepository.ListarPor(u => u.UsuarioId == entidade.Id);
            if (ListaEspecialidades != null && ListaEspecialidades.Count() > 0)
            {
                foreach (var item in ListaEspecialidades)
                {
                    resultado.ListaEspecialidadesId.Add(item.EspecialidadeId);
                }

            }

            return resultado;
        }      
        public bool VerificaSeRegistroExisteNaBase(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            return entidade != null;

        }
        public List<UsuarioViewModel> ListarConsultores(List<Guid> listaTiposConsultores)
        {
            IQueryable<Usuario> lista = _repositorio.Listar().Where(x => listaTiposConsultores.Contains(x.PerfilId));
            var resultado = _mapper.Map<List<UsuarioViewModel>>(lista);
            resultado = resultado.OrderBy(u => u.Nome).ToList();
            return resultado;
        }
        public List<UsuarioViewModel> ListarConsultoresAtivos(List<Guid> listaTiposConsultores)
        {
            IQueryable<Usuario> lista = _repositorio.Listar().Where(x => listaTiposConsultores.Contains(x.PerfilId) && x.Ativo == true);
            var resultado = _mapper.Map<List<UsuarioViewModel>>(lista);
            resultado = resultado.OrderBy(u => u.Nome).ToList();
            return resultado;
        }

        public List<UsuarioViewModel> ListarGestores(List<Guid> listaFiltroPerfil)
        {
            var lista = _repositorio.ListarPor(g => listaFiltroPerfil.Contains( g.PerfilId ));
            var resultado = _mapper.Map<List<UsuarioViewModel>>(lista);
            return resultado;
        }
    }
}
