﻿using AutoMapper;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Application.ViewModel.Auth;

using Schedule.Domain.Entities;
using Schedule.Domain.Enum;
using Schedule.Domain.Interfaces.Repository;
using Schedule.Util.Criptografia;
using Schedule.Util.Email;
using System;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;

namespace Schedule.Application.Services
{
    public class AccountServices : IAccountService
    {

        private readonly IMapper mapper;
        private readonly IUsuarioRepository _usuarioRepositorio;
        private readonly IAccountRepository _accountRepositorio;

        public AccountServices(IMapper mapper, IUsuarioRepository usuarioRepositorio, IAccountRepository accountRepositorio)
        {
            this.mapper = mapper;
            this._usuarioRepositorio = usuarioRepositorio;
            this._accountRepositorio = accountRepositorio;
        }
        public UsuarioViewModel GetUsuario(AuthViewModel model)
        {
            var senhaDigitada = Criptografia.RetornarMD5(model.Senha);
            var user = _usuarioRepositorio.ObterPor(x => x.Login.ToLower() == model.Login.ToLower() && x.Senha == senhaDigitada );
            return mapper.Map<UsuarioViewModel>(user);
        }
        public Tuple<bool, string> EnviarEmailRedefinicaoSenha(  string login, string urlRedefinicaoSenha)
        {

            var dadosUsuario = _accountRepositorio.BuscarEmailUsuario(login);
            if (String.IsNullOrEmpty(dadosUsuario.Email))
                return new Tuple<bool, string>(false, "Usuário não encontrado");

            if (dadosUsuario != null && dadosUsuario.Ativo == false)
                return new Tuple<bool, string>(false, "Usuário Inativo");


            var templateHtml = _accountRepositorio.BuscarTemplateEmail(TipoEmailEnum.RedefinicaoSenha);

            var token = Guid.NewGuid().ToString();
            var sLinkRedefinicao = string.Format(urlRedefinicaoSenha, token);

            string pattern = @"\#link\#";
            templateHtml = Regex.Replace(templateHtml, pattern, sLinkRedefinicao);
            pattern = @"\#NomeUsuario\#";
            templateHtml = Regex.Replace(templateHtml, pattern, dadosUsuario.Nome);

            Email email = new Email();
            email.Assunto = "Redefinição senha Schedule";
            email.emailPara = dadosUsuario.Email;
            email.NomeDestinatario = dadosUsuario.Nome;
            email.Corpo = templateHtml;
            email.Dispaly = "SCHEDULE";
            email.Rementente = ScheduleResorce.RetornaValor("Email");
            email.Senha = ScheduleResorce.RetornaValor("Senha");


            var envioEmailRetorno = EmailService.EnviarEmail(email);

            if (envioEmailRetorno.Item1)
            {
                var dataSolicitacao = DateTime.Now;
                var gerenciamentoToken = new GerenciamentoToken
                {
                    Token = token,
                    DataSolicitacao = dataSolicitacao,
                    Login = login,
                    DataValidade = dataSolicitacao.AddHours(1),
                    Utilizado = false,
                    DataAlteracao = dataSolicitacao,
                    Usuario = dadosUsuario.Usuario
                };
                _accountRepositorio.AdicionarToken(gerenciamentoToken);
            }

            return new Tuple<bool, string>(envioEmailRetorno.Item1, envioEmailRetorno.Item2);
        }
        public Tuple<bool, string> EditarSenha(string token, string novaSenha)
        {
            var gerenciamentoToken = _accountRepositorio.ValidarTokenRedefinicaoSenha(token);

            if (gerenciamentoToken == null)
                return new Tuple<bool, string>(false, "Token inválido");

            if (gerenciamentoToken.DataValidade < DateTime.Now)
                return new Tuple<bool, string>(false, "Token expirado");

            _accountRepositorio.EditarSenha(gerenciamentoToken.Login, novaSenha);

            _accountRepositorio.AtualizarTokenUsado(gerenciamentoToken.Token);
            return new Tuple<bool, string>(true, "Senha alterada com sucesso");
        }

    }
}
