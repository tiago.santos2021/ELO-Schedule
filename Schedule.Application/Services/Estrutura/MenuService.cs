﻿using AutoMapper;
using Schedule.Application.Interfaces.Services.Estrutura;
using Schedule.Application.ViewModel.Estrutura;
using Schedule.Domain.Entities.Estrutura;
using Schedule.Domain.Interfaces.Repository.Estrutura;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schedule.Application.Services.Estrutura
{
    public class MenuService : IMenuService
    {
        private readonly IMenuRepository _repositorio;
        private readonly IPerfilMenuRepository _repositorioPerfilMenu;
       private readonly IMapper _mapper;


        public MenuService(IMenuRepository repositorio, IPerfilMenuRepository repositorioPerfilMenu, IMapper mapper)
        {
            this._repositorio = repositorio;
            this._repositorioPerfilMenu = repositorioPerfilMenu;
            _mapper = mapper;

        }
        public List<MenuViewModel> RetornaListaDeMenusPorPerfil(Guid PerfilId)
        {
            List<Menu> retorno = null;
          
            var listaPerfilMenus = _repositorioPerfilMenu.Listar().Where(p => p.Perfilid == PerfilId ).ToList();
            if (listaPerfilMenus != null)
            {
                var listamenus = _repositorio.Listar().ToList();
                retorno = new List<Menu>();
                retorno = listamenus.Where(m => listaPerfilMenus.Select(x => x.MenuId).Contains(m.Id)).ToList();
                retorno = retorno.OrderBy(m => m.Ordem).ThenBy(m=> m.SubOrdem).ToList();
               
            }

            var resultado = _mapper.Map<List<MenuViewModel>>(retorno);
            return resultado;


        }
    }
}
