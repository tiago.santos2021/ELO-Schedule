﻿using AutoMapper;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Interfaces.Services.Estrutura;
using Schedule.Application.ViewModel.Estrutura;
using Schedule.Domain.Entities.Estrutura;
using Schedule.Domain.Interfaces.Repository.Estrutura;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schedule.Application.Services.Estrutura
{
    public class PermissaoService : IPermissaoService
    {
        private readonly IPermissaoRepository _repositorio;
        private readonly IPerfilPermissaoRepository _repositorioPerfilPermissao;
        private readonly IMapper _mapper;


        public PermissaoService(IPermissaoRepository repositorio, IPerfilPermissaoRepository repositorioPerfilPermissao, IMapper mapper)
        {
            this._repositorio = repositorio;
            this._repositorioPerfilPermissao = repositorioPerfilPermissao;
            _mapper = mapper;

        }

        public List<PermissaoViewModel> RetornaListaDePermissoesPorPerfil(Guid PerfilId)
        {
            List<Permissao> retorno = null;

            var listaPerfilMenus = _repositorioPerfilPermissao.Listar().Where(p => p.Perfilid == PerfilId).ToList();
            if (listaPerfilMenus != null)
            {
                var listamenus = _repositorio.Listar().ToList();
                retorno = new List<Permissao>();
                retorno = listamenus.Where(m => listaPerfilMenus.Select(x => x.PermissaoId).Contains(m.Id)).ToList();              
            }
            var resultado = _mapper.Map<List<PermissaoViewModel>>(retorno);
            return resultado;
        }
    }
}
