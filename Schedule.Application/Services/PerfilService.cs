﻿using AutoMapper;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schedule.Application.Services
{
    public class PerfilService : IPerfilService
    {
        private readonly IPerfilRepository _repositorio;
        private readonly IMapper _mapper;
        public PerfilService(IPerfilRepository repositorio, IMapper mapper)
        {
            this._repositorio = repositorio;
            this._mapper = mapper;

        }
        public void Adicionar(PerfilViewModel entidade)
        {
            if (_repositorio.Existe(x => x.Grupo.ToLower() == entidade.Grupo.ToLower()))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Grupo"));
            }
            if (_repositorio.Existe(x => x.Nome.ToLower() == entidade.Nome.ToLower()))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Nome"));
            }

          
            var empresa = _mapper.Map<Perfil>(entidade);
            _repositorio.Adicionar(empresa);
        }
        public void Alterar(PerfilViewModel entidade)
        {
            if (_repositorio.Existe(x => x.Grupo.ToLower() == entidade.Grupo.ToLower() && x.Id != entidade.id))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Grupo"));
            }
            if (_repositorio.Existe(x => x.Nome.ToLower() == entidade.Nome.ToLower() && x.Id != entidade.id))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Nome"));
            }

            var perfil = _repositorio.ObterPorId((Guid)entidade.id);
            perfil.Nome = entidade.Nome;
            perfil.Grupo = entidade.Grupo;
            perfil.Usuario = entidade.Usuario;
            _repositorio.Editar(perfil);
        }
        public void Excluir(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            _repositorio.Remover(entidade);
        }
        public List<PerfilViewModel> Listar()
        {
            var lista = _repositorio.Listar().ToList();
            var resultado = _mapper.Map<List<PerfilViewModel>>(lista);
            return resultado;
        }
        public PerfilViewModel RetornaPorId(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            var resultado = _mapper.Map<PerfilViewModel>(entidade);
            return resultado;
        }
        public bool VerificaSeRegistroExisteNaBase(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            return entidade != null;

        }
    }
}
