﻿using AutoMapper;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schedule.Application.Services
{
    public class CustoMedioService : ICustoMedioService
    {
        private readonly ICustoMedioRepository _repositorio;
        private readonly IEspecialidadeRepository _especialidadeRepositorio;
        private readonly IMapper _mapper;
        public CustoMedioService(ICustoMedioRepository repositorio, IEspecialidadeRepository especialidadeRepositorio, IMapper mapper)
        {
            this._repositorio = repositorio;
            _especialidadeRepositorio = especialidadeRepositorio;
            this._mapper = mapper;

        }
        public void Adicionar(CustoMedioViewModel entidade)
        {
            if (_repositorio.Existe(x => x.EspecialidadeId == entidade.EspecialidadeId ))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Código"));
            }
            if (entidade.Valor <= 0)
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("CampoMenorIgualaZero"), "Valor"));
            }
            if (entidade.EspecialidadeId == null || entidade.EspecialidadeId == Guid.Empty)
            {

                throw new DomainException("O campo especialidade é obrigatório");
            }
            var empresa = _mapper.Map<CustoMedio>(entidade);
            _repositorio.Adicionar(empresa);
        }
        public void Alterar(CustoMedioViewModel entidade)
        {
            if (_repositorio.Existe(x => x.EspecialidadeId == entidade.EspecialidadeId && x.Id != entidade.id))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Código"));
            }
            if (entidade.Valor <= 0)
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("CampoMenorIgualaZero"), "Valor"));
            }
            if (entidade.EspecialidadeId == null || entidade.EspecialidadeId == Guid.Empty)
            {

                throw new DomainException("O Campo especialidade é obrigatório");
            }


            var CustoMedio = _repositorio.ObterPorId((Guid)entidade.id);
            CustoMedio.EspecialidadeId = (Guid) entidade.EspecialidadeId;
            CustoMedio.Valor = entidade.Valor;
            CustoMedio.Usuario = entidade.Usuario;
            _repositorio.Editar(CustoMedio);
        }
        public void Excluir(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            _repositorio.Remover(entidade);
        }
        public List<CustoMedioViewModel> Listar()
        {
            var lista = _repositorio.Listar().ToList();
            var resultado = _mapper.Map<List<CustoMedioViewModel>>(lista);




            if (resultado != null && resultado.Count() > 0)
            {
                var especialidades = _especialidadeRepositorio.Listar();
                foreach (var item in resultado)
                {
                    var especialdiade = especialidades.Where(e => e.Id == item.EspecialidadeId).FirstOrDefault();
                    if (especialdiade != null)
                        item.EspecialidadeNome = especialdiade.UnidadeServico + "/" + especialdiade.Competencia;
                }
            }
            return resultado;
        }
        public CustoMedioViewModel RetornaPorId(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            var resultado = _mapper.Map<CustoMedioViewModel>(entidade);
            return resultado;

        }
        public bool VerificaSeRegistroExisteNaBase(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            return entidade != null;

        }
    }
}
