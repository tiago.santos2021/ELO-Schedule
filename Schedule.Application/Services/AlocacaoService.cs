﻿using AutoMapper;
using Schedule.Application.Interfaces.Services;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Application.ViewModel.Agenda;
using Schedule.Application.ViewModel.Agenda.Conflito;
using Schedule.Application.ViewModel.Estrutura;
using Schedule.Domain;
using Schedule.Domain.Entities;
using Schedule.Domain.Entities.Estrutura;
using Schedule.Domain.Enum;
using Schedule.Domain.Interfaces;
using Schedule.Domain.Interfaces.Repository;
using Schedule.Domain.Interfaces.Repository.Estrutura;
using Schedule.Util.Email;
using Schedule.Util.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

public class AlocacaoService : IAlocacaoService
{
    bool sucesso = false;
    string msgRetorno = "";
    private readonly IAlocacaoRepository _alocacaoRepository;
    private readonly IUsuarioEspecialidadeRepository _usuarioEspecialidadeRepository;
    private readonly IUsuarioRepository _usuariorepository;
    private readonly IEspecialidadeRepository _especialidadeRepository;
    private readonly ICentroDeCustoRepository _centroDeCustoRepositorio;
    private readonly ITipoContratacaoRepository _tipoContratacaoRepository;

    private readonly IMapper _mapper;
    private List<DateTime> listaSabado, listaDomingo, listaSegunda, listaterca, listaQuarta, listQuinta, listaSexta;

    List<TipoCenttroDeCustoEstiloViewModel> listaEstiloCentroDeCusto = new List<TipoCenttroDeCustoEstiloViewModel>();
    List<CentroCusto> listaCentroDeCusto = new List<CentroCusto>();
    List<TipoContratacao> listaTipoContratacao = new List<TipoContratacao>();
    Guid _TipoCentroDeCustoInterno;

    private List<TipoCenttroDeCustoEstiloViewModel> RetonaListaCentroDeCustoEstilizacao()
    {

        List<TipoCenttroDeCustoEstiloViewModel> lista = new List<TipoCenttroDeCustoEstiloViewModel>();

        lista.Add(new TipoCenttroDeCustoEstiloViewModel() { Chave = "BANCO_HORAS", Retorno = "BANCO_HORAS" });
        lista.Add(new TipoCenttroDeCustoEstiloViewModel() { Chave = "DESCANSO_REMUNERADO", Retorno = "DESCANSO_REMUNERADO" });
        lista.Add(new TipoCenttroDeCustoEstiloViewModel() { Chave = "FERIAS", Retorno = "FERIAS" });
        lista.Add(new TipoCenttroDeCustoEstiloViewModel() { Chave = "TREINAMENTO", Retorno = "TREINAMENTO" });
        lista.Add(new TipoCenttroDeCustoEstiloViewModel() { Chave = "AUSENTE", Retorno = "AUSENTE" });
        
        return lista;
    }


    public AlocacaoService(IAlocacaoRepository alocacaoRepository, IUsuarioEspecialidadeRepository usuarioEspecialidadeRepository,
        IUsuarioRepository usuariorepository, IEspecialidadeRepository EspecialidadeRepository, ICentroDeCustoRepository CentroDeCustoRepositorio, ITipoContratacaoRepository TipoConttratacaoRepository, IMapper mapper)
    {
        _alocacaoRepository = alocacaoRepository;
        _usuarioEspecialidadeRepository = usuarioEspecialidadeRepository;
        _usuariorepository = usuariorepository;
        _especialidadeRepository = EspecialidadeRepository;
        _centroDeCustoRepositorio = CentroDeCustoRepositorio;
        _tipoContratacaoRepository = TipoConttratacaoRepository;
        _mapper = mapper;
    }

    #region  Cadastro/Exclusão Alocação
    public Tuple<bool, string> Incluir(AlocacaoViewModel entidade)
    {
        try
        {
            string tabelaValidacao = string.Empty;
            string emailsConflito = string.Empty;



            if (entidade.DataFim == DateTime.MinValue)
                entidade.DataFim = entidade.DataInicio.Date;

            var dataAgenda = entidade.DataInicio.Date;

            while (dataAgenda <= entidade.DataFim.Date)
            {
                var diaSemana = (int)dataAgenda.DayOfWeek;

                if (!entidade.ListaDiasSemana.Contains(diaSemana) && entidade.Feriado == false)
                {
                    dataAgenda = dataAgenda.AddDays(1);
                }
                else
                {
                    foreach (var consultor in entidade.Consultores)
                    {
                        List<Agenda> listaAgendaPeriodos = new List<Agenda>();

                        if (entidade.Periodo1)
                        {
                            //Centro de custo pode ser nulo caso o registro seja reserva (RH)
                            if (entidade.ListaCentroCustos.Count > 0)
                            {
                                foreach (var cc in entidade.ListaCentroDeCustos)
                                {
                                    var novaAgendaP1 = AdicionaNovaAgenda(entidade, dataAgenda, consultor, 0);
                                    novaAgendaP1.CentroCustoId = (Guid)cc.CentroCustoId;
                                    novaAgendaP1.CentroCustoDescricao = cc.CentroCustoDescricao;
                                    listaAgendaPeriodos.Add(novaAgendaP1);
                                }
                            }
                            else
                            {
                                var novaAgendaP1 = AdicionaNovaAgenda(entidade, dataAgenda, consultor, 0);
                                listaAgendaPeriodos.Add(novaAgendaP1);
                            }
                        }

                        if (entidade.Periodo2)
                        {
                            //Centro de custo pode ser nulo caso o registro seja reserva (RH)
                            if (entidade.ListaCentroCustos.Count > 0)
                            {
                                foreach (var cc in entidade.ListaCentroDeCustos)
                                {
                                    var novaAgendaP2 = AdicionaNovaAgenda(entidade, dataAgenda, consultor, 1);
                                    novaAgendaP2.CentroCustoId = (Guid)cc.CentroCustoId;
                                    novaAgendaP2.CentroCustoDescricao = cc.CentroCustoDescricao;
                                    listaAgendaPeriodos.Add(novaAgendaP2);
                                }
                            }
                            else
                            {
                                var novaAgendaP2 = AdicionaNovaAgenda(entidade, dataAgenda, consultor, 1);
                                listaAgendaPeriodos.Add(novaAgendaP2);
                            }
                        }

                        //Valida conflitos e salva nova agenda
                        foreach (var novaAgenda in listaAgendaPeriodos)
                        {
                            //var retornoConflito = _alocacaoRepository.ValidarConflitos(novaAgenda.UsuarioId, novaAgenda.DataAgenda, novaAgenda.Periodo, entidade.Usuario);
                            //if (!string.IsNullOrEmpty(retornoConflito.Item1))
                            //{
                            //    foreach (var email in retornoConflito.Item1.Split(";"))
                            //    {
                            //        if (!emailsConflito.Contains(email))
                            //            emailsConflito += email + ",";
                            //    }

                            //    var gestores = retornoConflito.Item2.Split(";");

                            //    string mensagemConflito = string.Format("Validar com os <b>gestores {0}</b> - conflito de alocação para o <b>consultor: {1}</b>, <b>Data: {2}</b>, <b>Período: {3}</b>.",
                            //        string.Join(",", gestores),
                            //        novaAgenda.FuncionarioNome,
                            //        novaAgenda.DataAgenda.ToString("dd/MM/yyyy"),
                            //        novaAgenda.Periodo);

                            //    string linhaConflito = @"<tr><td align=""center"" style=""font-size: 15px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 5px;"">#VALIDACAO#</td></tr>";
                            //    string patternValidacao = @"\#VALIDACAO\#";
                            //    linhaConflito = Regex.Replace(linhaConflito, patternValidacao, mensagemConflito);
                            //    tabelaValidacao += linhaConflito;
                            //}

                            _alocacaoRepository.Incluir(novaAgenda);
                            sucesso = true;
                            msgRetorno = "Registro incluído!";
                        }
                    }

                    dataAgenda = dataAgenda.AddDays(1);
                }
            }

            if (!string.IsNullOrEmpty(emailsConflito) && !string.IsNullOrEmpty(tabelaValidacao))
            {
                var templateHtml = _alocacaoRepository.BuscarTemplateEmail(TipoEmailEnum.ConflitoAlocacao);
                string pattern = @"\#LINHAS\#";
                templateHtml = Regex.Replace(templateHtml, pattern, tabelaValidacao);
                //Email.EnviarEmail(emailsConflito.Remove(emailsConflito.Length - 1), "Conflito Alocação", "", templateHtml);
                msgRetorno = "Registro incluído, porém com conflito(s).";
            }
        }
        catch (Exception e)
        {
            sucesso = false;
            msgRetorno = e.Message;
            throw;
        }
        return new Tuple<bool, string>(sucesso, msgRetorno);
    }

    private Agenda AdicionaNovaAgenda(AlocacaoViewModel entidade, DateTime dataAgenda, ConsultorViewModel consultor, int periodo)
    {
        Agenda novaAgendaP1 = new Agenda
        {
            UsuarioId = consultor.Id,
            FuncionarioNome = consultor.Nome,
            EmpresaId = entidade.EmpresaId,
            EmpresaDescricao = entidade.EmpresaDescricao,
            DataAgenda = dataAgenda.Date,
            TipoAgenda = entidade.Reserva ? 0 : 1, //Caso seja reserva fica como 0, caso seja alocação efetivada fica como 1
            Periodo = periodo == 0 ? (int)PeriodoAlocacaoEnum.Manha : (int)PeriodoAlocacaoEnum.Tarde,
            Usuario = entidade.Usuario,
            Feriado = entidade.Feriado ? 1 : 0, //Flag para feriado
            DataAlteracao = DateTime.Now
        };
        return novaAgendaP1;
    }

    public Tuple<bool, string> Deletar(AlocacaoViewModel entidade)
    {
        if (entidade.DataFim == DateTime.MinValue)
            entidade.DataFim = entidade.DataInicio.Date;

        var dataAgenda = entidade.DataInicio.Date;

        while (dataAgenda <= entidade.DataFim.Date)
        {
            foreach (var consultor in entidade.Consultores)
            {
                var listaAgendaPeriodos = new List<Agenda>();

                if (entidade.Periodo1)
                {
                    if (entidade.ListaCentroCustos.Count > 0)
                    {
                        foreach (var centroCusto in entidade.ListaCentroCustos)
                        {
                            Agenda novaAgendaP1 = new Agenda();
                            novaAgendaP1.UsuarioId = consultor.Id;
                            novaAgendaP1.EmpresaId = entidade.EmpresaId;
                            novaAgendaP1.EmpresaDescricao = entidade.EmpresaDescricao;
                            novaAgendaP1.DataAgenda = dataAgenda.Date;
                            novaAgendaP1.Periodo = (int)PeriodoAlocacaoEnum.Manha;
                            if (centroCusto.ToString() == "00000000-0000-0000-0000-000000000000")
                                novaAgendaP1.CentroCustoId = null;
                            else
                                novaAgendaP1.CentroCustoId = (Guid)centroCusto;

                            listaAgendaPeriodos.Add(novaAgendaP1);
                        }
                    }
                }

                if (entidade.Periodo2)
                {
                    if (entidade.ListaCentroCustos.Count > 0)
                    {
                        foreach (var centroCusto in entidade.ListaCentroCustos)
                        {
                            Agenda novaAgendaP2 = new Agenda();
                            novaAgendaP2.UsuarioId = consultor.Id;
                            novaAgendaP2.EmpresaId = entidade.EmpresaId;
                            novaAgendaP2.EmpresaDescricao = entidade.EmpresaDescricao;
                            novaAgendaP2.DataAgenda = dataAgenda.Date;
                            novaAgendaP2.Periodo = (int)PeriodoAlocacaoEnum.Tarde;
                            if (centroCusto.ToString() == "00000000-0000-0000-0000-000000000000")
                                novaAgendaP2.CentroCustoId = null;
                            else
                                novaAgendaP2.CentroCustoId = (Guid)centroCusto;

                            listaAgendaPeriodos.Add(novaAgendaP2);
                        }
                    }
                }


                foreach (var novaAgenda in listaAgendaPeriodos)
                {
                    _alocacaoRepository.Deletar(novaAgenda);
                    sucesso = true;
                }

                msgRetorno = "Registro excluído!";
            }

            dataAgenda = dataAgenda.AddDays(1);
        }
        return new Tuple<bool, string>(sucesso, msgRetorno);
    }

    public void ValidarInclusao(AlocacaoViewModel entidade)
    {
        if (entidade.ListaConsultoresId == null || entidade.ListaConsultoresId.Count() == 0)
        {
            throw new DomainException("Para cadastrar a alocação selecione pelo menos um consultor");
        }
        if (!Validacao.VerificaData(entidade.DataIniciotexto) || !Validacao.VerificaData(entidade.DataFimtexto))
        {
            throw new DomainException("Verifique os campos data de início e/ou data final");
        }

        if (Convert.ToDateTime(entidade.DataIniciotexto) > Convert.ToDateTime(entidade.DataFimtexto))
        {
            throw new DomainException("O campo data de início deve ser menor que o campo data final");
        }

        if (entidade.Periodo1 == false && entidade.Periodo2 == false)
        {
            throw new DomainException("Para cadastrar a alocação informe pelo menos um período");
        }
        if (entidade.Reserva == false && (entidade.ListaCentroCustos == null || entidade.ListaCentroCustos.Count() == 0 ))
        {
            throw new DomainException("Para cadastrar a alocação informe pelo menos um centro de custo");
        }
        if (entidade.Feriado == false && (entidade.ListaDiasSemana == null || entidade.ListaDiasSemana.Count == 0))
        {
            throw new DomainException("Para cadastrar a alocação informe pelo menos um dia da semana(seg,ter,quarta..)");
        }
    }

    public void ValidarExclusao(AlocacaoViewModel entidade)
    {
        if (entidade.ListaConsultoresId == null || entidade.ListaConsultoresId.Count() == 0)
        {
            throw new DomainException("Selecione ao menos um consultor para a exclusão.");
        }
        if (entidade.ListaCentroCustos == null || entidade.ListaCentroCustos.Count() == 0)
        {
            throw new DomainException("Selecione ao menos um centro de custo para a exclusão.");
        }
        if (!Validacao.VerificaData(entidade.DataIniciotexto) || !Validacao.VerificaData(entidade.DataFimtexto))
        {
            throw new DomainException("Os campos de data inicio e data final devem estar preenchidos.");
        }

        if (Convert.ToDateTime(entidade.DataIniciotexto) > Convert.ToDateTime(entidade.DataFimtexto))
        {
            throw new DomainException("O campo Data de Início deve ser menor que o Campo Data Final");
        }

        if (entidade.Periodo1 == false && entidade.Periodo2 == false)
        {
            throw new DomainException("Para Cadastrar a Alocação informe pelo menos um Período");
        }
    }

    #endregion


    #region  Listagem Alocação
    public void ValidarPesquisarAlocacao(AgendaPesquisaViewModel parametroAlocacao)
    {
        if (parametroAlocacao.ListaConsultoresId == null || parametroAlocacao.ListaConsultoresId.Count() == 0)
        {
            throw new DomainException("Para realizar a pesquisa selecione pelo menos um consultor");
        }
        if (parametroAlocacao.ListaEspecialidades == null || parametroAlocacao.ListaEspecialidades.Count() == 0)
        {
            throw new DomainException("Para realizar a pesquisa selecione pelo menos uma especialidade");
        }
        if (parametroAlocacao.ListaCentroCustos == null || parametroAlocacao.ListaCentroCustos.Count() == 0)
        {
            throw new DomainException("Para realizar a pesquisa selecione pelo menos um centro de custo");
        }
        if (parametroAlocacao.DataInicio == DateTime.MinValue || parametroAlocacao.DataFim == DateTime.MinValue)
        {
            throw new DomainException("Os campos data de início e data final são obrigatórios");
        }
        Validacao.ValidaLimitePermetidoData(parametroAlocacao.DataInicio);
        Validacao.ValidaLimitePermetidoData(parametroAlocacao.DataFim);

        if (parametroAlocacao.DataInicio > parametroAlocacao.DataFim)
        {
            throw new DomainException("O campo data de início deve ser menor que o campo data final");
        }



    }
    private void MontarCalendario(AgendaPesquisaViewModel parametroAlocacao)
    {
        DateTime dataInicio = RetornaDataInicio(parametroAlocacao.DataInicio);
        DateTime dataFinal = RetornaDataLimite(parametroAlocacao.DataFim);

        listaSabado = GerarListaDeDatas(dataInicio, dataFinal);
        listaDomingo = GerarListaDeDatas(dataInicio.AddDays(1), dataFinal);
        listaSegunda = GerarListaDeDatas(dataInicio.AddDays(2), dataFinal);
        listaterca = GerarListaDeDatas(dataInicio.AddDays(3), dataFinal);
        listaQuarta = GerarListaDeDatas(dataInicio.AddDays(4), dataFinal);
        listQuinta = GerarListaDeDatas(dataInicio.AddDays(5), dataFinal);
        listaSexta = GerarListaDeDatas(dataInicio.AddDays(6), dataFinal);

    }

    private bool VerificaCentroDeCusto(Guid id)
    {
        bool retorno = false;
        var centroDeCusto = listaCentroDeCusto.Where(c => c.Id == id).FirstOrDefault();
        if (centroDeCusto.TipoCentroCustoId == _TipoCentroDeCustoInterno)
        {
            retorno = true;
        }
        else
        {
            if (centroDeCusto != null)
            {
                foreach (var item in listaEstiloCentroDeCusto)
                {
                    if (centroDeCusto.Nome.ToUpper().Contains(item.Chave.ToUpper()))
                    {
                        retorno = true;
                        break;
                    }
                }
            }
        }
        return retorno;
    }

    private string RetornaTipoCentroDeCusto(Guid id)
    {
        string retorno = "";
        var centroDeCusto = listaCentroDeCusto.Where(c => c.Id == id).FirstOrDefault();
        if (centroDeCusto != null)
        {
            foreach (var item in listaEstiloCentroDeCusto)
            {
                if (centroDeCusto.Nome.EndsWith(item.Chave))
                {
                    retorno = item.Chave;
                    break;
                }
            }
            if (retorno == string.Empty)
            {
                if (centroDeCusto.TipoCentroCustoId == _TipoCentroDeCustoInterno)
                {
                    retorno = "ELOPADRAO";
                }
            }

        }

        return retorno;
    }

    private PlanejamentoAlocacaoViewModel MontarListaAlocacao(List<Usuario> listaConsultores, List<Agenda> listaAlocacaoBanco, AgendaPesquisaViewModel parametroAlocacao)
    {
        PlanejamentoAlocacaoViewModel retorno = new PlanejamentoAlocacaoViewModel();
        retorno.Sucesso = true;
        List<AlocacaoAgendaViewModel> listaAgenda = new List<AlocacaoAgendaViewModel>();
        if (listaConsultores == null || listaConsultores.Count() == 0)
        {
            retorno.Sucesso = false;
            retorno.Mensagem = "Sua busca não retornou dados";

        }
        else
        {

            listaTipoContratacao = _tipoContratacaoRepository.Listar().ToList();
            listaEstiloCentroDeCusto = RetonaListaCentroDeCustoEstilizacao();
            listaCentroDeCusto = _centroDeCustoRepositorio.Listar().ToList();
            var ListaUsuariosEspecialidades = this._usuarioEspecialidadeRepository.Listar().ToList();
            var ListEspecialidades = this._especialidadeRepository.Listar().ToList();
            var ListaUsuariosComConflito = this.ListaConsultoresComConflitosPorPeriodo(parametroAlocacao.DataInicio, parametroAlocacao.DataFim);


            int index = 1;
            foreach (var item in listaConsultores)
            {
                AlocacaoAgendaViewModel alocacao = new AlocacaoAgendaViewModel();
                alocacao.ConsultorNome = item.Nome;
                alocacao.ConsultorId = item.Id;
                var consultor = item;
                alocacao.Especialidade = RetornaListaEspecialdiadesTexto(alocacao.ConsultorId, ListaUsuariosEspecialidades, ListEspecialidades);
                alocacao.Ordem = index;
                alocacao.zebrar = index % 2 == 0 ? true : false;
                alocacao.ClasseEstiloTabela = alocacao.zebrar == true ? "ConsultordadosZebraPar" : "ConsultordadosZebraImpar";

                if (ListaUsuariosComConflito.Count(x => x.id == alocacao.ConsultorId) > 0)
                {
                    alocacao.PossuiConflito = true;
                    alocacao.ClasseEstiloConflito = "bg-ConsultorComConflito";
                }



                index++;

                alocacao.ListaSabados1 = GeraAgenda(listaSabado, 1, listaAlocacaoBanco, consultor, true, "Sáb");
                alocacao.ListaSabados2 = GeraAgenda(listaSabado, 2, listaAlocacaoBanco, consultor, true, "Sáb");

                alocacao.ListaDomingos1 = GeraAgenda(listaDomingo, 1, listaAlocacaoBanco, consultor, true, "Dom");
                alocacao.ListaDomingos2 = GeraAgenda(listaDomingo, 2, listaAlocacaoBanco, consultor, true, "Dom");

                alocacao.ListaSegundas1 = GeraAgenda(listaSegunda, 1, listaAlocacaoBanco, consultor);
                alocacao.ListaSegundas2 = GeraAgenda(listaSegunda, 2, listaAlocacaoBanco, consultor);

                alocacao.ListaTercas1 = GeraAgenda(listaterca, 1, listaAlocacaoBanco, consultor);
                alocacao.ListaTercas2 = GeraAgenda(listaterca, 2, listaAlocacaoBanco, consultor);

                alocacao.ListaQuartas1 = GeraAgenda(listaQuarta, 1, listaAlocacaoBanco, consultor);
                alocacao.ListaQuartas2 = GeraAgenda(listaQuarta, 2, listaAlocacaoBanco, consultor);

                alocacao.ListaQuintas1 = GeraAgenda(listQuinta, 1, listaAlocacaoBanco, consultor);
                alocacao.ListaQuintas2 = GeraAgenda(listQuinta, 2, listaAlocacaoBanco, consultor);

                alocacao.ListaSextas1 = GeraAgenda(listaSexta, 1, listaAlocacaoBanco, consultor);
                alocacao.ListaSextas2 = GeraAgenda(listaSexta, 2, listaAlocacaoBanco, consultor);

                listaAgenda.Add(alocacao);
            }


            retorno.DataInicioCabecalho = RetornaDataInicio(parametroAlocacao.DataInicio);
            retorno.DataTerminoCabecalho = RetornaDataLimite(parametroAlocacao.DataFim);

            retorno.Alocacao = listaAgenda;
        }
        return retorno;


    }
    public PlanejamentoAlocacaoViewModel PesquisarAlocacao(AgendaPesquisaViewModel parametroAlocacao)
    {

        this.ValidarPesquisarAlocacao(parametroAlocacao);


        this.MontarCalendario(parametroAlocacao);

        _TipoCentroDeCustoInterno = parametroAlocacao.TipoCentroDeCustoInterno;
        var UsuarioEspecialidade = this.FiltraUsuariosEspecialdiades(parametroAlocacao);
        var listaConsultoresId = UsuarioEspecialidade.Select(x => x.Id).Distinct().ToList();
        var Consultores = TransformaListaEmString(listaConsultoresId);
        IEnumerable<Guid> listaConsultoresIdComAgenda = new List<Guid>();


        var listaAlocacaoBanco = _alocacaoRepository.PesquisaAlocacao(parametroAlocacao.DataInicio, parametroAlocacao.DataFim, Consultores, "", false);
        if (parametroAlocacao.TodosCentroDeCustoSelecionados == false)
        {
            var listaFiltro = listaAlocacaoBanco.Where(x => x.CentroCustoId != null && parametroAlocacao.ListaCentroCustos.Contains((Guid)x.CentroCustoId));
            listaConsultoresIdComAgenda = listaFiltro.Select(x => x.UsuarioId).Distinct();
            listaAlocacaoBanco = listaAlocacaoBanco.Where(x => listaConsultoresIdComAgenda.Contains(x.UsuarioId)).ToList();
        }
        var listaConsultoresAlocacao = listaAlocacaoBanco.Select(x => x.UsuarioId).Distinct();
        if (parametroAlocacao.TodosConsultoresSelecionados == false && parametroAlocacao.TodosCentroDeCustoSelecionados == false)
        {
            listaConsultoresId = listaConsultoresId.Where(x => listaConsultoresAlocacao.Contains(x)).ToList();
        }


        List<Usuario> listaConsultores = new List<Usuario>();
        if (parametroAlocacao.TodosCentroDeCustoSelecionados == false)
        {
            listaConsultores = _usuariorepository.Listar().Where(x => listaConsultoresId.Contains(x.Id) && listaConsultoresIdComAgenda.Contains(x.Id)).OrderBy(x => x.Nome).ToList();
        }
        else
        {
            listaConsultores = _usuariorepository.Listar().Where(x => listaConsultoresId.Contains(x.Id)).OrderBy(x => x.Nome).ToList();
        }

        var retorno = MontarListaAlocacao(listaConsultores, listaAlocacaoBanco, parametroAlocacao);


        return retorno;
    }
    private string RetornaClasseConsultorSemAgenda(Guid tipoContratatcao)
    {
        string retorno = "";


        var tipoContratcao = listaTipoContratacao.Where(t => t.Id == tipoContratatcao).FirstOrDefault();

        if (tipoContratcao != null)
        {
            if (tipoContratcao.Nome.ToUpper().Contains("HORISTA"))
            {
                retorno = "bg-ConsultorSemAlocacaoCinza";
            }
            else if (tipoContratcao.Nome.ToUpper().Contains("SOB DEMANDA"))
            {
                retorno = "bg-ConsultorSemAlocacaoBranco";
            }
            else
            {
                retorno = "bg-ConsultorSemAlocacaoVermelho";
            }


        }
        return retorno;
    }


    private string RetornaListaEspecialdiadesTexto(Guid ConsultorId, List<UsuarioEspecialidade> listausuarioEspecialidade, List<Especialidade> listaEspecialdiadeParametro)
    {
        string Retorno = "";

        var EspecialdiadesUsuarios = listausuarioEspecialidade.Where(x => x.UsuarioId == ConsultorId).ToList();
        List<Guid> EspecialdiadesId = EspecialdiadesUsuarios.Select(x => x.EspecialidadeId).ToList();

        var ListaEspecialdiades = listaEspecialdiadeParametro.Where(x => EspecialdiadesId.Contains(x.Id)).ToList();

        if (ListaEspecialdiades != null && ListaEspecialdiades.Count() > 0)
        {
            int posicaoFinal = ListaEspecialdiades.Count();

            for (int i = 0; i < posicaoFinal; i++)
            {
                if (i + 1 == posicaoFinal)
                {
                    Retorno += ListaEspecialdiades[i].UnidadeServico + "/" + ListaEspecialdiades[i].Competencia;
                }
                else
                {
                    Retorno += ListaEspecialdiades[i].UnidadeServico + "/" + ListaEspecialdiades[i].Competencia + " - ";
                }
            }

        }
        return Retorno;

    }
    private AgendaViewModel RetornaAgendaFormatada(DateTime data, int periodo, Agenda AgendaBanco, Usuario consultor, bool FinalDeSemana = false, string DiaSemana = "")
    {
        var agenda = new AgendaViewModel();
        agenda.DataAgenda = data;
        agenda.Periodo = periodo;

        if (AgendaBanco != null)
        {
            agenda.ValorExibicao = AgendaBanco.CentroCustoDescricao;
            if (AgendaBanco.Feriado == 1)
            {
                agenda.Classe = ScheduleResorce.RetornaValor("Feriado");
                agenda.ValorExibicao = agenda.ValorExibicao == string.Empty ? "Feriado" : agenda.ValorExibicao;
            }
            else if (AgendaBanco.CentroCustoId != null && this.VerificaCentroDeCusto((Guid)AgendaBanco.CentroCustoId) == true)
            {
                var TipoCentroCusto = RetornaTipoCentroDeCusto((Guid)AgendaBanco.CentroCustoId);
                agenda.Classe = ScheduleResorce.RetornaValor(TipoCentroCusto);
            }
            else if (AgendaBanco.TipoAgenda == 0)
            {
                agenda.Classe = ScheduleResorce.RetornaValor("AlocacaoReserva");
                agenda.ValorExibicao = agenda.ValorExibicao == string.Empty ? "Reserva" : agenda.ValorExibicao;
            }
            else
            {
                agenda.Classe = ScheduleResorce.RetornaValor("ConsultorAlocacao");
            }

        }
        else if (AgendaBanco == null)
        {
            if (FinalDeSemana == true)
            {

                agenda.ValorExibicao = DiaSemana;
                agenda.Classe = ScheduleResorce.RetornaValor("ConsultorSemAlocacaoFinalDeSemana");
            }
            else
            {
                agenda.ValorExibicao = "Sem Alocação";
                agenda.Classe = RetornaClasseConsultorSemAgenda(((Guid)consultor.TipoContratacaoId));
            }

        }
        return agenda;


    }
    private List<AgendaViewModel> GeraAgenda(List<DateTime> lista, int periodo, List<Agenda> listaAgenda, Usuario consultor, bool FinalDeSemana = false, string DiaSemana = "")
    {
        List<AgendaViewModel> retorno = new List<AgendaViewModel>();
        try
        {

            foreach (var item in lista)
            {

                var agendaBanco = listaAgenda.Where(x => x.UsuarioId == consultor.Id && x.DataAgenda.Date == item.Date && x.Periodo == periodo).FirstOrDefault();
                retorno.Add(this.RetornaAgendaFormatada(item, periodo, agendaBanco, consultor, FinalDeSemana, DiaSemana));

            }
        }
        catch (Exception ex)
        {

            throw;
        }
        return retorno;
    }
    private List<DateTime> GerarListaDeDatas(DateTime datainicio, DateTime dataFim)
    {
        List<DateTime> lista = new List<DateTime>();


        while (datainicio <= dataFim)
        {
            lista.Add(datainicio);
            datainicio = datainicio.AddDays(7);

        }

        return lista;
    }
    public DateTime RetornaDataInicio(DateTime data)
    {
        DateTime Datainicio = new DateTime(); ;
        if (data.DayOfWeek == DayOfWeek.Saturday)
        {
            Datainicio = data;
        }
        else
        {
            var numero = (int)data.DayOfWeek;
            Datainicio = data.AddDays(-numero).AddDays(-1);
        }


        return Datainicio;
    }
    public DateTime RetornaDataLimite(DateTime data)
    {
        DateTime DataLimite;
        if (data.DayOfWeek == DayOfWeek.Friday)
        {
            DataLimite = data;
        }
        else if (data.DayOfWeek == DayOfWeek.Saturday)
        {
            DataLimite = data.AddDays(6);
        }

        else
        {
            var numero = (int)data.DayOfWeek;
            DataLimite = data.AddDays(-numero).AddDays(5);
        }
        return DataLimite;
    }
    private string TransformaListaEmString(List<Guid> Lista)
    {
        string retorno = string.Empty;

        var total = Lista.Count;
        for (int i = 0; i < total; i++)
        {

            retorno += Lista[i] + (total > i ? ";" : "");
        }
        return retorno;
    }
    private List<Usuario> FiltraUsuariosEspecialdiades(AgendaPesquisaViewModel parmRelatorio)
    {

        if (parmRelatorio.ListaConsultoresId == null || parmRelatorio.ListaConsultoresId.Count == 0)
        {
            throw new DomainException("Para realizar a busca selecione pelo menos um consultor");
        }
        else if (parmRelatorio.ListaEspecialidades == null || parmRelatorio.ListaEspecialidades.Count == 0)
        {
            throw new DomainException("Para realizar a busca selecione pelo menos uma especialidade");
        }
        else if (parmRelatorio.ListaCentroCustos == null || parmRelatorio.ListaCentroCustos.Count == 0)
        {
            throw new DomainException("Para realizar a busca selecione pelo menos um centro de custo");
        }

        IQueryable<UsuarioEspecialidade> QueryUsuarioEspecialidade = _usuarioEspecialidadeRepository.Listar();

        //if (parmRelatorio.ListaConsultores != null)
        //    QueryUsuarioEspecialidade = _usuarioEspecialidadeRepository.Listar().Where(e => parmRelatorio.ListaConsultores.Contains(e.UsuarioId));

        if (parmRelatorio.ListaEspecialidades != null)
            QueryUsuarioEspecialidade = _usuarioEspecialidadeRepository.Listar().Where(e => parmRelatorio.ListaEspecialidades.Contains(e.EspecialidadeId));

        var UsuarioEspecialidade = QueryUsuarioEspecialidade.ToList();
        if (UsuarioEspecialidade == null || UsuarioEspecialidade.Count() == 0)
        {
            throw new DomainException("Não foram encontrados consultor(es) com a(s) especialidade(s) informada(s)");
        }

        var listaConsultoresId = UsuarioEspecialidade.Select(x => x.UsuarioId).Distinct().ToList();


        IQueryable<Usuario> QueryUsuario = _usuariorepository.Listar();
        if (parmRelatorio.EmpresaId != null && parmRelatorio.EmpresaId != Guid.Empty)
            QueryUsuario = QueryUsuario.Where(x => x.EmpresaId == parmRelatorio.EmpresaId);

        if (listaConsultoresId != null)
        {
            QueryUsuario = QueryUsuario.Where(x => listaConsultoresId.Contains(x.Id));
        }


        if (parmRelatorio.ListaConsultoresId != null)
        {
            QueryUsuario = QueryUsuario.Where(x => parmRelatorio.ListaConsultoresId.Contains(x.Id));
        }


        if (parmRelatorio.DataInicio != DateTime.MinValue)
        {
            QueryUsuario = QueryUsuario.Where(x => (x.DataTerminoContrato == null) || (x.DataTerminoContrato != null && x.DataTerminoContrato >= parmRelatorio.DataInicio));
        }


        var listaUsuario = QueryUsuario.ToList();


        return listaUsuario;
    }
    #endregion

    #region Gestão de Conflitos
    public List<UsuarioViewModel> ListaConsultoresComConflitosPorPeriodo(DateTime DataInicial, DateTime DataFinal)
    {
        var lista = _alocacaoRepository.ListaConsultoresComConflitos(DataInicial, DataFinal);
        var resultado = _mapper.Map<List<UsuarioViewModel>>(lista);
        return resultado;

    }
    private List<Usuario> FiltraUsuariosEspecialdiadesConflito(PesquisaConflitoViewModel parametroPesquisa)
    {


        IQueryable<UsuarioEspecialidade> QueryUsuarioEspecialidade = _usuarioEspecialidadeRepository.Listar();

        QueryUsuarioEspecialidade = _usuarioEspecialidadeRepository.Listar().Where(e => parametroPesquisa.ListaConsultoresId.Contains(e.UsuarioId));

        if (parametroPesquisa.ListaEspecialidades != null)
            QueryUsuarioEspecialidade = _usuarioEspecialidadeRepository.Listar().Where(e => parametroPesquisa.ListaEspecialidades.Contains(e.EspecialidadeId));

        var UsuarioEspecialidade = QueryUsuarioEspecialidade.ToList();
        if (UsuarioEspecialidade == null || UsuarioEspecialidade.Count() == 0)
        {
            throw new DomainException("Não foram encontrados consultor(es) com a(s) especialidade(s) informada(s)");
        }

        var listaConsultoresId = UsuarioEspecialidade.Select(x => x.UsuarioId).Distinct().ToList();


        IQueryable<Usuario> QueryUsuario = _usuariorepository.Listar();
        if (parametroPesquisa.EmpresaId != null && parametroPesquisa.EmpresaId != Guid.Empty)
            QueryUsuario = QueryUsuario.Where(x => x.EmpresaId == parametroPesquisa.EmpresaId);

        if (listaConsultoresId != null)
        {
            QueryUsuario = QueryUsuario.Where(x => listaConsultoresId.Contains(x.Id));
        }


        if (parametroPesquisa.ListaConsultoresId != null)
        {
            QueryUsuario = QueryUsuario.Where(x => parametroPesquisa.ListaConsultoresId.Contains(x.Id));
        }


        var listaUsuario = QueryUsuario.ToList();


        return listaUsuario;
    }
    public RespostaPesquisaConflitoViewModel ListaConsultoresConflitos(PesquisaConflitoViewModel parametroPesquisa)
    {
        RespostaPesquisaConflitoViewModel retorno = new RespostaPesquisaConflitoViewModel();
        this.ValidarPesquisarConflitos(parametroPesquisa);

        var UsuarioEspecialidade = this.FiltraUsuariosEspecialdiadesConflito(parametroPesquisa);
        var listaConsultoresId = UsuarioEspecialidade.Select(x => x.Id).Distinct().ToList();
        var lista = _alocacaoRepository.ListaConsultoresComConflitos(parametroPesquisa.DataInicio, parametroPesquisa.DataFim);
        lista = lista.Where(x => listaConsultoresId.Contains(x.Id)).ToList();

        var resultado = _mapper.Map<List<UsuarioViewModel>>(lista);

        if (resultado.Count() > 0)
        {
            retorno.DataPesquisaInicial = parametroPesquisa.DataInicio;
            retorno.DataPesquisaFinal = parametroPesquisa.DataFim;

            var listaUsuarios = _usuariorepository.Listar();
            retorno.Sucesso = true;
            foreach (var item in resultado)
            {
                var gestorImediato = listaUsuarios.Where(x => x.Id == item.GestorImediatoId).FirstOrDefault();
                if (gestorImediato != null)
                {
                    item.GestorImediatoNome = gestorImediato.Nome;
                }
            }



        }
        else
        {
            retorno.Sucesso = false;
            retorno.Mensagem = "Não foram encontrados consultores com conflitos de agenda para os critérios selecionados";
        }
        retorno.UsuariosConflitos = resultado;


        return retorno;
    }
    private GestaoConflitoViewModel RetornaBasciosDadosConsultor(Guid ConsultorId)
    {
        GestaoConflitoViewModel retorno = new GestaoConflitoViewModel();
        var Consultor = _usuariorepository.Listar().Where(c => c.Id == ConsultorId).FirstOrDefault();
        if (Consultor != null)
        {
            retorno.ConsultorNome = Consultor.Nome;
            retorno.EmailConsultor = Consultor.Email;
            retorno.TelefoneConsultor = "(" + Consultor.DDD1 + ")" + Consultor.Telefone1;

            var GestorImediato = _usuariorepository.Listar().Where(c => c.Id == Consultor.GestorImediatoId).FirstOrDefault();
            if (GestorImediato != null)
            {
                retorno.GestorImediato = GestorImediato.Nome;
                retorno.EmailGestorImediato = GestorImediato.Email;
                retorno.TelefoneGestorImediato = "(" + GestorImediato.DDD1 + ")" + GestorImediato.Telefone1;
            }
        }

        return retorno;
    }
    public GestaoConflitoViewModel ListaConflitosPorConsultor(Guid ConsultorId, DateTime DataInicial, DateTime DataFinal)
    {
        GestaoConflitoViewModel resultado = null;

        var lista = _alocacaoRepository.ListaConflitosPorConsultor(ConsultorId, DataInicial, DataFinal);
        if (lista != null && lista.Count > 0)
        {
            resultado = RetornaBasciosDadosConsultor(ConsultorId);

            var listaDatas = lista.Select(x => x.DataAgenda).Distinct();

            foreach (var item in listaDatas)
            {
                ConflitoViewModel conflito = new ConflitoViewModel();
                conflito.Data = item;
                var listaAgendas = lista.Where(x => x.DataAgenda == item);

                foreach (var agenda in listaAgendas)
                {
                    var agendaConflito = new AgendaConflitoViewModel();
                    agendaConflito.id = agenda.Id;
                    agendaConflito.CentroCusto = agenda.CentroCustoDescricao;
                    agendaConflito.DataAlteracao = agenda.DataAlteracao;
                    agendaConflito.UsuarioCriacao = agenda.Usuario;
                    agendaConflito.Periodo = agenda.Periodo;
                    if (agendaConflito.CentroCusto == string.Empty)
                    {
                        agendaConflito.CentroCusto = agenda.Feriado == 1 ? "Feriado" : "Reserva";
                    }

                    conflito.AgendaConflitos.Add(agendaConflito);
                }
                resultado.Conflitos.Add(conflito);

            }
        }
        return resultado;

    }


    public void ValidarPesquisarConflitos(PesquisaConflitoViewModel parametroPesquisa)
    {
        if (parametroPesquisa.ListaConsultoresId == null || parametroPesquisa.ListaConsultoresId.Count() == 0)
        {
            throw new DomainException("Para realizar a pesquisa selecione pelo menos um consultor");
        }
        if (parametroPesquisa.ListaEspecialidades == null || parametroPesquisa.ListaEspecialidades.Count() == 0)
        {
            throw new DomainException("Para realizar a pesquisa selecione pelo menos uma especialidade");
        }

        if (parametroPesquisa.DataInicio == DateTime.MinValue || parametroPesquisa.DataFim == DateTime.MinValue)
        {
            throw new DomainException("Os campos data de início e data final são obrigatórios");
        }

        Validacao.ValidaLimitePermetidoData(parametroPesquisa.DataInicio);
        Validacao.ValidaLimitePermetidoData(parametroPesquisa.DataFim);

        if (parametroPesquisa.DataInicio > parametroPesquisa.DataFim)
        {
            throw new DomainException("O campo data de início deve ser menor que o campo data final");
        }



    }

    public bool ExcluirConflitos(List<Guid> ListaExclusao)
    {
        return _alocacaoRepository.ExcluirConflitos(ListaExclusao);
    }

    #endregion
}





