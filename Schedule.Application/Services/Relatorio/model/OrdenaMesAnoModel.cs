﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.Services.Relatorio.model
{
   public class OrdenaMesAnoModel
    {
        public DateTime Data { get; set; }
        public int Ordem { get; set; }
    }
}
