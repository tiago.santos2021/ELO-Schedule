﻿using AutoMapper;
using Schedule.Application.Interfaces.Services.Relatorio;
using Schedule.Application.Services.Relatorio.model;
using Schedule.Application.ViewModel.Relatorio;
using Schedule.Application.ViewModel.Visao;
using Schedule.Domain;
using Schedule.Domain.Entities;
using Schedule.Domain.Entities.Estrutura;
using Schedule.Domain.Interfaces.Repository;
using Schedule.Domain.Interfaces.Repository.Estrutura;
using Schedule.Domain.Interfaces.Repository.Relatorio;
using Schedule.Domain.Relatorios;
using Schedule.Util.Formatacao;
using Schedule.Util.Validacao;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Schedule.Application.Services.Relatorios
{
    public class RelatorioService : IRelatorioService
    {
        private readonly IUsuarioEspecialidadeRepository _usuarioEspecialidadeRepository;
        private readonly IRelatorioRepository _relatorioRepository;
        private readonly IUsuarioRepository _usuariorepository;
        private readonly IEspecialidadeRepository _especialidadeRepository;
        private readonly IMapper _mapper;
        public RelatorioService(IUsuarioRepository usuariorepository, IUsuarioEspecialidadeRepository usuarioEspecialidadeRepository, IRelatorioRepository RelatorioRepository, IEspecialidadeRepository EspecialidadeRepository, IMapper mapper)
        {
            this._usuarioEspecialidadeRepository = usuarioEspecialidadeRepository;
            this._relatorioRepository = RelatorioRepository;
            this._usuariorepository = usuariorepository;
            this._especialidadeRepository = EspecialidadeRepository;
            this._mapper = mapper;
        }
        private string TransformaListaEmString(List<Guid> Lista)
        {
            string retorno = string.Empty;

            var totalConsultores = Lista.Count;
            for (int i = 0; i < totalConsultores; i++)
            {

                retorno += Lista[i] + (totalConsultores > i ? ";" : "");
            }
            return retorno;
        }
        private List<Usuario> FiltraUsuariosEspecialdiades(SolicitacaoImpressaoRelPlanejadoViewModel parmRelatorio)
        {

            if (parmRelatorio.ListaConsultores == null || parmRelatorio.ListaConsultores.Count == 0)
            {
                throw new DomainException("Para Realizar a Busca Selecione pelo menos um Consultor");
            }
            else if (parmRelatorio.ListaEspecialidades == null || parmRelatorio.ListaEspecialidades.Count == 0)
            {
                throw new DomainException("Para Realizar a Busca Selecione pelo menos uma Especialidade");
            }
            else if (parmRelatorio.ListaCentroCustos == null || parmRelatorio.ListaCentroCustos.Count == 0)
            {
                throw new DomainException("Para Realizar a Busca Selecione pelo menos um Centro de Custo");
            }
            else if (parmRelatorio.ListaEmpresas == null || parmRelatorio.ListaEmpresas.Count == 0)
            {
                throw new DomainException("Para Realizar a Busca Selecione pelo menos uma Empresa");
            }
            if (parmRelatorio.DataInicio == DateTime.MinValue || parmRelatorio.DataFinal == DateTime.MinValue)
            {
                throw new DomainException("os campos data de início e data final são obrigatórios");
            }

            Validacao.ValidaLimitePermetidoData(parmRelatorio.DataInicio);
            Validacao.ValidaLimitePermetidoData(parmRelatorio.DataFinal);

            if (parmRelatorio.DataInicio > parmRelatorio.DataFinal)
            {
                throw new DomainException("O campo data de início deve ser menor que o campo data final");
            }

            IQueryable<UsuarioEspecialidade> QueryUsuarioEspecialidade = _usuarioEspecialidadeRepository.Listar();

            //if (parmRelatorio.ListaConsultores != null)
            //    QueryUsuarioEspecialidade = _usuarioEspecialidadeRepository.Listar().Where(e => parmRelatorio.ListaConsultores.Contains(e.UsuarioId));

            if (parmRelatorio.ListaEspecialidades != null)
                QueryUsuarioEspecialidade = _usuarioEspecialidadeRepository.Listar().Where(e => parmRelatorio.ListaEspecialidades.Contains(e.EspecialidadeId));

            var UsuarioEspecialidade = QueryUsuarioEspecialidade.ToList();
            if (UsuarioEspecialidade == null || UsuarioEspecialidade.Count() == 0)
            {
                throw new DomainException("Não foram Encontrados Consultores Com a(s) Especialidade Informadas");
            }

            var listaConsultoresId = UsuarioEspecialidade.Select(x => x.UsuarioId).Distinct().ToList();


            IQueryable<Usuario> QueryUsuario = _usuariorepository.Listar();

            if (parmRelatorio.ListaEmpresas != null && parmRelatorio.ListaEmpresas.Count > 0)
                QueryUsuario = QueryUsuario.Where(x => parmRelatorio.ListaEmpresas.Contains((Guid)x.EmpresaId));

            if (listaConsultoresId != null)
            {
                QueryUsuario = QueryUsuario.Where(x => listaConsultoresId.Contains(x.Id));
            }


            if (parmRelatorio.ListaConsultores != null)
            {
                QueryUsuario = QueryUsuario.Where(x => parmRelatorio.ListaConsultores.Contains(x.Id));
            }


            var listaUsuario = QueryUsuario.ToList();


            return listaUsuario;
        }
        private List<RelPlanejadoDataViewModel> MapearListaPlanejamentoViewModel(List<VisaoRelatorioPlanejado> listaVisaoRelatorioPlanejado, int ValorPeriodo)
        {
            List<OrdenaMesAnoModel> listaOrdemMesAno = new List<OrdenaMesAnoModel>();
            var ListaUsuariosEspecialidades = this._usuarioEspecialidadeRepository.Listar().ToList();
            var ListEspecialidades = this._especialidadeRepository.Listar().ToList();

            List<RelPlanejadoDataViewModel> lista = new List<RelPlanejadoDataViewModel>();
            
            if (listaVisaoRelatorioPlanejado != null && listaVisaoRelatorioPlanejado.Count > 0)
            {
               

                var ordem = 1;
                foreach (var item in listaVisaoRelatorioPlanejado)
                {
                    RelPlanejadoDataViewModel relPlanejado = new RelPlanejadoDataViewModel();


                    //var ordemPrData = listaOrdemMesAno.FirstOrDefault(x => x.Data == new DateTime(item.Ano, item.Mes, 1));
                    relPlanejado.Ordem = ordem;
                    relPlanejado.Mes = item.Mes;
                    relPlanejado.Ano = item.Ano;
                    relPlanejado.Nome = item.Nome;
                    relPlanejado.Quantidade = item.Quantidade;
                    relPlanejado.CentroCustoDescricao = item.CentroCustoDescricao;
                    relPlanejado.FuncionarioId = item.FuncionarioId;
                    relPlanejado.Empresa = item.Empresa;
                    relPlanejado.Login = item.Login;
                    relPlanejado.Especialidade = RetornaListaEspecialdiadesTexto(relPlanejado.FuncionarioId, ListaUsuariosEspecialidades, ListEspecialidades);
                    relPlanejado.MesAno = this.ExibirMesPorExtenso(item.Mes, item.Ano);
                    relPlanejado.Quantidade = relPlanejado.Quantidade * ValorPeriodo;
                    relPlanejado.Senioridade = item.Senioridade;
                    relPlanejado.TipoContratacao = item.TipoContratacao;
                    relPlanejado.Email = item.Email;
                    relPlanejado.Gestor = item.Gestor;
                    relPlanejado.DataInicioContrato = item.DataInicioContrato;
                    relPlanejado.DataInicioContratoExibicao = "";
                    if (relPlanejado.DataInicioContrato != null && relPlanejado.DataInicioContrato != DateTime.MinValue)
                    {
                        relPlanejado.DataInicioContratoExibicao = ((DateTime)relPlanejado.DataInicioContrato).ToString("dd/MM/yyyy");
                    }
                    relPlanejado.DataFimContrato = item.DataFimContrato;
                    relPlanejado.DataFimContratoExibicao = "";
                    if (relPlanejado.DataFimContrato != null && relPlanejado.DataFimContrato != DateTime.MinValue)
                    {
                        relPlanejado.DataFimContratoExibicao = ((DateTime)relPlanejado.DataFimContrato).ToString("dd/MM/yyyy");
                    }

                    ordem++;
                    lista.Add(relPlanejado);

                }
            }
            lista = lista.OrderBy(x => x.Empresa).ThenBy(x => x.Nome).ThenBy(x => x.Ano).ThenBy(x => x.Mes).ToList();
            return lista;
        }
        public string ExibirMesPorExtenso(int Mes, int Ano)
        {

            string retorno = Formatacao.ExibirMesPorExtenso(Mes) + "/" + Ano.ToString("D2");

            return retorno;

        }
        private string RetornaListaEspecialdiadesTexto(Guid ConsultorId, List<UsuarioEspecialidade> listausuarioEspecialidade, List<Especialidade> listaEspecialdiadeParametro)
        {
            string Retorno = "";

            var EspecialdiadesUsuarios = listausuarioEspecialidade.Where(x => x.UsuarioId == ConsultorId).ToList();
            List<Guid> EspecialdiadesId = EspecialdiadesUsuarios.Select(x => x.EspecialidadeId).ToList();

            var ListaEspecialdiades = listaEspecialdiadeParametro.Where(x => EspecialdiadesId.Contains(x.Id)).ToList();

            if (ListaEspecialdiades != null && ListaEspecialdiades.Count() > 0)
            {
                int posicaoFinal = ListaEspecialdiades.Count();

                for (int i = 0; i < posicaoFinal; i++)
                {
                    if (i + 1 == posicaoFinal)
                    {
                        Retorno += ListaEspecialdiades[i].UnidadeServico + "/" + ListaEspecialdiades[i].Competencia;
                    }
                    else
                    {
                        Retorno += ListaEspecialdiades[i].UnidadeServico + "/" + ListaEspecialdiades[i].Competencia + " - ";
                    }
                }

            }
            return Retorno;

        }
        public RespostaSolicitacaoImpressaoPlanejadoViewModel RetornaRelatorioPlanejado(SolicitacaoImpressaoRelPlanejadoViewModel parmRelatorio)
        {

            RespostaSolicitacaoImpressaoPlanejadoViewModel retorno = new RespostaSolicitacaoImpressaoPlanejadoViewModel();

            var UsuarioEspecialidade = this.FiltraUsuariosEspecialdiades(parmRelatorio);
            var listaConsultoresId = UsuarioEspecialidade.Select(x => x.Id).Distinct().ToList();
            var Consultores = TransformaListaEmString(listaConsultoresId);
            var ListaCentrodeCusto = TransformaListaEmString(parmRelatorio.ListaCentroCustos);
            var listabanco = this._relatorioRepository.RetornaRelatorioPlanejado(parmRelatorio.DataInicio, parmRelatorio.DataFinal, Consultores, ListaCentrodeCusto);

            if (listabanco == null || listabanco.Count() == 0)
            {
                retorno.Erro = "Sua Busca não Retornou Dados ";
            }
            else
            {
                retorno.Dados = this.MapearListaPlanejamentoViewModel(listabanco, parmRelatorio.ValorPeriodo);
            }

            return retorno;
        }
    }
}
