﻿using Schedule.Application.Interfaces.Services;
using Schedule.Domain.Interfaces.Repository.Relatorio;
using Schedule.Domain.Relatorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schedule.Application.Services.Relatorio
{
    public class RelatorioDisponibilidadeService : IRelatorioDisponibilidadeService
    {
        private readonly IRelatorioRepository _relatorioRepository;
     
        public RelatorioDisponibilidadeService(IRelatorioRepository RelatorioRepository)
        {
            _relatorioRepository = RelatorioRepository;
        }

        public List<RelatorioDisponibilidade> RelatorioDisponibilidade(DateTime dataInicio, DateTime dataFinal, string[] idConsultores = null, string[] idEspecialidades = null, List<Guid> idEmpresas = null)
        {
            var listaColaboradores = _relatorioRepository.RelatorioColaboradores(dataInicio, dataFinal, idConsultores, idEspecialidades);

            if(listaColaboradores != null && idEmpresas != null && idEmpresas.Count > 0)
            {
                listaColaboradores = listaColaboradores.Where(x => idEmpresas.Contains(x.EmpresaId)).ToList();
            }
            var IdConsults = listaColaboradores.Select(x => x.Id.ToString()).ToArray();

            var listaAgendaProgramada = _relatorioRepository.RelatorioAgendasColaboradores(dataInicio, dataFinal, IdConsults);

            var listaAgendaVazia = AgendaVazia(dataInicio, dataFinal);

            return RelatorioDisponibilidade(dataInicio,dataFinal,idConsultores,listaColaboradores,listaAgendaProgramada,listaAgendaVazia);
        }


        public List<RelatorioDisponibilidade> RelatorioDisponibilidade(
            DateTime dataInicio,
            DateTime dataFinal, 
            string[] idConsultores, 
            List<RelatorioDisponibilidade> listaColaboradores,
            List<AgendaProgramada> listaAgendaProgramada,
            List<AgendaProgramada> listaAgendaVazia)
        {

            //preenche os dias que estão ocupados
            foreach (var colaboradores in listaColaboradores)
            {
                var agendaColab = listaAgendaProgramada.Where(x => x.Id == colaboradores.Id).ToList();

                colaboradores.Agenda = agendaColab;
                //preenche os dias que estão vagos

                //filtrar os dias disponiveis para o colaborador

                var agendaDiasLivres = new List<AgendaProgramada>();
                var detalheDataInicio = colaboradores.DataInicioContrato.Split("/");
                var detalheDataFim = String.IsNullOrEmpty(colaboradores.DataTerminoContrato) ? null : colaboradores.DataTerminoContrato.Split("/");
                var dataInicioContrato = detalheDataInicio[1] + "/" + detalheDataInicio[0] + "/" + detalheDataInicio[2];
                var dataFimcontrato = String.IsNullOrEmpty(colaboradores.DataTerminoContrato) ? null : detalheDataFim[1] + "/" + detalheDataFim[0] + "/" + detalheDataFim[2];

                var diasdisponiveis = String.IsNullOrEmpty(colaboradores.DataTerminoContrato) ? listaAgendaVazia.Where(x => x.Dia >= Convert.ToDateTime(dataInicioContrato)) : listaAgendaVazia.Where(x => x.Dia >= Convert.ToDateTime(dataInicioContrato)).Where(x => x.Dia <= Convert.ToDateTime(dataFimcontrato));



                foreach (var diaVazio in diasdisponiveis)
                {
                    if (agendaColab.Where(x => x.Dia == diaVazio.Dia && x.Periodo == diaVazio.Periodo).FirstOrDefault() == null)
                    {
                        agendaDiasLivres.Add(diaVazio);
                    }
                }

                colaboradores.Agenda.AddRange(agendaDiasLivres);
                colaboradores.Agenda = colaboradores.Agenda.OrderBy(x => x.Dia).ThenBy(x => x.Periodo).ToList();
                colaboradores.HorasDisponiveis = agendaDiasLivres.Count * 4;
                colaboradores.HorasNaoDisponiveis = agendaColab.Count * 4;
            }

            
            return listaColaboradores;

        }

        private List<AgendaProgramada> AgendaVazia(DateTime dataInicio, DateTime dataFinal)
        {
            var listaDiasUteis = ConsultaDiasUteis(dataInicio, dataFinal);

            var agenda = new  List<AgendaProgramada>();

            foreach (var dia in listaDiasUteis)
            {
                agenda.Add(new AgendaProgramada {  
                    Periodo = 1, 
                    Dia = dia, 
                    DescricaoAtividade = "Livre", 
                    StatusAgenda = StatusAgenda.Livre} );
                agenda.Add(new AgendaProgramada
                {
                    Periodo = 2,
                    Dia = dia,
                    DescricaoAtividade = "Livre",
                    StatusAgenda = StatusAgenda.Livre
                });
            }
            return agenda;
        }

        public List<DiasPeriodo> ConsultarDiasPeriodo(DateTime dataInicio, DateTime dataFinal)
        {
            var listaDias = new List<DiasPeriodo>();
            var listaFeriados = _relatorioRepository.ConsultarFeriados(dataInicio, dataFinal).Select(x => x.DataFeriado.Date).ToList();

            for (var dt = dataInicio; dt <= dataFinal; dt = dt.AddDays(1))
            {
                if (listaFeriados.Contains(dt))
                {
                    listaDias.Add(new DiasPeriodo { Dia = dt, StatusDia = StatusDia.Feriado });
                }else
                if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                {
                    listaDias.Add(new DiasPeriodo { Dia = dt, StatusDia = StatusDia.FimDeSemana });
                }else {
                    listaDias.Add(new DiasPeriodo { Dia = dt, StatusDia = StatusDia.DiaUtil});
                }
            }

            return listaDias;
        }

        public List<DateTime> ConsultaDiasUteis(DateTime dataInicio, DateTime dataFinal)
        {
            var listaFeriados = _relatorioRepository.ConsultarFeriados(dataInicio, dataFinal).Select(x => x.DataFeriado).Select( d=> d.Date).ToList();
            var listaDiasUteis = new List<DateTime>();

            for (var dt = dataInicio; dt <= dataFinal; dt = dt.AddDays(1))
            {   
                if(dt.DayOfWeek != DayOfWeek.Saturday && dt.DayOfWeek != DayOfWeek.Sunday)
                {
                    if (!listaFeriados.Contains(dt))
                    {
                        listaDiasUteis.Add(dt);
                    }
                }                
            }

            return listaDiasUteis;
        }

        public List<RelatorioDisponibilidade> RelatorioDisponibilidade(DateTime dataInicio, DateTime dataFinal, string nome, List<RelatorioDisponibilidade> listaColaboradores, List<AgendaProgramada> listaAgendaProgramada, List<AgendaProgramada> listaAgendaVazia)
        {
            throw new NotImplementedException();
        }

    
    }
}
