﻿using AutoMapper;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository;
using Schedule.Util.Validacao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schedule.Application.Services
{
    public class CentroCustoService : ICentroCustoService
    {
        private readonly ICentroDeCustoRepository _repositorio;
        private readonly IClienteRepository _clienteRepositorio;
        private readonly IUsuarioRepository _usuarioRepositorio;
        private readonly IEmpresaRepository _empresaRepositorio;
        private readonly ITipoCentroCustoRepository _tipoCentroCustoRepository;
        private readonly IMapper _mapper;
        public CentroCustoService(
            ICentroDeCustoRepository repositorio, IClienteRepository clienteRepositorio,
            IUsuarioRepository usuarioRepositorio, IEmpresaRepository emmpresaRepository, ITipoCentroCustoRepository tipoCentroCustoRepository, IMapper mapper)
        {
            this._repositorio = repositorio;
            this._clienteRepositorio = clienteRepositorio;
            this._usuarioRepositorio = usuarioRepositorio;
            this._empresaRepositorio = emmpresaRepository;
            this._tipoCentroCustoRepository = tipoCentroCustoRepository;
            _mapper = mapper;

        }

        public void Adicionar(CentroCustoViewModel entidade)
        {

            if (!Validacao.VerificaData(entidade.DataInicioContrato) || !Validacao.VerificaData(entidade.DataFinalContrato))
            {
                throw new DomainException("Verifique os campos data inicio de contrato/Data término Contrato");
            }
            if (Convert.ToDateTime(entidade.DataInicioContrato) > Convert.ToDateTime(entidade.DataFinalContrato))
            {
                throw new DomainException("O campo inicio de contrato deve ser menor que o campo data término Contrato");
            }
            Validacao.ValidaLimitePermetidoData(Convert.ToDateTime(entidade.DataInicioContrato));
            Validacao.ValidaLimitePermetidoData(Convert.ToDateTime(entidade.DataFinalContrato));


            if (_repositorio.Existe(x => x.Nome.ToLower() == entidade.Nome.ToLower()))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Nome"));
            }
            
            var CentroDeCusto = _mapper.Map<CentroCusto>(entidade);
            CentroDeCusto.Ativo = true;
            _repositorio.Adicionar(CentroDeCusto);
        }
        public void Alterar(CentroCustoViewModel entidade)
        {
            if (!Validacao.VerificaData(entidade.DataInicioContrato) || !Validacao.VerificaData(entidade.DataFinalContrato))
            {
                throw new DomainException("Verifique os campos data inicio de contrato/data término Contrato");
            }
            if (Convert.ToDateTime(entidade.DataInicioContrato) > Convert.ToDateTime(entidade.DataFinalContrato))
            {
                throw new DomainException("O campo data inicio de contrato deve ser menor que o campo data término contrato");
            }
            Validacao.ValidaLimitePermetidoData(Convert.ToDateTime(entidade.DataInicioContrato));
            Validacao.ValidaLimitePermetidoData(Convert.ToDateTime(entidade.DataFinalContrato));


            if (_repositorio.Existe(x => x.Nome.ToLower() == entidade.Nome.ToLower() && x.Id != entidade.id))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Nome"));
            }
            
            var CentroDeCusto = _repositorio.ObterPorId((Guid)entidade.id);
            CentroDeCusto.EmpresaId = entidade.EmpresaId;
            CentroDeCusto.ClienteId = entidade.ClienteId;
            CentroDeCusto.ResponsavelId = entidade.ResponsavelId;
            CentroDeCusto.TipoCentroCustoId = entidade.TipoCentroCustoId;
            CentroDeCusto.Taxa = entidade.Taxa;
            CentroDeCusto.Nome = entidade.Nome;
            CentroDeCusto.DataInicioContrato = Convert.ToDateTime(entidade.DataInicioContrato);
            CentroDeCusto.DataFinalContrato = Convert.ToDateTime(entidade.DataFinalContrato);
            CentroDeCusto.Usuario = entidade.Usuario;
            CentroDeCusto.Ativo = entidade.Ativo;


            _repositorio.Editar(CentroDeCusto);
        }
        public void Excluir(Guid id)
        {
            var CentroDeCusto = _repositorio.ObterPorId(id);
            _repositorio.Remover(CentroDeCusto);
        }
        public List<CentroCustoViewModel> Listar()
        {
            var lista = _repositorio.Listar().ToList();
            var resultado = _mapper.Map<List<CentroCustoViewModel>>(lista);

            if (resultado != null && resultado.Count() > 0)
            {
                var listaClientes = _clienteRepositorio.Listar().ToList();
                var listaUsuarios = _usuarioRepositorio.Listar().ToList();
                var listaEmpresas = _empresaRepositorio.Listar().ToList();
                var listaTiposCentroCusto = _tipoCentroCustoRepository.Listar().ToList();
                foreach (var item in resultado)
                {
                    item.ClienteNome = this.RetornaNomeCliente(listaClientes, item.ClienteId);
                    item.ResponsavelNome = this.RetornaNomeResponsavel(listaUsuarios, item.ResponsavelId);
                    item.EmpresaNome = this.RetornaNomeEmpresa(listaEmpresas, item.EmpresaId);
                    item.TipoCentroCustoNome = this.RetornaNomeTipoCentroCusto(listaTiposCentroCusto, item.TipoCentroCustoId);
                    item.DataInicioContrato = Convert.ToDateTime(item.DataInicioContrato).ToString("dd/MM/yyyy");
                    if (!string.IsNullOrEmpty(item.DataFinalContrato) && Validacao.VerificaData(item.DataFinalContrato))
                    {
                        item.DataFinalContrato = Convert.ToDateTime(item.DataFinalContrato).ToString("dd/MM/yyyy");
                    }

                }
            }
            return resultado;
        }
        private string RetornaNomeResponsavel(List<Usuario> lista, Guid UsuarioId)
        {
            string retorno = "";
            if (lista != null && lista.Count() > 0)
            {
                var entidade = lista.Where(c => c.Id == UsuarioId).FirstOrDefault();
                if (entidade != null)
                {
                    retorno = entidade.Nome;
                }
            }
            return retorno;

        }
        private string RetornaNomeCliente(List<Cliente> lista, Guid ClienteId)
        {
            string retorno = "";
            if (lista != null && lista.Count() > 0)
            {
                var entidade = lista.Where(c => c.Id == ClienteId).FirstOrDefault();
                if (entidade != null)
                {
                    retorno = entidade.Nome;
                }
            }
            return retorno;

        }
        private string RetornaNomeEmpresa(List<Empresa> lista, Guid EmpresaId)
        {
            string retorno = "";
            if (lista != null && lista.Count() > 0)
            {
                var entidade = lista.Where(c => c.Id == EmpresaId).FirstOrDefault();
                if (entidade != null)
                {
                    retorno = entidade.Nome;
                }
            }
            return retorno;

        }
        private string RetornaNomeTipoCentroCusto(List<TipoCentroCusto> lista, Guid TipoCentroCustoId)
        {
            string retorno = "";
            if (lista != null && lista.Count() > 0)
            {
                var entidade = lista.Where(c => c.Id == TipoCentroCustoId).FirstOrDefault();
                if (entidade != null)
                {
                    retorno = entidade.Codigo + "/" + entidade.Nome;
                }
            }
            return retorno;

        }
        public void Desativar(Guid Id, string usuario)
        {
            _repositorio.Desativar(Id, usuario);
        }
        public CentroCustoViewModel RetornaPorId(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            var resultado = _mapper.Map<CentroCustoViewModel>(entidade);
            return resultado;
        }

        public bool VerificaSeRegistroExisteNaBase(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            return entidade != null;

        }
    }
}
