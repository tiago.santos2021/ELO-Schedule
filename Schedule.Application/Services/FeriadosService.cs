﻿using AutoMapper;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.ViewModel;
using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schedule.Application.Services
{
    public class FeriadosService : IFeriadosService
    {

        private readonly IFeriadosRepository _repositorio;
        private readonly IMapper _mapper;
        public FeriadosService(IFeriadosRepository repositorio, IMapper mapper)
        {
            this._repositorio = repositorio;
            this._mapper = mapper;

        }

        public void Adicionar(FeriadosViewModel entidade)
        {
            var feriado = _mapper.Map<Feriado>(entidade);
            
            _repositorio.Adicionar(feriado);
        }

        public void Alterar(FeriadosViewModel entidade)
        {
            var retornado = _repositorio.ObterPorId((Guid)entidade.id);

            retornado.DataFeriado = entidade.DataFeriado;
            retornado.Descricao = entidade.Descricao;

            _repositorio.Editar(retornado);

        }

        public void Excluir(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            _repositorio.Remover(entidade);
        }

        public List<FeriadosViewModel> Listar()
        {
            var lista = _repositorio.Listar().ToList();
            var resultado = _mapper.Map<List<FeriadosViewModel>>(lista);
            return resultado;
        }

        public FeriadosViewModel RetornaPorId(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            var resultado = _mapper.Map<FeriadosViewModel>(entidade);
            return resultado;
        }

        public bool VerificaSeRegistroExisteNaBase(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            return entidade != null;
        }
    }
}
