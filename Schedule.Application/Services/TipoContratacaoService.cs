﻿using AutoMapper;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schedule.Application.Services
{
    public class TipoContratacaoService : ITipoContratacaoService
    {
        private readonly ITipoContratacaoRepository _repositorio;
        private readonly IMapper mapper;
        public TipoContratacaoService(ITipoContratacaoRepository repositorio, IMapper _mapper)
        {
            _repositorio = repositorio;
            mapper = _mapper;

        }
        public void Adicionar(TipoContratacaoViewModel entidade)
        {
            if (_repositorio.Existe(x => x.Codigo.ToLower() == entidade.Codigo.ToLower()))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Código"));
            }
            if (_repositorio.Existe(x => x.Nome.ToLower() == entidade.Nome.ToLower()))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Nome"));
            }


            var tipocontratacao = mapper.Map<TipoContratacao>(entidade);
            _repositorio.Adicionar(tipocontratacao);
        }
        public void Alterar(TipoContratacaoViewModel entidade)
        {
            if (_repositorio.Existe(x => x.Codigo.ToLower() == entidade.Codigo.ToLower() && x.Id != entidade.id))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Código"));
            }
            if (_repositorio.Existe(x => x.Nome.ToLower() == entidade.Nome.ToLower() && x.Id != entidade.id))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Descrição"));
            }

            var tipocontratacao = _repositorio.ObterPorId((Guid)entidade.id);
            tipocontratacao.Nome = entidade.Nome;
            tipocontratacao.Codigo = entidade.Codigo;
            tipocontratacao.Usuario = entidade.Usuario;
            _repositorio.Editar(tipocontratacao);
        }
        public void Excluir(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            _repositorio.Remover(entidade);
        }
        public List<TipoContratacaoViewModel> Listar()
        {
            var lista = _repositorio.Listar().ToList();
            var resultado = mapper.Map<List<TipoContratacaoViewModel>>(lista);
            return resultado;
        }
        public TipoContratacaoViewModel RetornaPorId(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            var resultado = mapper.Map<TipoContratacaoViewModel>(entidade);
            return resultado;
        }
        public bool VerificaSeRegistroExisteNaBase(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            return entidade != null;

        }
    }
}
