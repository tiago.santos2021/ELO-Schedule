﻿using AutoMapper;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Schedule.Application.Services
{
    public class EspecialidadeService : IEspecialidadeService
    {
        private readonly IEspecialidadeRepository _repositorio;
        private readonly IMapper _mapper;
        public EspecialidadeService(IEspecialidadeRepository repositorio, IMapper mapper)
        {
            this._repositorio = repositorio;
            this._mapper = mapper;

        }
        public void Adicionar(EspecialidadeViewModel entidade)
        {
            if (_repositorio.Existe(x => x.UnidadeServico.ToLower() == entidade.UnidadeServico.ToLower() && x.Competencia.ToLower() == entidade.Competencia.ToLower()))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Especialidade"));
            }
            

         
            var especialidade = _mapper.Map<Especialidade>(entidade);
            _repositorio.Adicionar(especialidade);
        }
        public void Alterar(EspecialidadeViewModel entidade)
        {
            if (_repositorio.Existe(x => x.UnidadeServico.ToLower() == entidade.UnidadeServico.ToLower() && x.Competencia.ToLower() == entidade.Competencia.ToLower() && x.Id != entidade.id))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Especialidade"));
            }
            //if (repositorio.Existe(x => x.Competencia.ToLower() == entidade.Competencia.ToLower() && x.Id != entidade.id))
            //{
            //    throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Competência"));
            //}

            var empresa = _repositorio.ObterPorId((Guid)entidade.id);
            empresa.UnidadeServico = entidade.UnidadeServico;
            empresa.Competencia = entidade.Competencia;
            empresa.Usuario = entidade.Usuario;
            _repositorio.Editar(empresa);
        }
        public void Excluir(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            _repositorio.Remover(entidade);
        }
        public List<EspecialidadeViewModel> Listar()
        {
            var lista = _repositorio.Listar().ToList();
            var resultado = _mapper.Map<List<EspecialidadeViewModel>>(lista);
            return resultado;
        }
        public EspecialidadeViewModel RetornaPorId(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            var resultado = _mapper.Map<EspecialidadeViewModel>(entidade);
            return resultado;

        }
        public bool VerificaSeRegistroExisteNaBase(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            return entidade != null;

        }
    }
}