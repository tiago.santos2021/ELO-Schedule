﻿using AutoMapper;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schedule.Application.Services
{
    public class TipoCentroCustoService : ITipoCentroCustoService
    {
        private readonly ITipoCentroCustoRepository _repositorio;
        private readonly IMapper _mapper;
        public TipoCentroCustoService(ITipoCentroCustoRepository repositorio, IMapper mapper)
        {
            this._repositorio = repositorio;
            this._mapper = mapper;

        }
        public void Adicionar(TipoCentroCustoViewModel entidade)
        {
            if (_repositorio.Existe(x => x.Codigo.ToLower() == entidade.Codigo.ToLower()))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Código"));
            }
            if (_repositorio.Existe(x => x.Nome.ToLower() == entidade.Nome.ToLower()))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Nome"));
            }


            var empresa = _mapper.Map<TipoCentroCusto>(entidade);
            _repositorio.Adicionar(empresa);
        }
        public void Alterar(TipoCentroCustoViewModel entidade)
        {
            if (_repositorio.Existe(x => x.Codigo.ToLower() == entidade.Codigo.ToLower() && x.Id != entidade.id))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Código"));
            }
            if (_repositorio.Existe(x => x.Nome.ToLower() == entidade.Nome.ToLower() && x.Id != entidade.id))
            {
                throw new DomainException(string.Format(ScheduleResorce.RetornaValor("ValorDupliado"), "Descrição"));
            }

            var empresa = _repositorio.ObterPorId((Guid)entidade.id);
            empresa.Nome = entidade.Nome;
            empresa.Codigo = entidade.Codigo;
            empresa.Usuario = entidade.Usuario;
            _repositorio.Editar(empresa);
        }
        public void Excluir(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            _repositorio.Remover(entidade);
        }
        public List<TipoCentroCustoViewModel> Listar()
        {
            var lista = _repositorio.Listar().ToList();
            var resultado = _mapper.Map<List<TipoCentroCustoViewModel>>(lista);
            return resultado;
        }
        public TipoCentroCustoViewModel RetornaPorId(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            var resultado = _mapper.Map<TipoCentroCustoViewModel>(entidade);
            return resultado;

        }
        public bool VerificaSeRegistroExisteNaBase(Guid id)
        {
            var entidade = _repositorio.ObterPorId(id);
            return entidade != null;

        }
    }

}

