﻿using Schedule.Application.Interfaces.Services.Resorces;
using Schedule.Application.Resorces.UF;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Application.Services.Resorces
{
    public class BaseUFService : IBaseUFService
    {
        public List<EstadoViewModel> Listar()
        {
            List<EstadoViewModel>  Lista = new List<EstadoViewModel>();
            Lista.Add(new EstadoViewModel() { Sigla = "AC", Descricao = "Acre" });
            Lista.Add(new EstadoViewModel() { Sigla = "AL", Descricao = "Alagoas" });
            Lista.Add(new EstadoViewModel() { Sigla = "AP", Descricao = "Amapá" });
            Lista.Add(new EstadoViewModel() { Sigla = "AM", Descricao = "Amazonas" });
            Lista.Add(new EstadoViewModel() { Sigla = "BA", Descricao = "Bahia" });
            Lista.Add(new EstadoViewModel() { Sigla = "CE", Descricao = "Ceará" });
            Lista.Add(new EstadoViewModel() { Sigla = "DF", Descricao = "Distrito Federal" });
            Lista.Add(new EstadoViewModel() { Sigla = "ES", Descricao = "Espírito Santo" });
            Lista.Add(new EstadoViewModel() { Sigla = "GO", Descricao = "Goiás" });
            Lista.Add(new EstadoViewModel() { Sigla = "MA", Descricao = "Maranhão" });
            Lista.Add(new EstadoViewModel() { Sigla = "MT", Descricao = "Mato Grosso" });
            Lista.Add(new EstadoViewModel() { Sigla = "MS", Descricao = "Mato Grosso do Sul" });
            Lista.Add(new EstadoViewModel() { Sigla = "MG", Descricao = "Minas Gerais" });
            Lista.Add(new EstadoViewModel() { Sigla = "PA", Descricao = "Pará" });
            Lista.Add(new EstadoViewModel() { Sigla = "PB", Descricao = "Paraíba" });
            Lista.Add(new EstadoViewModel() { Sigla = "PR", Descricao = "Paraná" });
            Lista.Add(new EstadoViewModel() { Sigla = "PE", Descricao = "Pernambuco" });
            Lista.Add(new EstadoViewModel() { Sigla = "PI", Descricao = "Piauí" });
            Lista.Add(new EstadoViewModel() { Sigla = "RJ", Descricao = "Rio de Janeiro" });
            Lista.Add(new EstadoViewModel() { Sigla = "RN", Descricao = "Rio Grande do Norte" });
            Lista.Add(new EstadoViewModel() { Sigla = "RS", Descricao = "Rio Grande do Sul" });
            Lista.Add(new EstadoViewModel() { Sigla = "RO", Descricao = "Rondônia" });
            Lista.Add(new EstadoViewModel() { Sigla = "RR", Descricao = "Roraima" });
            Lista.Add(new EstadoViewModel() { Sigla = "SC", Descricao = "Santa Catarina" });
            Lista.Add(new EstadoViewModel() { Sigla = "SP", Descricao = "São Paulo" });
            Lista.Add(new EstadoViewModel() { Sigla = "SE", Descricao = "Sergipe" });
            Lista.Add(new EstadoViewModel() { Sigla = "TO", Descricao = "Tocantins" });
            return Lista;
        }
    }
}
