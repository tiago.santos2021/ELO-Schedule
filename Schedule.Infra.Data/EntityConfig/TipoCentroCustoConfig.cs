﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Schedule.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Infra.Data.EntityConfig
{
 

    public class TipoCentroCustoConfig : IEntityTypeConfiguration<TipoCentroCusto>
    {
        public void Configure(EntityTypeBuilder<TipoCentroCusto> builder)
        {
            builder.HasKey(x => x.Id);
        }

    }
}
