﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Schedule.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Infra.Data.EntityConfig
{
   public class SenioridadeConfig : IEntityTypeConfiguration<Senioridade>
    {
      
        public void Configure(EntityTypeBuilder<Senioridade> builder)
        {
            builder.HasKey(x => x.Id);
        }
    }
}