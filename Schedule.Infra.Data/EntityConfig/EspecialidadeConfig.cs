﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Schedule.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Infra.Data.EntityConfig
{


    public class EspecialidadeConfig : IEntityTypeConfiguration<Especialidade>
    {
        public void Configure(EntityTypeBuilder<Especialidade> builder)
        {
            builder.HasKey(x => x.Id);
        }

    }
}
