﻿using Microsoft.EntityFrameworkCore;
using Schedule.Domain.Entities;
using Schedule.Domain.Entities.Estrutura;
using Schedule.Domain.Interfaces.Repository;
using Schedule.Infra.Data.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Schedule.Infra.Data.Repository
{
    public class UsuarioRepository : RepositoryBase<Usuario, Guid>, IUsuarioRepository
    {
        public UsuarioRepository(ScheduleContext context) : base(context)
        {
        }
        public void Adicionar(Usuario entidade, List<Guid> ListaEspecialidades)
        {
            entidade.DataAlteracao = DateTime.Now;

            _context.Usuario.Add(entidade);
            foreach (var item in ListaEspecialidades)
            {
                var usuarioEspecialdiade = new UsuarioEspecialidade() { EspecialidadeId = item, UsuarioId = entidade.Id, DataAlteracao = DateTime.Now, Usuario = entidade.Usuario };
                _context.UsuarioEspecialidade.Add(usuarioEspecialdiade);
            }
            _context.SaveChanges();

        }
        public void DesativarUsuario(Guid Id, string usuarioAlteracao)
        {
            var usuario = _context.Usuario.Where(u => u.Id == Id).FirstOrDefault();
            usuario.DataAlteracao = DateTime.Now;
            usuario.Usuario = usuarioAlteracao;
            usuario.Ativo = false;
            _context.SaveChanges();

        }
        public void Editar(Usuario entidade, List<Guid> ListaEspecialidades)
        {

            var usuario = _context.Usuario.Where(u => u.Id == entidade.Id).FirstOrDefault();
            usuario.DataAlteracao = DateTime.Now;
            usuario.Nome = entidade.Nome;
            usuario.Login = entidade.Login;
            usuario.UF = entidade.UF;
            usuario.Telefone1 = entidade.Telefone1;
            usuario.DDD1 = entidade.DDD1;
            usuario.Telefone2 = entidade.Telefone2;
            usuario.DDD2 = entidade.DDD2;
            usuario.PerfilId = entidade.PerfilId;
            usuario.DataInicioContrato = entidade.DataInicioContrato;
            usuario.DataTerminoContrato = entidade.DataTerminoContrato;
            usuario.EmpresaId = entidade.EmpresaId;
            usuario.SenioridadeId = entidade.SenioridadeId;
            usuario.GestorImediatoId = entidade.GestorImediatoId;
            usuario.TipoContratacaoId = entidade.TipoContratacaoId;
            usuario.ValorHora = entidade.ValorHora;
            usuario.ValorMensal = entidade.ValorMensal;
            usuario.Usuario = entidade.Usuario;
            usuario.Ativo = entidade.Ativo;
            usuario.Email = entidade.Email;
            usuario.Observacao = entidade.Observacao;
            var listaEspecialdiadesBanco = _context.UsuarioEspecialidade.Where(u => u.UsuarioId == usuario.Id).ToList();
            if (listaEspecialdiadesBanco == null || listaEspecialdiadesBanco.Count() == 0)
            {
                foreach (var esp in ListaEspecialidades)
                {
                    var usuarioEspecialdiade = new UsuarioEspecialidade() { EspecialidadeId = esp, UsuarioId = entidade.Id, DataAlteracao = DateTime.Now, Usuario = entidade.Usuario };
                    _context.UsuarioEspecialidade.Add(usuarioEspecialdiade);
                }
            }
            else
            {
                foreach (var item in ListaEspecialidades)
                {
                    var especialdiadeCadastrada = listaEspecialdiadesBanco.Where(e => e.EspecialidadeId == item).FirstOrDefault();
                    if (especialdiadeCadastrada == null)
                    {
                        var usuarioEspecialdiade = new UsuarioEspecialidade() { EspecialidadeId = item, UsuarioId = entidade.Id, DataAlteracao = DateTime.Now, Usuario = entidade.Usuario };
                        _context.UsuarioEspecialidade.Add(usuarioEspecialdiade);
                    }
                }
                var listaEspecialdiadesBancoExclusao = listaEspecialdiadesBanco.Where(e => !ListaEspecialidades.Contains(e.EspecialidadeId)).ToList();

                if (listaEspecialdiadesBancoExclusao != null && listaEspecialdiadesBancoExclusao.Count() > 0)
                {
                    foreach (var espeExclusao in listaEspecialdiadesBancoExclusao)
                    {

                        _context.UsuarioEspecialidade.Remove(espeExclusao);
                    }
                }


            }
            _context.SaveChanges();
        }

        public List<object> ListaUsuarioDetalhes()
        {

            var resultado = new List<dynamic>();

            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = @"select 
                                      
                                        US.LOGIN,
                                        US.NOME, 
                                        PE.NOME AS PERFILDESCRICAO, 
                                        US.ATIVO AS ATIVO, 
                                        SEN.NOME AS SENIORIDADE , 
                                        US.DATAINICIOCONTRATO, 
                                        US.DATATERMINOCONTRATO,
                                        (
                                        SUBSTRING(
	                                        (
		                                        SELECT ', '+ESP.Competencia  AS [text()]
		                                        FROM Especialidade ESP
		                                        left join UsuarioEspecialidade ue
		                                        on ESP.id = ue.EspecialidadeId
		                                        WHERE ue.UsuarioId = us.id
		                                        ORDER BY ESP.id
		                                        FOR XML PATH (''), TYPE
	                                        ).value('text()[1]','nvarchar(max)'), 2, 1000) 
                                        ) as 'ESPECIALIDADE', 
                                        TP.CODIGO AS TIPOCONTRATACAO,
                                        PE.GRUPO AS PERFILGRUPO,
                                        US.ID ,
                                        US.OBSERVACAO,
                                        E.NOME AS EMPRESA
                                        from Usuario as us
                                        left join Perfil as pe on pe.id = us.PerfilId
                                        left join UsuarioEspecialidade as ue on ue.UsuarioId = us.id 
                                        left join Senioridade as sen on sen.id =us.SenioridadeId 
                                        left join TipoContratacao as tp on us.TipoContratacaoId = tp.id
                                        LEFT JOIN EMPRESA AS E ON  E.ID = us.EMPRESAID
                                        group by 
	                                        Login,
	                                        us.Nome,
	                                        pe.Nome,
	                                        us.Ativo,
	                                        sen.Nome,
	                                        us.DataInicioContrato,
	                                        us.DataTerminoContrato,
	                                        tp.Codigo,
	                                        pe.grupo,
	                                        us.Id,
	                                        us.Observacao,
                                            E.NOME
	                                    order by Nome";
                _context.Database.OpenConnection();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        string Empresa = reader["EMPRESA"] == DBNull.Value ? "" : reader["EMPRESA"].ToString();
                        string Login = reader["LOGIN"] == DBNull.Value ? "" : reader["LOGIN"].ToString();
                        string Nome = reader["NOME"] == DBNull.Value ? "" : reader["NOME"].ToString();
                        string PerfilDescricao = reader["PERFILDESCRICAO"] == DBNull.Value ? "" : reader["PERFILDESCRICAO"].ToString();
                        bool? Ativo = reader["ATIVO"] == DBNull.Value ? false : Convert.ToBoolean(reader["ATIVO"]);
                        string Senioridade = reader["SENIORIDADE"] == DBNull.Value ? "" : reader["SENIORIDADE"].ToString();
                        string DataInicioContrato = reader["DATAINICIOCONTRATO"] == DBNull.Value ? "" : Convert.ToDateTime( reader["DATAINICIOCONTRATO"]).ToString("dd/MM/yyyy");
                        string DataTerminoContrato = reader["DATATERMINOCONTRATO"] == DBNull.Value ? "" : Convert.ToDateTime(reader["DATATERMINOCONTRATO"]).ToString("dd/MM/yyyy");
                        string Especialidade = reader["ESPECIALIDADE"] == DBNull.Value ? "" : reader["ESPECIALIDADE"].ToString();
                        string TipoContratacao = reader["TIPOCONTRATACAO"] == DBNull.Value ? "" : reader["TIPOCONTRATACAO"].ToString();
                        string PerfilId = reader["PERFILGRUPO"] == DBNull.Value ? "" : reader["PERFILGRUPO"].ToString();
                        Guid? Id = Guid.Parse(reader["ID"].ToString());
                        string Observacao = reader["OBSERVACAO"] == DBNull.Value ? "" : reader["OBSERVACAO"].ToString();

                        var objetoRetorno = new
                        {
                            Login,
                            Nome,
                            PerfilDescricao,
                            Ativo,
                            Senioridade,
                            DataInicioContrato,
                            DataTerminoContrato,
                            Especialidade,
                            TipoContratacao,
                            Id,
                            Observacao,
                            Empresa
                        };

                        resultado.Add(objetoRetorno);
                    }
                }
                _context.Database.CloseConnection();

            }



            return resultado;
        }
    }
}
