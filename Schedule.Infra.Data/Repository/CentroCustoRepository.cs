﻿using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository;
using Schedule.Infra.Data.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schedule.Infra.Data.Repository
{
   public class CentroCustoRepository : RepositoryBase<CentroCusto, Guid>, ICentroDeCustoRepository
    {

        private readonly ScheduleContext _context;
        public CentroCustoRepository(ScheduleContext context) : base(context)
        {
            _context = context;

        }
        public void Desativar(Guid Id, string usuarioAlteracao)
        {

            var entidade = _context.CentroCusto.Where(x => x.Id == Id).FirstOrDefault();
            entidade.DataAlteracao = DateTime.Now;
            entidade.Usuario = usuarioAlteracao;
            entidade.Ativo = false;
            _context.SaveChanges();
        }
    }
}