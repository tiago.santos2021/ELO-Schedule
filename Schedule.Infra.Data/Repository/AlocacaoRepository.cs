﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Schedule.Domain.Entities;
using Schedule.Domain.Enum;
using Schedule.Domain.Interfaces;
using Schedule.Infra.Data.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Schedule.Infra.Data.Repository
{
    public class AlocacaoRepository : RepositoryBase<Agenda, Guid>, IAlocacaoRepository
    {

        public AlocacaoRepository(ScheduleContext context) : base(context)
        {
        }
        public void Incluir(Agenda entidade)
        {
            entidade.DataAlteracao = DateTime.Now;

            _context.Agenda.Add(entidade);

            _context.SaveChanges();
        }
        public Tuple<string, string> ValidarConflitos(Guid usuarioId, DateTime dataAgenda, int periodo, string usuarioLogado)
        {
            var agenda = _context.Agenda.Where(x => x.UsuarioId == usuarioId && x.DataAgenda == dataAgenda && x.Periodo == periodo).FirstOrDefault();

            if (agenda == null)
                return new Tuple<string, string>("", "");

            //var alocacaoAtual = _context.Usuario.Where(x => x.Login == usuarioLogado).Select(x => new { x.Nome, x.Email }).FirstOrDefault();
            //var alocacaoAnterior = _context.Usuario.Where(x => x.Login == agenda.Usuario).Select(x => new { x.Nome, x.Email }).FirstOrDefault();

            var alocacaoAtual = _context.Usuario.Where(x => x.Nome == usuarioLogado).Select(x => new { x.Nome, x.Email }).FirstOrDefault();
            var alocacaoAnterior = _context.Usuario.Where(x => x.Nome == agenda.Usuario).Select(x => new { x.Nome, x.Email }).FirstOrDefault();

            //return new Tuple<string,string>(alocacaoAtual.Email + ";" + alocacaoAnterior.Email + ";" + "schedule@elo.inf.br", alocacaoAtual.Nome + ";" + alocacaoAnterior.Nome);
            return new Tuple<string, string>(alocacaoAtual.Email + ";" + alocacaoAnterior.Email + ";" + "filipe.feitoza@elo.inf.br", alocacaoAtual.Nome + ";" + alocacaoAnterior.Nome);
        }
        public string BuscarTemplateEmail(TipoEmailEnum tipoEmail)
        {
            var template = _context.TemplateEmail.Where(x => x.Tipo == (int)tipoEmail).Select(x => new { x.Html }).FirstOrDefault();
            return template.Html;
        }
        public List<Agenda> PesquisaAlocacao(DateTime dataInicio, DateTime dataFinal, string consultores, string centroCusto, bool IncluirReservaFeriado)
        {
            var cmd = _context.Database.GetDbConnection().CreateCommand();
            _context.Database.GetDbConnection().Open();
            cmd.CommandText = "sp_PesquisaAlocacao";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@dataInicio", dataInicio));
            cmd.Parameters.Add(new SqlParameter("@dataFinal", dataFinal));
            cmd.Parameters.Add(new SqlParameter("@Consultores", consultores));
           
            using (var rd = cmd.ExecuteReader())
            {
                List<Agenda> Lista = new List<Agenda>();
                foreach (var item in rd)
                {
                    Agenda data = new Agenda();
                    data.TipoAgenda = -1;
                    data.FuncionarioNome = rd["FuncionarioNome"].ToString();
                    data.DataAgenda = Convert.ToDateTime(rd["DataAgenda"]);
                    data.CentroCustoDescricao = rd["CentroCustoDescricao"].ToString();
                    if (rd["CentroCustoId"] != DBNull.Value)
                    {
                        data.CentroCustoId = Guid.Parse(rd["CentroCustoId"].ToString());
                    }
                    if (rd["Feriado"] != DBNull.Value)
                    {
                        data.Feriado = Convert.ToInt32(rd["Feriado"]);
                    }

                    data.Periodo = Convert.ToInt16(rd["Periodo"]);
                    data.UsuarioId = Guid.Parse(rd["UsuarioId"].ToString());
                    data.TipoAgenda = Convert.ToInt16(rd["TipoAgenda"]);
                    data.DataAlteracao = Convert.ToDateTime(rd["DataAlteracao"]);
                    Lista.Add(data);
                }
                _context.Database.GetDbConnection().Close();
                return Lista;
            }


        }
        public void Deletar(Agenda agenda)
        {
            var agendasDeletar = _context.Agenda.Where(x =>
                x.UsuarioId == agenda.UsuarioId &&
                x.EmpresaId == agenda.EmpresaId &&
                x.CentroCustoId == agenda.CentroCustoId &&
                x.DataAgenda == agenda.DataAgenda &&
                //x.TipoAgenda == agenda.TipoAgenda &&
                x.Periodo == agenda.Periodo
            ).ToList();

            foreach (var agendaItem in agendasDeletar)
            {
                _context.Agenda.Remove(agendaItem);
            }

            _context.SaveChanges();
        }
        public IQueryable<Agenda> Listar()
        {
            return _context.Agenda.AsQueryable();
        }
        public List<Usuario> ListaConsultoresComConflitos(DateTime DataInicial, DateTime DataFinal)
        {
            var cmd = _context.Database.GetDbConnection().CreateCommand();
            _context.Database.GetDbConnection().Open();
            cmd.CommandText = "sp_ListaConsultoresComConflitos";
            cmd.Parameters.Add(new SqlParameter("@dataInicio", DataInicial));
            cmd.Parameters.Add(new SqlParameter("@dataFinal", DataFinal));
            cmd.CommandType = System.Data.CommandType.StoredProcedure;


            List<Usuario> Lista = new List<Usuario>();
            using (var rd = cmd.ExecuteReader())
            {

                foreach (var item in rd)
                {
                    Usuario usuario = new Usuario();
                    usuario.Id = Guid.Parse(rd["ID"].ToString());
                    usuario.Nome = Convert.ToString(rd["NOME"].ToString());
                    usuario.GestorImediatoId = Guid.Parse(rd["GESTORIMEDIATOID"].ToString());
                    Lista.Add(usuario);
                }
                _context.Database.GetDbConnection().Close();

            }

            return Lista;
        }
        public List<Agenda> ListaConflitosPorConsultor(Guid ConsultorId, DateTime DataInicial, DateTime DataFinal)
        {
            var cmd = _context.Database.GetDbConnection().CreateCommand();
            _context.Database.GetDbConnection().Open();
            cmd.CommandText = "sp_ListaConflitosPorConsultor";
            cmd.Parameters.Add(new SqlParameter("@ConsultorId", ConsultorId));
            cmd.Parameters.Add(new SqlParameter("@dataInicio", DataInicial));
            cmd.Parameters.Add(new SqlParameter("@dataFinal", DataFinal));
            cmd.CommandType = System.Data.CommandType.StoredProcedure;


            List<Agenda> Lista = new List<Agenda>();
            using (var rd = cmd.ExecuteReader())
            {

                foreach (var item in rd)
                {
                    Agenda agenda = new Agenda();
                    agenda.Id = Guid.Parse(rd["ID"].ToString());
                    agenda.FuncionarioNome = Convert.ToString(rd["FUNCIONARIONOME"].ToString());
                    agenda.UsuarioId = Guid.Parse(rd["USUARIOID"].ToString());
                    agenda.CentroCustoDescricao = Convert.ToString(rd["CENTROCUSTODESCRICAO"].ToString());
                    agenda.DataAgenda = Convert.ToDateTime(rd["DATAAGENDA"].ToString());
                    agenda.TipoAgenda = Convert.ToInt32(rd["TIPOAGENDA"].ToString());
                    agenda.Usuario = Convert.ToString(rd["USUARIO"].ToString());
                    agenda.DataAlteracao = Convert.ToDateTime(rd["DATAALTERACAO"].ToString());
                    agenda.Periodo = Convert.ToInt32(rd["PERIODO"].ToString());
                    if (rd["FERIADO"] != DBNull.Value)
                    {
                        agenda.Feriado = Convert.ToInt32(rd["FERIADO"]);
                    }

                    Lista.Add(agenda);
                }
                _context.Database.GetDbConnection().Close();

            }

            return Lista;
        }    
        public bool ExcluirConflitos(List<Guid> ListaExclusao)
        {
            var ListaAgendas = _context.Agenda.Where(x => ListaExclusao.Contains(x.Id)).ToList();
            _context.Agenda.RemoveRange(ListaAgendas);
            var retorno = _context.SaveChanges();
            return retorno > 0;
        }
    }
}
