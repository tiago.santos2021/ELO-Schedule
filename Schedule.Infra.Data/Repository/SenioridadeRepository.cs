﻿using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository;
using Schedule.Infra.Data.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Infra.Data.Repository
{
  
    public class SenioridadeRepository : RepositoryBase<Senioridade, Guid>, ISenioridadeRepository
    {
        private readonly ScheduleContext _context;
        public SenioridadeRepository(ScheduleContext context) : base(context)
        {
            _context = context;

        }

    }
}


