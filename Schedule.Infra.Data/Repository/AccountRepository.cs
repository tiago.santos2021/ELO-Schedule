﻿using Schedule.Domain.Entities;
using Schedule.Domain.Enum;
using Schedule.Domain.Interfaces.Repository;
using Schedule.Infra.Data.Data;
using System;
using System.Linq;

namespace Schedule.Infra.Data.Repository
{
    public class AccountRepository: RepositoryBase<Usuario, Guid>, IAccountRepository
    {
        public AccountRepository(ScheduleContext context) : base(context)
        {
        }

         
        public UsuarioEmail BuscarEmailUsuario(string login)
        {
            var dadosUsuario = _context.Usuario.Where(x => x.Login == login ).Select(x => new { x.Nome, x.Usuario, x.Email,x.Ativo }).FirstOrDefault();

            UsuarioEmail retorno = new UsuarioEmail();
            if (dadosUsuario != null)
            {
                retorno.Nome = dadosUsuario.Nome;
                retorno.Usuario = dadosUsuario.Usuario;
                retorno.Email = dadosUsuario.Email;
                retorno.Ativo = dadosUsuario.Ativo;
            }
            return retorno;
        }
        public string BuscarTemplateEmail(TipoEmailEnum tipoEmail)
        {
            var template = _context.TemplateEmail.Where(x => x.Tipo == (int)tipoEmail).Select(x => new { x.Html }).FirstOrDefault();
            return template.Html;
        }
        public void AdicionarToken(GerenciamentoToken entidade)
        {
            _context.GerenciamentoToken.Add(entidade);
            _context.SaveChanges();
        }
        public GerenciamentoToken ValidarTokenRedefinicaoSenha(string token)
        {
            return _context.GerenciamentoToken.Where(x => x.Token == token && !x.Utilizado).FirstOrDefault();
        }
        public void AtualizarTokenUsado(string token)
        {
            var tokenUpdate = _context.GerenciamentoToken.Where(x => x.Token == token).FirstOrDefault();
            tokenUpdate.Utilizado = true;
            tokenUpdate.DataAlteracao = DateTime.Now;

            _context.GerenciamentoToken.Update(tokenUpdate);
            _context.SaveChanges();
        }
        public void EditarSenha(string login, string senha)
        {
            var usuarioUpdate = _context.Usuario.Where(u => u.Login == login).FirstOrDefault();
            usuarioUpdate.DataAlteracao = DateTime.Now;
            usuarioUpdate.Senha = senha;

            _context.Usuario.Update(usuarioUpdate);
            _context.SaveChanges();
        }

    }
}
