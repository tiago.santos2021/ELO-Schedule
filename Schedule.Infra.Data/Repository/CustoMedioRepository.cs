﻿using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository;
using Schedule.Infra.Data.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Infra.Data.Repository
{
  public  class CustoMedioRepository : RepositoryBase<CustoMedio, Guid>, ICustoMedioRepository
    {

        private readonly ScheduleContext _context;

        public CustoMedioRepository(ScheduleContext context) : base(context)
        {
            _context = context;

        }
    }
}