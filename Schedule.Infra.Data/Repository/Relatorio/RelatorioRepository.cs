﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Schedule.Domain.Interfaces.Repository.Relatorio;
using Schedule.Domain.Relatorios;
using Schedule.Infra.Data.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Schedule.Domain.Entities;

namespace Schedule.Infra.Data.Repository.Relatorio
{
    public class RelatorioRepository : RepositorioBaseDapper, IRelatorioRepository 
    {
        private readonly ScheduleContext _context;
        
        public RelatorioRepository(ScheduleContext context, IBancoDeDadosRepository bancoDeDados) : base(bancoDeDados)
        {
            _context = context;
        }

        public List<Feriado> ConsultarFeriados(DateTime dataInicio, DateTime dataFinal)
        {
            return Db.Query<Feriado>("select * from Feriado where DataFeriado between @dataInicio and @dataFinal",
                    new { dataInicio, dataFinal }).ToList();
        }
        public List<RelatorioDisponibilidade> RelatorioColaboradores(DateTime dataInicio, DateTime dataFinal, string nomes)
        {
            return Db.Query<RelatorioDisponibilidade>(@"select distinct
                                                        us.id,
                                                        us.Nome, 
                                                        sen.Nome as Senioridade, 
                                                        us.DataInicioContrato, 
                                                        us.DataTerminoContrato,
                                                        (SUBSTRING(
	                                                        (
		                                                        SELECT ', '+ESP.Competencia  AS [text()]
		                                                        FROM Especialidade ESP
		                                                        left join UsuarioEspecialidade ue
		                                                        on ESP.id = ue.EspecialidadeId
		                                                        WHERE ue.UsuarioId = us.id
		                                                        ORDER BY ESP.id
		                                                        FOR XML PATH (''), TYPE
	                                                        ).value('text()[1]','nvarchar(max)'), 2, 1000) 
                                                        ) as 'Especialidade', 
                                                        us.Ativo as Ativo, 
                                                        tp.Nome as TipoContratacao 
                                                        from Usuario as us
                                                        left join Perfil as pe on pe.id = us.PerfilId
                                                        left join UsuarioEspecialidade as ue on ue.UsuarioId = us.id 
                                                        left join Senioridade as sen on sen.id = us.SenioridadeId 
                                                        left join TipoContratacao as tp on us.TipoContratacaoId = tp.id
                                                        where us.Nome like @nome order by us.Nome                                                         
                                                        ", new { @nome = nomes + '%' }).ToList();
        }   
        public List<AgendaProgramada> RelatorioAgendasColaboradores(DateTime dataInicio, DateTime dataFinal, string[] idConsultores)
        {
            return Db.Query<AgendaProgramada>(@"select UsuarioId as Id, DataAgenda as Dia, Periodo, 
                                                CentroCustoDescricao as DescricaoAtividade, StatusAgenda = 2 
                                                from Agenda 
                                                where DataAgenda between  @dataInicio and @dataFinal 
                                                and UsuarioID in @idConsultores",
                                                new {  @idConsultores = idConsultores, @dataInicio= dataInicio, @dataFinal  = dataFinal })
                                                .ToList();
        }
        public List<AgendaProgramada> RelatorioAgendasColaboradores(DateTime dataInicio, DateTime dataFinal, string nome)
        {
            return Db.Query<AgendaProgramada>(@"select UsuarioId as Id, DataAgenda as Dia, Periodo, 
                                                CentroCustoDescricao as DescricaoAtividade, StatusAgenda = 2 
                                                from Agenda 
                                                where DataAgenda between  @dataInicio and @dataFinal
                                                and  FuncionarioNome  like @nome OR @nome IS NULL", 
                                                new { dataInicio, dataFinal, @nome = nome + '%' })
                                                .ToList();
        }
        public List<VisaoRelatorioPlanejado> RetornaRelatorioPlanejado(DateTime dataInicio, DateTime dataFinal, string consultores, string centroCusto)
        {
            var cmd = _context.Database.GetDbConnection().CreateCommand();
            _context.Database.GetDbConnection().Open();
            cmd.CommandText = "sp_RetornaRelatorioPlanejado";
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@dataInicio", dataInicio));
            cmd.Parameters.Add(new SqlParameter("@dataFinal", dataFinal));
            cmd.Parameters.Add(new SqlParameter("@Consultores", consultores));
            cmd.Parameters.Add(new SqlParameter("@CentroCusto", centroCusto));
 
            using (var rd = cmd.ExecuteReader())
            {
                List<VisaoRelatorioPlanejado> Lista = new List<VisaoRelatorioPlanejado>();
                foreach (var item in rd)
                {
                    VisaoRelatorioPlanejado data = new VisaoRelatorioPlanejado();
                    data.Nome = rd["FUNCIONARIONOME"].ToString();
                    data.Quantidade = Convert.ToDecimal(rd["QUANTIDADE"]);
                    data.CentroCustoDescricao = Convert.ToString(rd["CENTROCUSTODESCRICAO"]);
                    data.FuncionarioId = Guid.Parse(rd["USUARIOID"].ToString());
                    data.Empresa = Convert.ToString(rd["EMPRESA"]);
                    data.Login = Convert.ToString(rd["LOGIN"]);
                    data.Senioridade = Convert.ToString(rd["SENIORIDADE"]);
                    data.TipoContratacao = Convert.ToString(rd["TIPOCONTRATACAO"]);
                    data.Email = Convert.ToString(rd["EMAIL"]);
                    data.Gestor = Convert.ToString(rd["GESTOR"]);
                    data.Ano = Convert.ToInt32(rd["ANO"]);
                    data.Mes = Convert.ToInt32(rd["MES"]);

                    if (rd["DATAINICIOCONTRATO"] != DBNull.Value)
                    {
                        data.DataInicioContrato = Convert.ToDateTime(rd["DATAINICIOCONTRATO"]);
                    }
                    if (rd["DATATERMINOCONTRATO"] != DBNull.Value)
                    {
                        data.DataFimContrato = Convert.ToDateTime(rd["DATATERMINOCONTRATO"]);
                    }

                    Lista.Add(data);
                }
                _context.Database.GetDbConnection().Close();
                return Lista;
            }
        }
        public List<RelatorioDisponibilidade> RelatorioColaboradores(DateTime dataInicio, DateTime dataFinal, string[] idConsultores = null, string [] idEspecialidades = null)
        {
            var querybuilder = new StringBuilder(@"select distinct
                                    us.id,
                                    us.Nome,
                                    sen.Nome as Senioridade,
                                    us.DataInicioContrato,
                                    us.DataTerminoContrato,
                                    E.Nome as Empresa,
                                    E.id as EmpresaId,
                                    G.Nome as Gestor,
                                     us.Email as Email,
                                    (SUBSTRING(
                                        (
                                            SELECT ', ' + ESP.Competencia  AS[text()]
                                            FROM Especialidade ESP
                                            left join UsuarioEspecialidade ue
                                            on ESP.id = ue.EspecialidadeId
                                            WHERE ue.UsuarioId = us.id
                                            ORDER BY ESP.id
                                            FOR XML PATH(''), TYPE
                                        ).value('text()[1]', 'nvarchar(max)'), 2, 1000)
                                    ) as 'Especialidade', 
                                    us.Ativo as Ativo, 
                                    tp.Nome as TipoContratacao
                                    from Usuario as us
                                    left join Perfil as pe on pe.id = us.PerfilId
                                    left join UsuarioEspecialidade as ue on ue.UsuarioId = us.id
                                    left join Senioridade as sen on sen.id = us.SenioridadeId
                                    left join TipoContratacao as tp on us.TipoContratacaoId = tp.id
                                    left join Empresa as E on E.id = us.EmpresaId
                                    left join Usuario as  G on   G.id = us.GestorImediatoId
                                    where pe.Grupo in ('03','04') 
                                    and (us.ativo = 1 and ((cast(us.dataInicioContrato As Date) <= @dataFinal) OR (cast(us.DataTerminoContrato As Date) <= @dataFinal))) ");

            if (@idConsultores.Length > 0)
            {
                querybuilder.Append(" and us.id in @idConsultores ");
            }
            if (@idEspecialidades.Length > 0)
            {
                querybuilder.Append(" and ue.EspecialidadeId in @idEspecialidades ");
            }

            querybuilder.Append(" order by us.Nome ");

            return Db.Query<RelatorioDisponibilidade>(querybuilder.ToString(), new { @idConsultores, @idEspecialidades, @dataInicio, @dataFinal }).ToList();
        }

       
    }
}
