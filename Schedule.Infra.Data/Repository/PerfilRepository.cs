﻿using Schedule.Domain.Entities;
using Schedule.Domain.Interfaces.Repository;
using Schedule.Infra.Data.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Infra.Data.Repository
{
   public class PerfilRepository : RepositoryBase<Perfil, Guid>, IPerfilRepository
    {

        private readonly ScheduleContext _context;
        public PerfilRepository(ScheduleContext context) : base(context)
        {
            _context = context;

        }
    }
}