﻿using Schedule.Domain.Entities.Estrutura;
using Schedule.Domain.Interfaces.Repository.Estrutura;
using Schedule.Infra.Data.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Infra.Data.Repository.Estrutura
{
   public class PerfilMenuRepository : RepositoryBase<PerfilMenu, Guid>, IPerfilMenuRepository
    {

        private readonly ScheduleContext _context;

        public PerfilMenuRepository(ScheduleContext context) : base(context)
        {
            _context = context;

        }
    }
}
