﻿using Microsoft.Data.SqlClient;

namespace Schedule.Infra.Data.Data
{
    public interface IBancoDeDadosRepository
    {
         SqlConnection Conn { get; }

         SqlTransaction Cmtx { get; }

    }
}