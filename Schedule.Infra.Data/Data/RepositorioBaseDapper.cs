﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Infra.Data.Data
{
    public abstract class RepositorioBaseDapper
    {
        private readonly IBancoDeDadosRepository _bancoDeDadosRepository;

        protected RepositorioBaseDapper(IBancoDeDadosRepository bancoDeDadosRepository)
        {
            _bancoDeDadosRepository = bancoDeDadosRepository;
        }

        protected SqlConnection Db
        {
            get
            {
                if (_bancoDeDadosRepository.Conn == null)
                    throw new System.Exception("Conexão com Banco de Dados não está diposnível");
                return _bancoDeDadosRepository.Conn;
            }
        }

        protected SqlTransaction Trans
        {
            get
            {
                return _bancoDeDadosRepository.Cmtx;
            }
        }

    }
}
