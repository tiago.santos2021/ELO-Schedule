﻿using Microsoft.EntityFrameworkCore;
using Schedule.Domain.Entities;
using Schedule.Domain.Entities.Estrutura;
using Schedule.Infra.Data.EntityConfig;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Infra.Data.Data
{
    public class ScheduleContext : DbContext
    {

        public ScheduleContext(DbContextOptions options) : base(options)
        {

        }
        public ScheduleContext()
        {

        }
        public DbSet<Empresa> Empresa { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Senioridade> Senioridade { get; set; }
        public DbSet<TipoContratacao> TipoContratacao { get; set; }
        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<TipoCentroCusto> TipoCentroCusto { get; set; }
        public DbSet<Especialidade> Especialidade { get; set; }
        public DbSet<CustoMedio> CustoMedio { get; set; }
        public DbSet<CentroCusto> CentroCusto { get; set; }
        public DbSet<Perfil> Perfil { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<PerfilMenu> PerfilMenu { get; set; }
        public DbSet<Permissao> Permissao { get; set; }
        public DbSet<PerfilPermissao> PerfilPermissao { get; set; }
        public DbSet<UsuarioEspecialidade> UsuarioEspecialidade { get; set; }
        public DbSet<TemplateEmail> TemplateEmail { get; set; }
        public DbSet<GerenciamentoToken> GerenciamentoToken { get; set; }
        public DbSet<Agenda> Agenda { get; set; }
        public DbSet<Feriado> Feriado { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EmpresaConfig());
            modelBuilder.ApplyConfiguration(new SenioridadeConfig());
            modelBuilder.ApplyConfiguration(new TipoContratacaoConfig());
            modelBuilder.ApplyConfiguration(new ClienteConfig());
            modelBuilder.ApplyConfiguration(new TipoCentroCustoConfig());
            modelBuilder.ApplyConfiguration(new EspecialidadeConfig());
            modelBuilder.ApplyConfiguration(new CustoMedioConfig());
            modelBuilder.ApplyConfiguration(new PerfilConfig());

        }
    }
}
