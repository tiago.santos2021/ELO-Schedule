﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Infra.Data.Data
{
    public class BancoDeDadosRepository : IDisposable, IBancoDeDadosRepository
    {
        public SqlConnection Conn { get; private set; }

        public SqlTransaction _cmtTx { get; private set; }

        public SqlTransaction Cmtx => _cmtTx;

        private readonly string _conexaoPadrao;

        public BancoDeDadosRepository(IConfiguration configuration)
        {
            _conexaoPadrao = configuration.GetConnectionString("Default").ToString();
            ConectarBaseDados();
        }

        private void ConectarBaseDados()
        {
            if(Conn == null)
            {
                Conn = new SqlConnection(_conexaoPadrao);
                Conn.Open();
            }
        }

        public void Dispose()
        {
            Conn.Close();
            Conn.Dispose();
        }
    }
}
