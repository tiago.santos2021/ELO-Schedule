﻿using Microsoft.Extensions.DependencyInjection;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Interfaces.Services;
using Schedule.Application.Interfaces.Services.Estrutura;
using Schedule.Application.Interfaces.Services.Relatorio;
using Schedule.Application.Interfaces.Services.Resorces;
using Schedule.Application.Services;
using Schedule.Application.Services.Estrutura;
using Schedule.Application.Services.Relatorio;
using Schedule.Application.Services.Relatorios;
using Schedule.Application.Services.Resorces;

namespace Schedule.IoC.DI
{
    public static class Service
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IAccountService, AccountServices>();
            services.AddScoped<IEmpresaService, EmpresaService>();
            services.AddScoped<ISenioridadeService, SenioridadeService>();
            services.AddScoped<ITipoContratacaoService, TipoContratacaoService>();
            services.AddScoped<IClienteService, ClienteService>();
            services.AddScoped<ITipoCentroCustoService, TipoCentroCustoService>();
            services.AddScoped<IEspecialidadeService, EspecialidadeService>();
            services.AddScoped<ICustoMedioService, CustoMedioService>();
            services.AddScoped<ICentroCustoService, CentroCustoService>();
            services.AddScoped<IUsuarioService, UsuarioService>();
            services.AddScoped<IPerfilService, PerfilService>();
            services.AddScoped<IBaseUFService, BaseUFService>();
            services.AddScoped<IMenuService, MenuService>();
            services.AddScoped<IPermissaoService, PermissaoService>();
            services.AddScoped<IAlocacaoService, AlocacaoService>(); 
            services.AddScoped<IRelatorioService, RelatorioService>();
            services.AddScoped<IRelatorioDisponibilidadeService, RelatorioDisponibilidadeService>();
            services.AddScoped<IFeriadosService, FeriadosService>();
            return services;
        }
    }
}
