﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

using Schedule.Application.ViewModel;
using Schedule.Application.ViewModel.Estrutura;
using Schedule.Domain.Entities;
using Schedule.Domain.Entities.Estrutura;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.IoC.DI
{
    public static class AutoMapping
    {

        public static IServiceCollection AddAutoMapping(this IServiceCollection repository)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Empresa, EmpresaViewModel>().ReverseMap();
                cfg.CreateMap<Usuario, UsuarioViewModel>().ReverseMap();
                cfg.CreateMap<Senioridade, SenioridadeViewModel>().ReverseMap();
                cfg.CreateMap<TipoContratacao, TipoContratacaoViewModel>().ReverseMap();
                cfg.CreateMap<Cliente, ClienteViewModel>().ReverseMap();
                cfg.CreateMap<TipoCentroCusto, TipoCentroCustoViewModel>().ReverseMap();
                cfg.CreateMap<Especialidade, EspecialidadeViewModel>().ReverseMap();
                cfg.CreateMap<CustoMedio, CustoMedioViewModel>().ReverseMap();
                cfg.CreateMap<CentroCusto, CentroCustoViewModel>().ReverseMap();
                cfg.CreateMap<Perfil, PerfilViewModel>().ReverseMap();
                cfg.CreateMap<Menu, MenuViewModel>().ReverseMap();
                cfg.CreateMap<Permissao, PermissaoViewModel>().ReverseMap();
                cfg.CreateMap<Feriado, FeriadosViewModel>().ReverseMap();

            });

            IMapper mapper = config.CreateMapper();
            repository.AddSingleton(mapper);

            return repository;
        }







    }
}
