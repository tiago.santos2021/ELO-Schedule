﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Schedule.Domain.Interfaces;
using Schedule.Domain.Interfaces.Repository;
using Schedule.Domain.Interfaces.Repository.Estrutura;
using Schedule.Domain.Interfaces.Repository.Relatorio;
using Schedule.Infra.Data.Data;
using Schedule.Infra.Data.Repository;
using Schedule.Infra.Data.Repository.Estrutura;
using Schedule.Infra.Data.Repository.Relatorio;

namespace Schedule.IoC.DI
{

    public static class Repository
    {
        public static IServiceCollection AddRepositories(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ScheduleContext>(options => options.UseSqlServer(configuration.GetConnectionString("Default")));

            services.AddScoped<IEmpresaRepository, EmpresaRepository>();
            services.AddScoped<IUsuarioRepository, UsuarioRepository>();
            services.AddScoped<ISenioridadeRepository, SenioridadeRepository>();
            services.AddScoped<ITipoContratacaoRepository, TipoContratacaoRepository>();
            services.AddScoped<IClienteRepository, ClienteRepository>();
            services.AddScoped<ITipoCentroCustoRepository, TipoCentroCustoRepository>();
            services.AddScoped<IEspecialidadeRepository, EspecialidadeRepository>();
            services.AddScoped<ICustoMedioRepository, CustoMedioRepository>();
            services.AddScoped<ICentroDeCustoRepository, CentroCustoRepository>();
            services.AddScoped<IPerfilRepository, PerfilRepository>();
            services.AddScoped<IMenuRepository, MenuRepository>();
            services.AddScoped<IPerfilMenuRepository, PerfilMenuRepository>();
            services.AddScoped<IUsuarioEspecialidadeRepository, UsuarioEspecialidadeRepository>();
            services.AddScoped<IPerfilPermissaoRepository, PerfilPermissaoRepository>();
            services.AddScoped<IPermissaoRepository, PermissaoRepository>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<IRelatorioRepository, RelatorioRepository>();
            services.AddScoped<IAlocacaoRepository, AlocacaoRepository>();
            services.AddScoped<IBancoDeDadosRepository, BancoDeDadosRepository>();
            services.AddScoped<IFeriadosRepository, FeriadosRepository>();
            return services;
        }
    }







}
