﻿using Schedule.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Util.Validacao
{
    public static class Validacao
    {
        private static DateTime DataMinima = new DateTime(1973, 1, 1);
        private static DateTime DataMaxima = new DateTime(9999, 12, 31);


        public static void ValidaLimitePermetidoData(DateTime data)
        {
            if (data <= DataMinima || data > DataMaxima)
            {
                throw new DomainException("Atenção verifique os campos de data, valores menores ou maiores do que o sistema suporta");
            }
        }

        public static bool VerificaData(string data)
        {
            bool resultado = true;
            DateTime datavalida;
            if (!DateTime.TryParse(data, out datavalida))
            {
                resultado = false;
            }
            if (datavalida == DateTime.MinValue)
            {
                resultado = false;
            }
            return resultado;
        }





    }
}
