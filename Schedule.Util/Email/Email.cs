﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace Schedule.Util.Email
{
   public class Email
    {
        public string emailPara { get; set; }
        public string Assunto { get; set; }
        public string NomeDestinatario { get; set; }
        public string Corpo { get; set; }
        public string Rementente { get; set; }
        public string Senha { get; set; }
        public string Dispaly { get; set; }

        public static Tuple<bool,string> EnviarEmail(string emailPara, string assunto, string nomeUsuario, string html)
        {
            MailAddressCollection TO_addressList = new MailAddressCollection();
            foreach (var email in emailPara.Split(","))
            {
                MailAddress mytoAddress = new MailAddress(email);
                TO_addressList.Add(mytoAddress);
            }

            SmtpClient client = new SmtpClient("smtp.gmail.com");
            client.Port = 587;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("lipemarqs92@gmail.com", "afjbztyftzhlvqar");

            MailMessage mensagem = new MailMessage();
            mensagem.Sender = new MailAddress("lipemarqs92@gmail.com", "Bruno Oliveira");
            mensagem.From = new MailAddress("lipemarqs92@gmail.com", "Bruno Oliveira");
            mensagem.To.Add(TO_addressList.ToString());
            mensagem.Subject = assunto;
            mensagem.Body = html;
            mensagem.IsBodyHtml = true;

            try
            {
                client.Send(mensagem);
                return new Tuple<bool, string>(true, "Email enviado com sucesso");
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, "Falha ao enviar o email: " + ex.Message);
            }            
        }
    }
}
