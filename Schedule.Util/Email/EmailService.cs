﻿using System;
using System.Net;
using System.Net.Mail;

namespace Schedule.Util.Email
{
    public static class EmailService
    {
      
        public static Tuple<bool,string> EnviarEmail(Email email)
        {
            SmtpClient client = new SmtpClient("smtp.gmail.com");
            client.Port = 587;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(email.Rementente, email.Senha);

            MailMessage mensagem = new MailMessage();
            mensagem.Sender = new MailAddress(email.Rementente, email.Dispaly);
            mensagem.From = new MailAddress(email.Rementente, email.Dispaly);
            mensagem.To.Add(new MailAddress(email.emailPara, email.NomeDestinatario));
            mensagem.Subject = email.Assunto;
            mensagem.Body = email.Corpo;
            mensagem.IsBodyHtml = true;

            try
            {
                client.Send(mensagem);
                return new Tuple<bool, string>(true, "Aguarde Você receberá um email com um Link para Alteração de Senha");
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, "Falha ao enviar o email: " + ex.Message);
            }            
        }
    }
}
