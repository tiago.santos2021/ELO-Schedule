﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Schedule.Util.Formatacao
{
    public static class Formatacao
    {

        public static string FormataMilhar(decimal valor)
        {
            return string.Format("{0:N}", valor);
        }

        public static string ExibirMesPorExtenso(int Mes)
        {
            string retorno = "";
            switch (Mes)
            {
                case 1:
                    retorno = "JAN";
                    break;
                case 2:
                    retorno = "FEV";
                    break;
                case 3:
                    retorno = "MAR";
                    break;

                case 4:
                    retorno = "ABRI";
                    break;
                case 5:
                    retorno = "MAIO";
                    break;
                case 6:
                    retorno = "JUN";
                    break;
                case 7:
                    retorno = "JUL";
                    break;
                case 8:
                    retorno = "AGO";
                    break;
                case 9:
                    retorno = "SET";
                    break;

                case 10:
                    retorno = "OUT";
                    break;
                case 11:
                    retorno = "NOV";
                    break;
                case 12:
                    retorno = "DEZ";
                    break;

            }

            return retorno;
        }

    }
}
