﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Schedule.Domain.Entities.Estrutura;
using System.Drawing;

namespace Schedule.Util.GeradorExcel
{
    public class GeradorExcel
    {
        public static byte[] RelatorioUsuarios<T>(List<T> listaGenerica, string nomeplanilha)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage excel = new ExcelPackage();
            
            var workSheet = excel.Workbook.Worksheets.Add(nomeplanilha);
            
            workSheet.TabColor = System.Drawing.Color.Black;
            workSheet.DefaultRowHeight = 12;
            
            workSheet.Row(1).Height = 20;
            workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(1).Style.Font.Bold = true;

            PropertyInfo[] propertyInfos;
            propertyInfos = typeof(T).GetProperties();

            int contadorColunaCabecalho = 1;
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                workSheet.Cells[1, contadorColunaCabecalho].Value = propertyInfo.Name;
                contadorColunaCabecalho++;
            }

            workSheet.Cells[1, 1, 1, contadorColunaCabecalho-1].Style.Font.Color.SetColor(Color.White);
            workSheet.Cells[1, 1, 1, contadorColunaCabecalho-1].Style.Fill.PatternType = ExcelFillStyle.Solid;
            workSheet.Cells[1, 1, 1, contadorColunaCabecalho-1].Style.Fill.BackgroundColor.SetColor(Color.Blue);
            workSheet.Cells[1, 1, 1, contadorColunaCabecalho-1].Style.Font.Bold = true;

            int indiceListaGenerica = 2;

            foreach (var itemGerico in listaGenerica)
            {
                int indiceColuna = 1;

                foreach (PropertyInfo propertyInfo in propertyInfos)
                {
                    var propObjeto = itemGerico.GetType().GetProperty(propertyInfo.Name);
                    var propValue = itemGerico.GetType().GetProperty(propertyInfo.Name).GetValue(itemGerico);
                    workSheet.Cells[indiceListaGenerica, indiceColuna].Value = propValue;
                    indiceColuna++;
                }
                indiceListaGenerica++;
            }
            workSheet.Columns.AutoFit();

            return excel.GetAsByteArray();
        }
        
    }


}
