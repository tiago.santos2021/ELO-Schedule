﻿$(document).ready(function () {

    $('.delete').on('click', function () {
        $('span.nome').text($(this).data('codigo'));
        var actionLink = "/Usuario/DesativarUsuario/" + $(this).data('id');;
        $('a.delete-yes').attr('href', actionLink);
        $('#Modal').modal('show');
    });
    $("#DataInicioContrato").datepicker();
    $("#DataTerminoContrato").datepicker();
    $('#DataInicioContrato').mask('00/00/0000');
    $('#DataTerminoContrato').mask('00/00/0000');
    $('#DDD1').mask('00');
    $('#DDD2').mask('00');
    $('#Telefone1').mask('00000-0000');
    $('#Telefone2').mask('00000-0000');



    $('#ListaEspecialidadesId').multiselect({
        enableFiltering: true,
        maxHeight: 300,
       
        filterPlaceholder: 'Pesquisar...',
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: false,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum Item Selecionado...',
        allSelectedText: "Todos os itens estão selecionados",
    });


    $("#ValorHora").mask('##.###,00', {
        reverse: true
    });

    $("#ValorMensal").mask('##.###,00', {
        reverse: true
    });


    $("#ValorHora").on('keypress', function (e) {
        var $this = $(this);
        var key = (window.event) ? event.keyCode : e.which;

        if ((key > 47 && key < 58)
            || (key == 44)) {
            return true;
        } else {
            return (key == 8 || key == 0) ? true : false;
        }
    });
    $("#ValorMensal").on('keypress', function (e) {
        var $this = $(this);
        var key = (window.event) ? event.keyCode : e.which;

        if ((key > 47 && key < 58)
            || (key == 44)) {
            return true;
        } else {
            return (key == 8 || key == 0) ? true : false;
        }
    });

    $("#PerfilId").change(function (e) {
        debugger;
        $("#DivValidation").empty();
        if ($("#PerfilId").val().toLowerCase() == $("#HddTipoConsultor").val().toLowerCase() || $("#PerfilId").val().toLowerCase() == $("#HddTtipoGestaoComAlocacao").val().toLowerCase())
        {
          
            $("#DivConsultor").css('display', 'block');
        }
        else {
            $("#DivConsultor").css('display', 'none');
        }
    });

    $('#tbListaUsuarios').DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.11.3/i18n/pt_br.json"
        },
        "columnDefs": [
            { "targets": 8, "type": "date-eu" },
            { "targets": 9, "type": "date-eu" }

        ],
    });

    $('#gerarexcel').on('click', function () {

        $.ajax({
            type: "GET",
            url: "./Usuario/GerarExcel",
            xhrFields: {
                responseType: 'blob'
            },
            success: function (data) {
                var a = document.createElement('a');
                var url = window.URL.createObjectURL(data);
                a.href = url;
                a.download = `Planilha_Usuarios_${(new Date()).toLocaleDateString()}.xlsx`;
                document.body.append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(url);
            }
        });

    });
        

});