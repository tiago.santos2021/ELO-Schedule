﻿$(document).ready(function () {

   
    const FROM_PATTERN = 'DD/MM/YYYY';
    const TO_PATTERN = 'DD/MM/YYYY';

    $("#PesquisarVisaoColaborador").click(function () {
        CarregarVisaoColaborador();
        $("#DivDetalheVisaoColaborador").css("display", "block");
    });

    function ValidaSolicitacao() {

        let validacao = true;

        if ($('#DataInicial').val() == '' || $('#Datafinal').val() == '') {

   
            $("#spanMensagem").text("O Campo Data de início e data final são Obrigatórios")


            validacao = false

        }

        return validacao;

    }

    $("#PesquisaVisaoPlanejado").click(function () {

        $("#spanMensagem").text("")
        $("#DivVisaoPlanejado").hide();
        let FormValido = ValidaSolicitacao();
        if (FormValido) {
          
            var data = {
                ListaConsultores: $('#ListaConsultores').val(),
                ListaEspecialidades: $('#ListaEspecialidades').val(),
                ListaCentroCustos: $('#ListaCentroCustos').val(),
                ListaEmpresas: $('#ListaEmpresas').val(),
                DataInicio: $('#DataInicial').val(),
                DataFinal: $('#Datafinal').val(),
            };

            $.ajax({
                type: "POST",
                url: "/Visao/Planejado",
                data: data,
                dataType: "json",
                success: function (data) {
                   
                    let table = $('#tbListaRelatorioPlanejado').DataTable();
                    table.clear();
                    table.destroy();

                    if (data.erro != null && data.erro != '') {
                       
                        $("#spanMensagem").text(data.erro)
                       
                    }
                    else {
                        console.log(data);
                        //$("#DivVisaoPlanejado").show();
                        //CarregarVisaoPlanejado(data.dados);

                        $('#tbListaRelatorioPlanejado').DataTable({
                            "language": {
                                "url": "https://cdn.datatables.net/plug-ins/1.11.3/i18n/pt_br.json",
                            },
                            dom: 'lBftip',
                            buttons: [
                                {
                                    extend: 'excelHtml5', text: 'Exportar', title: 'RelatorioPlanejado', className: 'btn btnExportaRelatorioPlanejado exp-avan',
                                    exportOptions: {
                                        columns: ':visible'
                                    }
                                },
                               

                            ],
                            "retrieve": true,
                            "data": data.dados,
                            "paging": true,
                            "columnDefs": [
                                { "orderData": 0, "targets": 11 },
                                { "visible": false, "targets": 0 }],


                            "columns": [
                                { data: 'ordem' },
                                { data: 'empresa'},
                                { data: 'nome' },
                                { data: 'login'},
                                { data: 'senioridade'},
                                { data: 'tipoContratacao'},
                                { data: 'dataInicioContratoExibicao', "type": "date-eu" },
                                { data: 'dataFimContratoExibicao', "type": "date-eu"  },
                                { data: 'especialidade', width: '300px'},
                                { data: 'email' },
                                { data: 'gestor' },
                                { data: 'mesAno' },
                                { data: 'quantidade'},
                                { data: 'custo'},
                                { data: 'centroCustoDescricao' },
                               
                            ],
                            
                           
                        });

                        
                    }


                },
                error: function (error) {
                    $("#spanMensagem").text("Erro ao estabelecer conexão, sua sessão expirou.por favor Se autentique novamente ");
                }
            });

        }

    });
    $("#PesquisaVisaoPlanejadoRealizado").click(function () {
        CarregarVisaoRealizadoPlanejado();
    });

    $('#ListaConsultores').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        filterPlaceholder: 'Pesquisar...',
        selectAllText: "Selecionar Todos",
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum Item Selecionado...',
        allSelectedText: "Todos os itens estão selecionados",
    });

    $('#ListaEspecialidades').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        selectAllText: "Selecionar Todos",
        filterPlaceholder: 'Pesquisar...',
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum Item Selecionado...',
        allSelectedText: "Todos os itens estão selecionados",
    });

    $('#ListaCentroCustos').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        selectAllText: "Selecionar Todos",
        filterPlaceholder: 'Pesquisar...',
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum Item Selecionado...',
        allSelectedText: "Todos os itens estão selecionados",
    });
    $('#ListaEmpresas').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        selectAllText: "Selecionar Todos",
        filterPlaceholder: 'Pesquisar...',
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum Item Selecionado...',
        allSelectedText: "Todos os itens estão selecionados",
    });
    $(function () {
        $("#DataInicial").datepicker();
        $("#Datafinal").datepicker();
        $('#DataInicial').mask('00/00/0000');
        $('#Datafinal').mask('00/00/0000');


    });

    function CarregarVisaoPlanejado(data) {
        Highcharts.chart('DivVisaoPlanejado', {
            chart: { type: 'bar' },


            lang: {
                printChart: 'Print Gráfico',
                downloadPNG: 'Download PNG',
                downloadJPEG: 'Download JPEG',
                downloadPDF: 'Download PDF',
                downloadSVG: 'Download SVG',
                contextButtonTitle: 'Context menu'
            },
            exporting: {
                buttons: {
                    contextButton: {
                        menuItems: ["printChart",
                            "separator",
                            "downloadPNG",
                            "downloadJPEG",
                            "downloadPDF",
                            "downloadSVG",
                            "separator",
                            "downloadCSV",
                            "downloadXLS",
                            "openInCloud"]
                    }
                }
            },
            navigation: {
                buttonOptions: {
                    height: 20,
                    width: 30,
                    symbolSize: 12,
                    symbolX: 10,
                    symbolY: 10,
                    symbolStrokeWidth: 2
                }
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Horas Planejadas Colaborador ',
                style: {
                    color: '#000000',
                    fontWeight: 'bold',
                    fontSize: '30px',
                }

            },
            //subtitle: {
            //    text: 'Competência: Julho',
            //    style: {
            //        color: '#0000CD',
            //        fontWeight: 'bold',
            //        fontSize: '25px',
            //    }
            //},

            plotOptions: {
                series: {
                    pointWidth: 20
                }
            },
            xAxis: {
                type: 'category',
                lineColor: '#000000',
                lineWidth: 1,
                labels: {
                    rotation: 0,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                max: 200,
                lineWidth: 1,
                title: {
                    text: 'Horas'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Total de Horas: <b>{point.y:.1f}</b>'
            },
            series: [{
                name: 'Recursos',
                color: '#003b87',

                data: data,
                dataLabels: {
                    enabled: true,
                    rotation: 0,
                    color: '#FF0000',
                    align: 'top',

                    /*format: '{point.y:.1f}', // one decimal*/
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '11px',
                        fontFamily: 'Verdana, sans-serif',
                        color: '#003b87',
                    }
                }
            }]
        });
    }

    function CarregarVisaoColaborador() {

        Highcharts.chart('DivVisaoColaborador', {
            chart: {
                type: 'line'
            },
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            title: {
                text: 'Planejamento Anual Coloborador',

                style: {
                    color: '#000000',
                    fontWeight: 'bold',
                    fontSize: '30px',
                }
            },
            subtitle: {
                text: 'Erica Uyeda',
                style: {
                    color: '#0000CD',
                    fontWeight: 'bold',
                    fontSize: '25px',
                }
            },

            xAxis: {
                style: {
                    color: '#ff0000',
                    fontWeight: 'bold',

                },
                categories: [
                    'Janeiro',
                    'Fevereiro',
                    'Março',
                    'Abril',
                    'Maio',
                    'Junho',
                    'Julho',
                    'Agosto',
                    'Setembro',
                    'Outubro',
                    'Novembro',
                    'Dezembro'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                max: 250,
                title: {
                    text: 'Número de Horas',
                    style: {
                        color: '#003b87',
                        fontWeight: 'bold',

                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:15px">{point.key}</span><table>',
                pointFormat: '<tr>' +
                    '<td style="padding:0"><b>{point.y:.1f} Horas</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Erica',
                data: [11.9, 71.5, 206.4, 229.2, 240.0, 200.0, 165.6, 168.5, 168.4, 168.1, 95.6, 54.4],
                color: '#FF0000'

            }


            ]
        });
    }

    function CarregarVisaoRealizadoPlanejado() {

        Highcharts.chart('DivVisaoPlanejadoRealizado', {
            chart: {
                type: 'line'
            },
            credits: {
                enabled: false
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                fontSize: '15px',
            },
            title: {
                text: 'Planejado X Realizado Anual',

                style: {
                    color: '#000000',
                    fontWeight: 'bold',
                    fontSize: '30px',
                }
            },
            subtitle: {
                text: 'Erica Uyeda',
                style: {
                    color: '#0000CD',
                    fontWeight: 'bold',
                    fontSize: '25px',
                }
            },

            xAxis: {
                style: {
                    color: '#ff0000',
                    fontWeight: 'bold',

                },
                categories: [
                    'Janeiro',
                    'Fevereiro',
                    'Março',
                    'Abril',
                    'Maio',
                    'Junho',
                    'Julho',
                    'Agosto',
                    'Setembro',
                    'Outubro',
                    'Novembro',
                    'Dezembro'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                max: 250,
                title: {
                    text: 'Número de Horas',
                    style: {
                        color: '#003b87',
                        fontWeight: 'bold',

                    }
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:15px">{point.key}</span><table>',
                pointFormat: '<tr>' +
                    '<td style="padding:0"><b>{point.y:.1f} Horas</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Realizado',
                data: [168.9, 189.5, 206.4, 177.2, 117.0, 99.0, 153.6, 168.5, 200.4, 220.1, 157.6, 166.4],
                color: '#003b87'

            },

            {
                name: 'Planejado',
                data: [172.9, 179.5, 248.4, 174.2, 166.0, 95.0, 105.6, 168.5, 110.4, 222.1, 158.6, 116.4],
                color: '#32CD32'

            }


            ]
        });
    }
});