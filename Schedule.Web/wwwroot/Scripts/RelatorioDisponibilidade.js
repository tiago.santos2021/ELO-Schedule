﻿
$(document).ready(function () {

    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    $("#dataInicial").datepicker().datepicker("setDate", firstDay);
    $("#dataFinal").datepicker().datepicker("setDate", lastDay);
    $('#dataInicial').mask('00/00/0000');
    $('#dataFinal').mask('00/00/0000');

    const FROM_PATTERN = 'MM/DD/YYYY';
    const TO_PATTERN = 'DD/MM/YYYY';
   
    $("#btnPesquisar").click(function () {
        //nomes
        // let nomes = $(".multiselect-selected-text")[0].innerText

        //especialidades
        //let especialidades = $(".multiselect-selected-text")[1].innerText

        if ($('#dataInicial').val() == '' || $('#dataInicial').val() == '') {
            alert("Os campos de datas precisam ser preenchidos")
            return;
        }

        let dtIni = $('#dataInicial').val();
        let datainicialTestar = new Date(dtIni.substring(6, 10), dtIni.substring(3, 5) - 1, dtIni.substring(0, 2)); 

        let dtFin = $('#dataFinal').val();
        let datafinalTestar = new Date(dtFin.substring(6, 10), dtFin.substring(3, 5) - 1, dtFin.substring(0, 2));
        console.log("comparativo", datainicialTestar, datafinalTestar)
        if (datainicialTestar > datafinalTestar) {
            alert("Data incial não pode ser maior que data final")
            return;
        }

        var data = {
            dataInicial: $('#dataInicial').val(),
            dataFinal: $('#dataFinal').val(),
        };

        let idConsultores = $("#ListaConsultores").val().length == 0 ? null : $("#ListaConsultores").val();
        if (idConsultores != null) {
            data.idConsultores = idConsultores;
        }

        let idEspecialidades = $("#ListaEspecialidades").val().length == 0 ? null : $("#ListaEspecialidades").val();
        if (idEspecialidades != null) {
            data.idEspecialidades = idEspecialidades;
        }

        let idEmpresas = $("#ListaEmpresas").val().length == 0 ? null : $("#ListaEmpresas").val();
        if (idEmpresas != null) {
            data.idEmpresas = idEmpresas;
        }



        let dadosRelatorio = null;
        $.ajax({
            type: "POST",
            url: "/RelatorioDisponibilidade/DadosRelatorio",
            data: data,
            dataType: "json",
            success: function (data) {
                console.log('carregados dados relatório');
                dadosRelatorio = data;
                console.log(data);
                if ($.fn.dataTable.isDataTable('#tbListaDisponibilidade')) {
                    let table = $('#tbListaDisponibilidade').DataTable();
                    table.destroy();
                }
               
               //let dadosRelatorio = dadosRelatorio.map((d) => d.dataInicioContrato.split(0, 10), d.dataTerminoContrato.split(0, 10) )
               // console.log("dadosRelatorio", dadosRelatorio);

                for (var i = 0; i < dadosRelatorio.length; i++) {

                    if (dadosRelatorio[i].dataInicioContrato != undefined) {
                        dadosRelatorio[i].dataInicioContrato = dadosRelatorio[i].dataInicioContrato.substring(0, 10);
                    }                    

                    if (dadosRelatorio[i].dataTerminoContrato != undefined) {
                        dadosRelatorio[i].dataTerminoContrato = dadosRelatorio[i].dataTerminoContrato.substring(0, 10);
                    }
                    
                }
                let datahora = dataAtualFormatada();
                $('#tbListaDisponibilidade').DataTable({
                    "language": {
                        "url": "https://cdn.datatables.net/plug-ins/1.11.3/i18n/pt_br.json",
                    },
                    "retrieve": true ,                     
                    "data": dadosRelatorio,
                    "paging": true,
                    "columns": [
                        { data: 'empresa', width: '120px'},
                        { data: 'nome', width: '120px'},
                        { data: 'senioridade' },
                        { data: 'tipoContratacao' },
                        { data: 'gestor', width: '200px' },
                        { data: 'email', width: '200px' },
                        { data: 'dataInicioContrato', render: $.fn.dataTable.render.moment(FROM_PATTERN, TO_PATTERN)},
                        {data: 'dataTerminoContrato', render: $.fn.dataTable.render.moment(FROM_PATTERN, TO_PATTERN)},
                        { data: 'especialidade' },
                        { data: 'horasDisponiveis' },                        
                    ],
                    dom: 'lBftip',
                    buttons: [
                        {
                            extend: 'excel', text: 'Exportar', title: 'Relatório_Disponibilidade_Simplificado_' + datahora, className: 'btn btn-light  exp-simp'                         
                        }],
                    "paging": true,
                    "pagingType": "full_numbers"
                });

                $('#tbListaDisponibilidade').attr({ "style": "width:100%" });

                function DadoConsultor(nome, senioridade) {
                    this.nome = nome;                   
                    this.senioridade = senioridade;

                    this._dados = function () {
                        return 
                    }
                };

                consultarDiasPeriodo(dadosRelatorio);
            },
            error: function (error) {
                $("#spanMensagem").text("Erro ao estabelecer conexão, sua sessão expirou.por favor Se autentique novamente ");
            }
        });



        function consultarDiasPeriodo(dadosRelatorio) {

            $.ajax({
                type: "POST",
                url: "/RelatorioDisponibilidade/ConsultarDiasPeriodo",
                data: data,
                dataType: "json",
                success: function (data) {
                    let listaDiasPeriodo = data;
                    //limpar
                   // console.log('listaDiasPeriodo');

                    if ($.fn.dataTable.isDataTable('#tbRelatorioAvancado')) {
                        $('#tbRelatorioAvancado').DataTable().destroy();
                    }

                    $("#tbRelatorioAvancado tbody").html('');

                    $("#tbRelatorioAvancado thead").innerHTML = '';

                    $("#cabecalhoInferior").html('').append('<td></td>'); 

                    listaDiasPeriodo.length;
                    $("#labelDatas").attr({ 'colspan': listaDiasPeriodo.length })

                    listaDiasPeriodo.map(function (diaPeriodo) {


                        $("#tbRelatorioAvancado #cabecalhoInferior").append(`<th  >${diaPeriodo.dia.substring(8, 10) }-${ diaPeriodo.dia.substring(5, 7)}</th>`);
                    });

                    dadosRelatorio.map(function (colaborador) {

                        $("#tbRelatorioAvancado tbody").append(`<tr id=${colaborador.id} > <td> ${colaborador.nome}  </td> </tr>`)
                        //colaborador
                        for (var i = 0; i < listaDiasPeriodo.length; i++) {

                            //console.log("colabarador", colaborador.nome);
                            try {

                                let classeCssTeste = '';
                                if (listaDiasPeriodo[i].statusDia == 1)
                                {
                                    classeCssTeste = 'diaUtil';
                                } else if (listaDiasPeriodo[i].statusDia == 2 || listaDiasPeriodo[i].statusDia == 3) {
                                    classeCssTeste = 'diaNaoUtil';
                                }

                                let dataPeriodo = listaDiasPeriodo[i].dia.substring(0, 10);
                                let diasColab = colaborador.agenda.filter(x => x.dia.substring(0, 10) == dataPeriodo);
                                let registro = "";
                                if (diasColab.length == 0) {
                                    registro = `<td class="${classeCssTeste}"></td>`;
                                }
                                else if (diasColab.length == 1) {
                                    console.log("diasColab", diasColab);

                                    if (diasColab[0].statusAgenda == 1 && diasColab[0].periodo == 1) {
                                        registro = `<td class="${classeCssTeste} center"></td>`;
                                    } else if (diasColab[0].statusAgenda == 1 && diasColab[0].periodo == 2) {
                                        registro = `<td class="${classeCssTeste} center"></td>`;
                                    }
                                    else if (diasColab[0].statusAgenda == 2 && diasColab[0].periodo == 1) {
                                        registro = `<td class="${classeCssTeste} center">x|</td>`;

                                    } else if (diasColab[0].statusAgenda == 2 && diasColab[0].periodo == 2) {
                                        registro = `<td class="${classeCssTeste} center">|x</td>`;
                                    }
                                }
                                else if (diasColab.filter(x => x.statusAgenda == 1).length == 2) {
                                    registro = `<td class="${classeCssTeste} center"></td>`;
                                }
                                else if (diasColab.filter(x => x.statusAgenda == 2).length == 2) {
                                    registro = `<td class="${classeCssTeste} center">x|x</td>`;
                                }
                                else if (diasColab[0].statusAgenda == 1 && diasColab[1].statusAgenda == 2) {
                                    registro = `<td class="${classeCssTeste} center">|x</td>`
                                }
                                else if (diasColab[0].statusAgenda == 2 && diasColab[1].statusAgenda == 1) {
                                    registro = `<td class="${classeCssTeste} center" >x|</td>`
                                } else {
                                    console.log("conflito agenda")
                                    registro = `<td>erro</td>` 
                                }

                                $(`#${colaborador.id}`).append(registro);

                            } catch (e) {
                                console.log('nome',colaborador.nome);
                                console.log(e);
                            }
                        }
                    });

                    console.log("reiniciou datatable")
                    try {

                        let datahora = dataAtualFormatada();

                        $('#tbRelatorioAvancado').DataTable({
                            "language": {
                                "url": "https://cdn.datatables.net/plug-ins/1.11.3/i18n/pt_br.json",
                            },
                            dom: 'lBftip',
                            buttons: [
                                {
                                    extend: 'excelHtml5', text: 'Exportar', title: 'Relatório_Disponibilidade_Avançado_' + datahora, className: 'btn btn-light exp-avan'
                                }],
                            "paging": true,
                            "pagingType": "full_numbers",

                        });

                    } catch (e) {
                        console.log("destroy erro", e);
                    }

                },
                error: function (error) {
                    $("#spanMensagem").text("Erro ao estabelecer conexão, sua sessão expirou.por favor Se autentique novamente ");
                }
            });

        }
       
    }); 

    function dataAtualFormatada() {
        var data = new Date(),
            dia = data.getDate().toString(),
            diaF = (dia.length == 1) ? '0' + dia : dia,
            mes = (data.getMonth() + 1).toString(), //+1 pois no getMonth Janeiro começa com zero.
            mesF = (mes.length == 1) ? '0' + mes : mes,
            anoF = data.getFullYear();
        return diaF + "_" + mesF + "_" + anoF;
    }

    $("#btnExport").click(function () {
        //fnExcelReport('tbListaDisponibilidade');
        //fnExcelReport('tbRelatorioAvancado');

        $(".exp-simp").trigger("click");
        $(".exp-avan").trigger("click");

    });

    $('#ListaConsultores').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        filterPlaceholder: 'Pesquisar...',
        selectAllText: "Selecionar Todos",
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum Consultor Selecionado...',
        allSelectedText: "Todos os itens estão selecionados",
    });

    $('#ListaEspecialidades').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        selectAllText: "Selecionar Todas",
        filterPlaceholder: 'Pesquisar...',
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhuma Especialidade Selecionada...',
        allSelectedText: "Todos os itens estão selecionados",
    });



    $('#ListaEmpresas').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        selectAllText: "Selecionar Todas",
        filterPlaceholder: 'Pesquisar...',
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhuma Empresa Selecionada...',
        allSelectedText: "Todos os itens estão selecionados",
    });
})
