﻿$(document).ready(function () {


    $("#DataInicioContrato").datepicker();
    $("#DataFinalContrato").datepicker();
    $('#DataInicioContrato').mask('00/00/0000');
    $('#DataFinalContrato').mask('00/00/0000');

    $('.delete').on('click', function () {
        $('span.nome').text($(this).data('codigo'));
        var actionLink = "/CentroCusto/Excluir/" + $(this).data('id');;
        $('a.delete-yes').attr('href', actionLink);
        $('#Modal').modal('show');
    });

    $('#ResponsavelId').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        filterPlaceholder: 'Pesquisar...',
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: false,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum Item Selecionado...',
       /* allSelectedText: "Todos os itens estão selecionados",*/
    });

    $("#Taxa").mask('##.###,00', {
        reverse: true
    });
    $("#Taxa").on('keypress', function (e) {
        var $this = $(this);
        var key = (window.event) ? event.keyCode : e.which;

        if ((key > 47 && key < 58)
            || (key == 44)) {
            return true;
        } else {
            return (key == 8 || key == 0) ? true : false;
        }
    });

    $('#tbListaCentroCusto').DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.11.3/i18n/pt_br.json"
        },
        "columnDefs": [
            { "targets": 6, "type": "date-eu" },
            { "targets": 7, "type": "date-eu" }

        ],
    });
});