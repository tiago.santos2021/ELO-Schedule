﻿$(document).ready(function () {

    $('#EspecialidadeId').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        filterPlaceholder: 'Pesquisar...',
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum iten Selecionado...',
        allSelectedText: "Todos os itens estão selecionados",
    });


    $("#Valor").mask('##.###,00', {
        reverse: true
    });



    $('.delete').on('click', function () {
        $('span.nome').text($(this).data('codigo'));
        var actionLink = "/CustoMedio/Excluir/" + $(this).data('id');;
        $('a.delete-yes').attr('href', actionLink);
        $('#Modal').modal('show');
    });

    $("#Valor").on('keypress', function (e) {
        var $this = $(this);
        var key = (window.event) ? event.keyCode : e.which;

        if ((key > 47 && key < 58)
            || (key == 44)) {
            return true;
        } else {
            return (key == 8 || key == 0) ? true : false;
        }
    });

    $('#tbListaEmpresas').DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.11.3/i18n/pt_br.json"
        }
    });
});