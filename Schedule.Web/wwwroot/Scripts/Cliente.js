﻿$(document).ready(function () {

    $('.delete').on('click', function () {
        $('span.nome').text($(this).data('codigo'));
        var actionLink = "/Cliente/Excluir/" + $(this).data('id');;
        $('a.delete-yes').attr('href', actionLink);
        $('#Modal').modal('show');
    });

    $('#tbListaEmpresas').DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.11.3/i18n/pt_br.json"
        }
    });
});