﻿$(document).ready(function () {

    window.addEventListener('load', CarregaPaginaLogin());

    $("#btnLogin").click(function () {

        if ($('#Login').val() == "" || $('#Senha').val() == "") {
            $("#H1Mensagem").text("Os Campos Login e Senha São Obrigatórios!");
            return;
        }
        else {
            var data = {
                Login: $('#Login').val(),
                Senha: $('#Senha').val()

            };
            $.ajax({
                type: "POST",
                url: "/Account/login",
                data: data,
                dataType: "json",
                success: function (data) {
                    if (data.success == true) {
                        GerenciaArmazenamentoLoginSenha();
                        window.location.href = "/Home/Index";
                    }
                    else {
                        $("#H1Mensagem").text(data.responseText);
                    }
                },
                error: function (error) {
                    $("#H1Mensagem").text("Erro ao estabelecer conexão, sua sessão expirou.por favor Se autentique novamente ");
                }
            });
        }

    });

    function GerenciaArmazenamentoLoginSenha() {
        if ($("#customCheck").is(":checked")) {
            GuardarLoginSenha();
        }
        else {
            localStorage.removeItem("ScheduleLogin");
            localStorage.removeItem("ScheduleSenha");
        }


    }

    function CarregaPaginaLogin() {
        var login = localStorage.getItem('ScheduleLogin');
        var Senha = localStorage.getItem('ScheduleSenha');
      
        if (login != null && login != '' && Senha != null && Senha != '') {
            $('#Login').val(login);
            $('#Senha').val(Senha);
            $("#customCheck").prop("checked", true);
        }
    };

    function GuardarLoginSenha() {

        localStorage.setItem('ScheduleLogin', $('#Login').val());
        localStorage.setItem('ScheduleSenha', $('#Senha').val());
    }

    $("#h1TrocarSenha").click(function () {


        $("#H1Mensagem").removeClass("text-success");
        $("#H1Mensagem").addClass("text-danger");
        $("#H1Mensagem").text("");

        if ($('#Login').val() == "") {

            $("#H1Mensagem").text("Por favor, preencha o Login!");

        }
        else {
            var data = {};
            data.Login = $('#Login').val().trim();

            $.ajax({
                type: "POST",
                url: "/Account/EnviarEmailRedefinicaoSenha",
                data: JSON.stringify(data),
                dataType: "json",
                contentType: "application/json;charset=utf-8",
                success: function (response) {

                    $("#H1Mensagem").removeClass("text-danger");
                    $("#H1Mensagem").addClass("text-success");
                    $("#H1Mensagem").text(response.responseText);
                },
                error: function (response) {

                    $("#H1Mensagem").text(response.responseText);

                }
            });
            e.preventDefault();
        }

    });

    function RedirecionaLogin() {
        setTimeout(function () {
            window.location.href = "/Account/Login";
        }, 10000);
    }

    $("#btnAlterarSenha").click(function () {
        $("#H1Mensagem").removeClass("text-success");
        $("#H1Mensagem").addClass("text-danger");
        $("#H1Mensagem").text("");


        if ($('#NovaSenha').val().trim() == "" || $('#ConfirmarNovaSenha').val().trim() == "") {
            $("#H1Mensagem").text('Os Campos Senha e Confirmação de Senha São Obrigatórios!');
            return;
        }

        if ($('#NovaSenha').val() != $('#ConfirmarNovaSenha').val()) {
           
            $("#H1Mensagem").text('Os Campos Senha e Confirmação de Senha devem ser iguais!');
            return;

        }
        else {
            var urlParams = new URLSearchParams(window.location.search);
            var data = {};
            data.NovaSenha = $('#NovaSenha').val();
            data.Token = urlParams.get('token')
            $.ajax({
                type: "PUT",
                url: "/Account/EditarSenha",
                data: JSON.stringify(data),
                dataType: "json",
                contentType: "application/json;charset=utf-8",
                success: function (response) {

                    if (response.success == true) {
                        console.log(response);
                        localStorage.removeItem("ScheduleLogin");
                        localStorage.removeItem("ScheduleSenha");

                        $("#H1Mensagem").removeClass("text-danger");
                        $("#H1Mensagem").addClass("text-success");
                        $("#H1Mensagem").text(response.responseText);

                        RedirecionaLogin();
                    }
                    else {
                        $("#H1Mensagem").text(response.responseText);
                    }
                  
                },
                error: function (response) {
                    $("#H1Mensagem").text(response.responseText);
                }
            });
           /* e.preventDefault();*/
        }


    });


});