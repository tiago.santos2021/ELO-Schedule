﻿$(document).ready(function () {


    $('#ConsultorId').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        filterPlaceholder: 'Pesquisar...',
        selectAllText: "Selecionar Todos",
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum Consultor Selecionado...',
        allSelectedText: "Todos os itens estão selecionados",
    });

    $('#ListaEspecialidades').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        selectAllText: "Selecionar Todas",
        filterPlaceholder: 'Pesquisar...',
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhuma Especialidade Selecionada...',
        allSelectedText: "Todos os itens estão selecionados",

    });

    if ($('#HiddenrefazerPesquisa').val() != "True") {
        $('#ListaEspecialidades').multiselect('selectAll', true);
    }

    $('#ListaCentroCustos').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        selectAllText: "Selecionar Todos",
        filterPlaceholder: 'Pesquisar...',
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum Centro de Custo Selecionado...',
        allSelectedText: "Todos os itens estão selecionados",
    });
    if ($('#HiddenrefazerPesquisa').val() != "True") {
        $('#ListaCentroCustos').multiselect('selectAll', true);
    }

    $('#CentroCustoExcluirId').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        selectAllText: "Selecionar Todos",
        filterPlaceholder: 'Pesquisar...',
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum Centro de Custo Selecionado...',
        allSelectedText: "Todos os itens estão selecionados",
    });

    $('#CentroCustoNovoId').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        selectAllText: "Selecionar Todos",
        filterPlaceholder: 'Pesquisar...',
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum Centro de Custo Selecionado...',
        allSelectedText: "Todos os itens estão selecionados",
    });
  
    $('#ListaConsultoresId').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        filterPlaceholder: 'Pesquisar...',
        selectAllText: "Selecionar Todos",
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum Consultor Selecionado...',
        allSelectedText: "Todos os itens estão selecionados",
    });
    

    $('#ListaConsultoresIdConflito').multiselect({
        enableFiltering: true,
        maxHeight: 300,
        buttonWidth: '100%',
        filterPlaceholder: 'Pesquisar...',
        selectAllText: "Selecionar Todos",
        enableCaseInsensitiveFiltering: true,
        includeSelectAllOption: true,
        nSelectedText: 'Selecionado',
        nonSelectedText: 'Nenhum Consultor Selecionado...',
        allSelectedText: "Todos os itens estão selecionados",
    });

    if ($('#HiddenrefazerPesquisaConflito').val() != "True") {
        $('#ListaConsultoresIdConflito').multiselect('selectAll', true);
    }
    
    $("#DataInicial").datepicker();
    $("#Datafinal").datepicker();
    $("#DataIniciotexto").datepicker();
    $("#DataFimtexto").datepicker();
    $("#DataInicio").datepicker();
    $("#DataFim").datepicker();
    $('#DataInicio').mask('00/00/0000');
    $('#DataFim').mask('00/00/0000');
    $('#DataInicial').mask('00/00/0000');
    $('#Datafinal').mask('00/00/0000');
    $('#DataIniciotexto').mask('00/00/0000');
    $('#DataFimtexto').mask('00/00/0000');

    $('[data-toggle="tooltip"]').tooltip()

    function LimparValidacao() {
        $("#spanMensagemErroConsultor").css("display", "None");
        $("#DivValidaConsultor").removeClass("divErro");

        $("#spanMensagemErroEmpresa").css("display", "None");
        $("#DivValidaEmpresa").removeClass("divErro");

        $("#spanMensagemErroCentroCusto").css("display", "None");
        $("#DivValidaCentroCusto").removeClass("divErro");



        $("#spanMensagemErroDataInicio").css("display", "None");
        $("#DivValidaDataInicio").removeClass("divErro");

        $("#spanMensagemErroataFim").css("display", "None");
        $("#DivValidaDataFinal").removeClass("divErro");

        $("#spanMensagemErroCentroCusto").css("display", "None");
        $("#DivValidaCentroCusto").removeClass("divErro");


        $("#DivValidacao").css("display", "None");
        $("#DivValidacao").text("");
    }

    function ValidaSolicitacao() {

        LimparValidacao();
        let validacao = true;

        if ($('#ConsultorId').val().length == 0) {
            $("#spanMensagemErroConsultor").css("display", "block");
            $("#DivValidaConsultor").addClass("divErro");
            validacao = false
        }



        if ($('#EmpresaId').val() == "") {
            $("#spanMensagemErroEmpresa").css("display", "block");
            $("#DivValidaEmpresa").addClass("divErro");
            validacao = false
        }

        if ($('#DataIniciotexto').val() == "") {
            $("#spanMensagemErroDataInicio").css("display", "block");
            $("#DivValidaDataInicio").addClass("divErro");
            validacao = false
        }

        if ($('#DataFimtexto').val() == "") {
            $("#spanMensagemErroataFim").css("display", "block");
            $("#DivValidaDataFinal").addClass("divErro");
            validacao = false
        }


        console.log($("#Reserva").is(":checked"));
        console.log($('#CentroCustoNovoId').val());

        if (!$("#Reserva").is(":checked") && $('#CentroCustoNovoId').val() == "") {
            if (!$("#Feriado").is(":checked")) {
                $("#spanMensagemErroCentroCusto").css("display", "block");
                $("#DivValidaCentroCusto").addClass("divErro");
                validacao = false
            }
        }

        if (!$("#Periodo1").is(":checked") && !$("#Periodo2").is(":checked")) {

            $("#DivValidacao").css("display", "block");
            $("#DivValidacao").text("Para Cadastrar a Alocação informe pelo menos um Período");
            validacao = false
        }

        return validacao;
    }

    $("#Todos").change(function () {

        if (this.checked) {
            $('.diaSemana').each(function () {
                this.checked = true;
            });
        } else {
            $('.diaSemana').each(function () {
                this.checked = false;
            });
        }
    });

    //function ValidaSolicitacaoExclusao() {
    //    LimparValidacao();
    //    let validacao = true;

    //    if ($('#ConsultorId').val().length == 0) {
    //        $("#spanMensagemErroConsultor").css("display", "block");
    //        $("#DivValidaConsultor").addClass("divErro");
    //        validacao = false
    //    }



    //    if ($('#EmpresaId').val() == "") {
    //        $("#spanMensagemErroEmpresa").css("display", "block");
    //        $("#DivValidaEmpresa").addClass("divErro");
    //        validacao = false
    //    }



    //    if ($('#DataIniciotexto').val() == "") {
    //        $("#spanMensagemErroDataInicio").css("display", "block");
    //        $("#DivValidaDataInicio").addClass("divErro");
    //        validacao = false
    //    }

    //    if ($('#DataFimtexto').val() == "") {
    //        $("#spanMensagemErroataFim").css("display", "block");
    //        $("#DivValidaDataFinal").addClass("divErro");
    //        validacao = false
    //    }



    //    if (!$("#Periodo1").is(":checked") && !$("#Periodo2").is(":checked")) {

    //        $("#DivValidacao").css("display", "block");
    //        $("#DivValidacao").text("Para Excluir á Alocação informe pelo menos um Período");
    //        validacao = false
    //    }

    //    return validacao;
    //}

    function ValidaSolicitacaoExclusao() {
        LimparValidacao();
        let validacao = true;

        if ($('#ConsultorId').val().length == 0) {
            $("#spanMensagemErroConsultor").css("display", "block");
            $("#DivValidaConsultor").addClass("divErro");
            validacao = false
        }



        if ($('#EmpresaId').val() == "") {
            $("#spanMensagemErroEmpresa").css("display", "block");
            $("#DivValidaEmpresa").addClass("divErro");
            validacao = false
        }

        if ($('#CentroCustoExcluirId').val() == "") {

            $("#spanMensagemErroCentroCusto").css("display", "block");
            $("#DivValidaCentroCusto").addClass("divErro");
            validacao = false
        }


        if ($('#DataIniciotexto').val() == "") {
            $("#spanMensagemErroDataInicio").css("display", "block");
            $("#DivValidaDataInicio").addClass("divErro");
            validacao = false
        }

        if ($('#DataFimtexto').val() == "") {
            $("#spanMensagemErroataFim").css("display", "block");
            $("#DivValidaDataFinal").addClass("divErro");
            validacao = false
        }



        if (!$("#Periodo1").is(":checked") && !$("#Periodo2").is(":checked")) {

            $("#DivValidacao").css("display", "block");
            $("#DivValidacao").text("Para excluir á alocação informe pelo menos um período");
            validacao = false
        }

        return validacao;
    }

    $('#Feriado').click(function () {
        if (this.checked) {
            $(".feriado").hide();
            $('#Segunda').prop('checked', false)
            $('#Terca').prop('checked', false)
            $('#Quarta').prop('checked', false)
            $('#Quinta').prop('checked', false)
            $('#Sexta').prop('checked', false)
            $('#Sabado').prop('checked', false)
            $('#Domingo').prop('checked', false)
            $('#Todos').prop('checked', false)

        } else {
            $(".feriado").show();
        }

    });

    $("#btnPesquisar").click(function () {
        //let FormValido = ValidaSolicitacao();
        //if (FormValido) {
        var data = {
            EmpresaId: $('#EmpresaId').val(),
            ListaConsultoresId: $('#ListaConsultoresId').val(),
            ListaCentroCustos: $('#ListaCentroCustos').val(),
            ListaEspecialidades: $('#ListaEspecialidades').val(),
            DataInicio: $('#DataInicio').val(),
            DataFim: $('#DataFim').val(),


        };
        $("#DivValidacao").css("display", "none");
        $('#tabelaAlocacao').html("");
        $("#ScheduleTable").css("display", "none");


        $("#idloading").modal('show')

        $.ajax({
            type: "POST",
            url: "/GestaoAgenda/Index",
            data: data,
            dataType: "json",
            success: function (data) {
                $("#idloading").modal('hide');
                console.log(data);
                if (data.success == true) {

                    $("#ScheduleTable").css("display", "block");
                    $('#tabelaAlocacao').html(data.data)
                    $('tbody').scroll(function (e) {
                        $('thead').css("left", -$("tbody").scrollLeft());
                        $('thead th:nth-child(1)').css("left", $("tbody").scrollLeft());
                        $('tbody td:nth-child(1)').css("left", $("tbody").scrollLeft());

                        $('thead th:nth-child(2)').css("left", $("tbody").scrollLeft());
                        $('tbody td:nth-child(2)').css("left", $("tbody").scrollLeft());

                        $('thead th:nth-child(3)').css("left", $("tbody").scrollLeft());
                        $('tbody td:nth-child(3)').css("left", $("tbody").scrollLeft());

                        $('thead th:nth-child(4)').css("left", $("tbody").scrollLeft());
                        $('tbody td:nth-child(4)').css("left", $("tbody").scrollLeft());
                    });
                }
                else {
                    if (data.mensagem != null && data.mensagem != '') {

                        $("#DivValidacao").css("display", "block");
                        $("#DivValidacao").text(data.mensagem);
                    }
                    else {
                        $("#DivValidacao").css("display", "block");
                        $("#DivValidacao").text(data.erro);
                    }
                }
            },
            error: function (error) {
                $("#idloading").modal('hide')
                $("#DivValidacao").css("display", "block");
                $("#DivValidacao").text("Erro ao estabelecer conexão,sua sessão expirou. por favor se autentique novamente ");
            }
        });

    });

    if ($('#HiddenrefazerPesquisa').val() == "True") {

        $("#btnPesquisar").click();
    }

  
    $("#btnPesquisarConflitos").click(function () {

        var data = {
            EmpresaId: $('#EmpresaId').val(),
            ListaConsultoresId: $('#ListaConsultoresIdConflito').val(),
            ListaCentroCustos: $('#ListaCentroCustos').val(),
            ListaEspecialidades: $('#ListaEspecialidades').val(),
            DataInicio: $('#DataInicio').val(),
            DataFim: $('#DataFim').val(),


        };

        $("#DivValidacao").css("display", "none");
        $('#tbListaConsultoresConflito').html("");
        $("#DivTableConsultoresConflito").css("display", "none");

        $.ajax({
            type: "POST",
            url: "/GestaoAgenda/ListaConflitos",
            data: data,
            dataType: "json",
            success: function (data) {

                console.log(data);
                if (data.success == true) {
                    $('#DataPesquisaInicial').val(data.data.dataPesquisaInicial);
                    $('#DataPesquisaFinal').val(data.data.dataPesquisaFinal)

                    $("#DivTableConsultoresConflito").css("display", "block");
                    $('#tbListaConsultoresConflito').html(data.data.html);
                    $('#tbListaConsultoresConflito').DataTable({
                        "language": {
                            "url": "https://cdn.datatables.net/plug-ins/1.11.3/i18n/pt_br.json"

                        },
                        "bDestroy": true
                    });
                }
                else {
                    if (data.mensagem != null && data.mensagem != '') {

                        $("#DivValidacao").css("display", "block");
                        $("#DivValidacao").text(data.mensagem);
                    }
                    else {
                        $("#DivValidacao").css("display", "block");
                        $("#DivValidacao").text(data.erro);
                    }


                }

            },
            error: function (error) {
                console.log(error);
                $("#idloading").modal('hide')
                $("#DivValidacao").css("display", "block");
                $("#DivValidacao").text("Erro ao estabelecer conexão,sua sessão expirou. por favor se autentique novamente ");
            }
        });

    });
    if ($('#HiddenrefazerPesquisaConflito').val() == "True") {

        $("#btnPesquisarConflitos").click();
    }


    $("#btnPesquisarAlocacao").click(function () {
        let FormValido = true;
        if (FormValido) {
            var data = {
                ListaConsultoresId: $('#ConsultorId').val(),
                ConsultorId: $('#ConsultorId').val(),
                EmpresaId: $('#EmpresaId').val(),
                CentroCustoId: $('#CentroCustoId').val(),
                DataIniciotexto: $('#DataInicial').val(),
                DataFimtexto: $('#Datafinal').val(),
                Periodo1: $("#Periodo1").is(":checked") == true ? true : false,
                Periodo2: $("#Periodo2").is(":checked") == true ? true : false,
                FimDeSemana: $("#FimDeSemana").is(":checked") == true ? true : false,
                Reserva: $("#Reserva").is(":checked") == true ? true : false,
                Feriado: $("#Feriado").is(":checked") == true ? true : false,
            };

            $("#idloading").modal('show')

            $.ajax({
                type: "POST",
                url: "/GestaoAgenda/Novo",
                data: data,
                dataType: "json",
                success: function (data) {
                    alert('Sucesso');
                    $("#idloading").modal('hide')
                    console.log(data);
                    if (data.erro != null && data.erro != '') {
                        $("#DivValidacao").css("display", "block");
                        $("#DivValidacao").text(data.erro);
                    }
                    else {
                        $("#success").val("alocação cadastarda com sucesso");
                        window.location.href = "/GestaoAgenda/Index?mensagem=Sucesso"
                    }
                },
                error: function (error) {
                    alert(error);
                    $("#idloading").modal('hide')
                    $("#DivValidacao").css("display", "block");
                    $("#DivValidacao").text("Erro ao estabelecer conexão,sua sessão expirou. por favor se autentique novamente ");
                }
            });
        }
    });

    $("#btnSalvarAlocacao").click(function () {
        if ($('#Feriado').is(':checked')) {
            $('#DataFimtexto').val($('#DataIniciotexto').val());
        }

        let FormValido = ValidaSolicitacao();
        if (FormValido) {
            var data = {
                ListaConsultoresId: $('#ConsultorId').val(),
                ConsultorId: $('#ConsultorId').val(),
                EmpresaId: $('#EmpresaId').val(),
                ListaCentroCustos: $('#CentroCustoNovoId').val(),
                DataIniciotexto: $('#DataIniciotexto').val(),
                DataFimtexto: $('#DataFimtexto').val(),
                Periodo1: $("#Periodo1").is(":checked") == true ? true : false,
                Periodo2: $("#Periodo2").is(":checked") == true ? true : false,
                Segunda: $("#Segunda").is(":checked") == true ? true : false,
                Terca: $("#Terca").is(":checked") == true ? true : false,
                Quarta: $("#Quarta").is(":checked") == true ? true : false,
                Quinta: $("#Quinta").is(":checked") == true ? true : false,
                Sexta: $("#Sexta").is(":checked") == true ? true : false,
                Sabado: $("#Sabado").is(":checked") == true ? true : false,
                Domingo: $("#Domingo").is(":checked") == true ? true : false,
                Reserva: $("#Reserva").is(":checked") == true ? true : false,
                Feriado: $("#Feriado").is(":checked") == true ? true : false,
            };

            $("#idloading").modal('show')

            $.ajax({
                type: "POST",
                url: "/GestaoAgenda/Novo",
                data: data,
                dataType: "json",
                success: function (data) {
                    $("#idloading").modal('hide')
                    console.log(data);
                    if (data.erro != null && data.erro != '') {
                        $("#DivValidacao").css("display", "block");
                        $("#DivValidacao").text(data.erro);
                    }
                    else {
                        $("#success").val("alocação cadastarda com sucesso");
                        window.location.href = "/GestaoAgenda/Index?mensagem=Sucesso"
                    }
                },
                error: function (error) {
                    $("#idloading").modal('hide')
                    $("#DivValidacao").css("display", "block");
                    $("#DivValidacao").text("Erro ao estabelecer conexão,sua sessão expirou. por favor Se autentique novamente ");
                }
            });
        }
    });

    $("#btnExcluirAlocacao").click(function () {
        debugger;
        let FormValido = ValidaSolicitacaoExclusao();
        if (FormValido) {
            var data = {
                ListaConsultoresId: $('#ConsultorId').val(),
                ConsultorId: $('#ConsultorId').val(),
                EmpresaId: $('#EmpresaId').val(),
                //CentroCustoId: $('#CentroCustoExcluirId').val(),
                ListaCentroCustos: $('#CentroCustoExcluirId').val(),
                DataIniciotexto: $('#DataIniciotexto').val(),
                DataFimtexto: $('#DataFimtexto').val(),
                Periodo1: $("#Periodo1").is(":checked") == true ? true : false,
                Periodo2: $("#Periodo2").is(":checked") == true ? true : false,
                FimDeSemana: $("#FimDeSemana").is(":checked") == true ? true : false,
                Reserva: $("#Reserva").is(":checked") == true ? true : false,
            };

            $("#idloading").modal('show')

            $.ajax({
                type: "POST",
                url: "/GestaoAgenda/Excluir",
                data: data,
                dataType: "json",
                success: function (data) {
                    $("#idloading").modal('hide')
                    console.log(data);
                    if (data.erro != null && data.erro != '') {
                        $("#DivValidacao").css("display", "block");
                        $("#DivValidacao").text(data.erro);
                    }
                    else {
                        $("#success").val("alocação cadastarda com sucesso");
                        window.location.href = "/GestaoAgenda/Index?mensagem=Sucesso"
                    }
                },
                error: function (error) {
                    $("#idloading").modal('hide')
                    $("#DivValidacao").css("display", "block");
                    $("#DivValidacao").text("Erro ao estabelecer conexão,sua sessão expirou. por favor Se autentique novamente ");
                }
            });
        }
    });

    $("#SelecionarTodos").click(function () {
        if ($(this).is(':checked')) {
            $('input:checkbox').prop("checked", true);
        } else {
            $('input:checkbox').prop("checked", false);
        }
    });

    $("#tbListaConsultoresConflito").delegate(".ResolverConflito", "click", function (e) {
        window.location.href = "/GestaoAgenda/ResolverConflito?id=" + $(this).data('id') + "&DataInicial=" + $('#DataPesquisaInicial').val() + "&DataFinal=" + $('#DataPesquisaFinal').val()
    });

    $("#btnEliminarConflitos").click(function () {

        var listaExclusao = [];
        $('#TabelaResolverConflitos tr').each(function (index, tr) {
            if (index > 1) {
                if ($(this).find("input").is(':checked')) {
                    listaExclusao.push({ "id": $(this).find(".AgendaId").html() })
                }
            }
        });

        var dto = { ListaExclusao: listaExclusao }


        $("#DivValidacao").css("display", "none");
        $("#DivValidacao").text("");



        if (listaExclusao.length > 0) {
            $("#idloading").modal('show')

            $.ajax({
                type: "POST",
                url: "/GestaoAgenda/ResolverConflito",
                data: dto,
                dataType: "json",
                success: function (data) {
                    $("#idloading").modal('hide');
                    if (data.erro != null && data.erro != '') {
                        $("#DivValidacao").css("display", "block");
                        $("#DivValidacao").text(data.erro);
                    }
                    else {
                        window.location.href = "/GestaoAgenda/ResolverConflito?id=" + $('#UsuarioId').val() + "&DataInicial=" + $('#dataInicial').val() + "&DataFinal=" + $('#dataFinal').val() + "&mensagem=Sucesso"
                      
                    }
                },
                error: function (error) {
                    $("#idloading").modal('hide');
                    $("#DivValidacao").css("display", "block");
                    $("#DivValidacao").text("Erro ao estabelecer conexão,sua sessão expirou. por favor Se autentique novamente ");
                }
            });
        }
        else {
            $("#DivValidacao").css("display", "block");
            $("#DivValidacao").text("Selecione pelo menos uma agenda");
        }




        console.log(listaExclusao)

    });


});




