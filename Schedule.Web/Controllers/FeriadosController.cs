﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Web.Autorizacao;
using Schedule.Web.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Schedule.Web.Controllers
{
    [Authorize]
    public class FeriadosController : BaseController
    {
        private const string Recurso = "Feriados";
        private readonly IFeriadosService _feriadosService;
        private readonly IMapper _mapper;
        public FeriadosController(IFeriadosService feriadosService, IMapper mapper)
        {
            _feriadosService = feriadosService;
            _mapper = mapper;
        }
        [ClaimRequirement(Recurso, "Listar")]
        public IActionResult Index()
        {
            var lista = _feriadosService.Listar().OrderBy(x => x.DataFeriado).ToList(); ;
            lista = BindOrdenacaoData<FeriadosViewModel>(lista);
            return View(lista);
        }
        
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo()
        {
            return View();
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult SalvarNovo(FeriadosViewModel entidade)
        {
           //if (ModelState.IsValid)
            {
                try
                {
                    //entidade.DataFeriado
                    entidade.Usuario = this.UsuarioLogado;
                    entidade.DataAlteracao = DateTime.Now;
                    _feriadosService.Adicionar(entidade);
                    TempData["success"] = ScheduleResorce.RetornaValor("MensagemSucesso");
                    return RedirectToAction("Index");
                }
                catch (DomainException ex)
                {

                    ModelState.AddModelError("", ex.Message);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;

                }
            }

            return View();
        }
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Editar(Guid id)
        {
            if (!_feriadosService.VerificaSeRegistroExisteNaBase(id))
            {
                TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                return RedirectToAction("Index");
            }
            var entidade = _feriadosService.RetornaPorId(id);
            return View(entidade);
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Editar(FeriadosViewModel entidade)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (ModelState.IsValid)
                    {
                        if (!_feriadosService.VerificaSeRegistroExisteNaBase((Guid)entidade.id))
                        {
                            TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                        }
                        else
                        {
                            entidade.Usuario = this.UsuarioLogado;
                            _feriadosService.Alterar(entidade);
                            TempData["success"] = "Operação Realizada Com Sucesso!";
                        }
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (DomainException ex)
            {

                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;

            }
            return View(entidade);
        }
        [ClaimRequirement(Recurso, "Excluir")]
        public IActionResult Excluir(Guid id)
        {
            try
            {
                if (!_feriadosService.VerificaSeRegistroExisteNaBase(id))
                {
                    TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                }
                else
                {
                    _feriadosService.Excluir(id);
                    TempData["success"] = "Operação Realizada Com Sucesso!";
                }
            }
            catch (DomainException ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
            }
            return RedirectToAction("Index");
        }


    }

}
