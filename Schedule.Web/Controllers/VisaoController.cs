﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Interfaces.Services.Relatorio;
using Schedule.Application.ViewModel;
using Schedule.Application.ViewModel.Relatorio;
using Schedule.Application.ViewModel.Visao;
using Schedule.Web.Autorizacao;
using Schedule.Web.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Schedule.Web.Controllers
{
    [Authorize]
    public class VisaoController : BaseController
    {
        private readonly IUsuarioService _usuarioService;

        private readonly IEspecialidadeService _especialidadeService;
        private readonly IEmpresaService _empresaService;
        private readonly ICentroCustoService _centroCustoService;
        private readonly IRelatorioService _relatorioService;

        private const string Recurso = "Relatorio";
        public VisaoController(IRelatorioService RelatorioService, ICentroCustoService CentroCustoService, IUsuarioService usuarioService, IEspecialidadeService especialidadeService,
            IEmpresaService empresaService, IConfiguration Configuration)
        {
            this._usuarioService = usuarioService;
            this._configuration = Configuration;
            this._especialidadeService = especialidadeService;
            this._empresaService = empresaService;
            this._relatorioService = RelatorioService;
            this._centroCustoService = CentroCustoService;
        }
        [ClaimRequirement(Recurso, "Listar")]
        public IActionResult Index()
        {

            return View();
        }
        [ClaimRequirement(Recurso, "Planejado")]
        public IActionResult Planejado()
        {
            var TipoConsultorId = Guid.Parse(this.RetornaParametro("TipoConsultorId"));
            var tipoGestaoComAlocacaoId = Guid.Parse(this.RetornaParametro("tipoGestaoComAlocacao"));
            var ListaConsultores = _usuarioService.ListarConsultores(new List<Guid>() { TipoConsultorId, tipoGestaoComAlocacaoId });
            ViewBag.ListaConsultores = ListaConsultores;

            var ListaEspecialidades = _especialidadeService.Listar().OrderBy(e => e.DescricaoEspecialidade).ToList();
            ViewBag.ListaEspecialidades = ListaEspecialidades;

            var ListaCentroCusto = _centroCustoService.Listar().OrderBy(c => c.Nome).ToList();
            ViewBag.ListaCentroCustos = ListaCentroCusto;


            var ListaEmpresa = _empresaService.Listar().ToList();
            ViewBag.ListaEmpresa = ListaEmpresa;

            

            return View("VisaoPlanejado");
        }
        [ClaimRequirement(Recurso, "Planejado")]
        [HttpPost]
        public JsonResult Planejado(SolicitacaoImpressaoRelPlanejadoViewModel form)
        {
            RespostaSolicitacaoImpressaoPlanejadoViewModel resposta = new RespostaSolicitacaoImpressaoPlanejadoViewModel();
            try
            {
                var ValorPeriodo = _configuration.GetSection("parametros")["ValorPeriodo"];
                form.ValorPeriodo = Convert.ToInt32(ValorPeriodo);
                resposta = _relatorioService.RetornaRelatorioPlanejado(form);

                return Json(resposta);
            }
            catch (Exception ex)
            {
                resposta.Erro = ex.Message;
            }

            return Json(resposta);
        }
        [ClaimRequirement(Recurso, "Coloborador")]
        public IActionResult Coloborador()
        {
            return View("VisaoColaborador");
        }
        [ClaimRequirement(Recurso, "PlanejadoRealizado")]
        public IActionResult PlanejadoRealizado()
        {
            return View("VisaoPlanejadoRealizado");
        }

        //[ClaimRequirement(Recurso, "Disponivel")]
        public IActionResult Disponivel()
        {
            ViewBag.SelecionaConsultor = false;
            Planejado();
            return View("VisaoDisponibilidade");
        }

        //[ClaimRequirement(Recurso, "Templates")]
        public IActionResult Templates()
        {
            return View("Templates");
        }
    }
}
