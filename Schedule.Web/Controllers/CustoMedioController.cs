﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.Services;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Web.Autorizacao;
using Schedule.Web.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Schedule.Web.Controllers
{
    [Authorize]
    public class CustoMedioController : BaseController
    {
        private const string Recurso = "Cliente";
        private readonly ICustoMedioService _custoMedioService;
        private readonly IEspecialidadeService _especialidadeservice;
        private readonly IMapper _mapper;

        public CustoMedioController(ICustoMedioService custoMedioService, IEspecialidadeService Especialidadeservice, IMapper mapper)

        {
            _custoMedioService = custoMedioService;
            _especialidadeservice = Especialidadeservice;
            _mapper = mapper;
        }
        private void CarregaEspecialdiades()
        {
            var especialdiades = _especialidadeservice.Listar().OrderBy(x => x.UnidadeServico).ToList(); ;
            ViewBag.Especialidades = new SelectList(especialdiades, "id", "DescricaoEspecialidade", "Selecione");
        }

        [ClaimRequirement(Recurso, "Listar")]
        public IActionResult Index()
        {
            var lista = _custoMedioService.Listar().OrderBy(x => x.DataAlteracao).ToList();

            lista = BindOrdenacaoData<CustoMedioViewModel>(lista);
            return View(lista);
        }
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo()
        {
            this.CarregaEspecialdiades();
            return View();
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo(CustoMedioViewModel entidade)
        {
            ViewBag.ValidaEspecialidade = "none";
            ViewBag.DivValidaEspecialidade = "none";
            if (ModelState.IsValid)
            {
                try
                {
                    entidade.Usuario = this.UsuarioLogado;
                    _custoMedioService.Adicionar(entidade);
                    TempData["success"] = ScheduleResorce.RetornaValor("MensagemSucesso");
                    return RedirectToAction("Index");
                }
                catch (DomainException ex)
                {
                    if (ex.Message.Contains("Especialidade é Obrigatório"))
                    {
                        ViewBag.DivValidaEspecialidade = "solid 1px red";
                        ViewBag.ValidaEspecialidade = "block";
                    }
                    ModelState.AddModelError("", ex.Message);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;

                }
            }
            this.CarregaEspecialdiades();
            return View(entidade);
        }
        [ClaimRequirement(Recurso, "Editar")]
        public IActionResult Editar(Guid id)
        {
            if (!_custoMedioService.VerificaSeRegistroExisteNaBase(id))
            {
                TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                return RedirectToAction("Index");
            }

            var entidade = _custoMedioService.RetornaPorId(id);
            this.CarregaEspecialdiades();
            return View(entidade);
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Editar")]
        public IActionResult Editar(CustoMedioViewModel entidade)
        {
            ViewBag.DivValidaEspecialidade = "none";
            ViewBag.ValidaEspecialidade = "none";
            try
            {
                if (ModelState.IsValid)
                {
                    if (!_custoMedioService.VerificaSeRegistroExisteNaBase((Guid)entidade.id))
                    {
                        TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                    }
                    else
                    {
                        entidade.Usuario = this.UsuarioLogado;
                        _custoMedioService.Alterar(entidade);
                        TempData["success"] = "Operação Realizada Com Sucesso!";
                    }
                    return RedirectToAction("Index");
                }
            }
            catch (DomainException ex)
            {

                if (ex.Message.Contains("Especialidade é Obrigatório"))
                {
                    ViewBag.DivValidaEspecialidade = "solid 1px red";
                    ViewBag.ValidaEspecialidade = "block";
                }

                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;

            }
            this.CarregaEspecialdiades();
            return View(entidade);
        }
        [ClaimRequirement(Recurso, "Excluir")]
        public IActionResult Excluir(Guid id)
        {
            try
            {
                if (!_custoMedioService.VerificaSeRegistroExisteNaBase(id))
                {
                    TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                }
                else
                {
                    _custoMedioService.Excluir(id);
                    TempData["success"] = "Operação Realizada Com Sucesso!";
                }
            }
            catch (DomainException ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
            }
            return RedirectToAction("Index");
        }
    }
}
