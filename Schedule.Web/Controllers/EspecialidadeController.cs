﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Web.Autorizacao;
using Schedule.Web.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Schedule.Web.Controllers
{
    [Authorize]
    public class EspecialidadeController : BaseController
    {
        private const string Recurso = "Especialidade";
        private readonly IEspecialidadeService _especialidadeService;
        private readonly IMapper _mapper;
        public EspecialidadeController(IEspecialidadeService especialidadeService, IMapper mapper)
        {
            _especialidadeService = especialidadeService;
            _mapper = mapper;
        }
        [ClaimRequirement(Recurso, "Listar")]
        public IActionResult Index()
        {
            var lista = _especialidadeService.Listar().OrderBy(x => x.DataAlteracao).ToList(); ;
            lista = BindOrdenacaoData<EspecialidadeViewModel>(lista);
            return View(lista);
        }
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo()
        {
            return View();
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo(EspecialidadeViewModel entidade)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    entidade.Usuario = this.UsuarioLogado;
                    _especialidadeService.Adicionar(entidade);
                    TempData["success"] = ScheduleResorce.RetornaValor("MensagemSucesso");
                    return RedirectToAction("Index");
                }
                catch (DomainException ex)
                {

                    ModelState.AddModelError("", ex.Message);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;

                }
            }

            return View();
        }
        [ClaimRequirement(Recurso, "Editar")]
        public IActionResult Editar(Guid id)
        {
            if (!_especialidadeService.VerificaSeRegistroExisteNaBase(id))
            {
                TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                return RedirectToAction("Index");
            }
            var entidade = _especialidadeService.RetornaPorId(id);
            return View(entidade);
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Editar")]
        public IActionResult Editar(EspecialidadeViewModel entidade)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (ModelState.IsValid)
                    {
                        if (!_especialidadeService.VerificaSeRegistroExisteNaBase((Guid)entidade.id))
                        {
                            TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                        }
                        else
                        {
                            entidade.Usuario = this.UsuarioLogado;
                            _especialidadeService.Alterar(entidade);
                            TempData["success"] = "Operação Realizada Com Sucesso!";
                        }
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (DomainException ex)
            {

                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;

            }
            return View(entidade);
        }
        [ClaimRequirement(Recurso, "Excluir")]
        public IActionResult Excluir(Guid id)
        {
            try
            {
                if (!_especialidadeService.VerificaSeRegistroExisteNaBase(id))
                {
                    TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                }
                else
                {
                    _especialidadeService.Excluir(id);
                    TempData["success"] = "Operação Realizada Com Sucesso!";
                }
            }
            catch (DomainException ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
            }
            return RedirectToAction("Index");
        }


    }

}
