﻿
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Interfaces.Services;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Web.Autorizacao;
using Schedule.Web.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Schedule.Web.Controllers
{
    [Authorize]
    public class RelatorioDisponibilidadeController : Controller
    {
        private const string Recurso = "RelatorioDisponbilidade";
        private readonly IRelatorioDisponibilidadeService _relatorioDisponibilidadeService;
        public RelatorioDisponibilidadeController( IRelatorioDisponibilidadeService relatorioDisponibilidadeService)
        {
            _relatorioDisponibilidadeService = relatorioDisponibilidadeService;
        }
        public JsonResult DadosRelatorio(DateTime dataInicial, DateTime dataFinal, string []idConsultores = null, string [] idEspecialidades = null, List<Guid> idEmpresas = null)
        {
            

return new JsonResult(_relatorioDisponibilidadeService.RelatorioDisponibilidade(dataInicial, dataFinal, idConsultores, idEspecialidades, idEmpresas));
        }
        public JsonResult ConsultarDiasPeriodo(DateTime dataInicial, DateTime dataFinal)
        {
            return new JsonResult(_relatorioDisponibilidadeService.ConsultarDiasPeriodo(dataInicial, dataFinal));
        }

    }
}


