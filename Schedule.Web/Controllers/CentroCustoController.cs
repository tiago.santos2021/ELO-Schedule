﻿using AutoMapper;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Web.Autorizacao;
using Schedule.Web.Controllers.Base;
using System;
using System.Linq;

namespace Schedule.Web.Controllers
{
    [Authorize]
    public class CentroCustoController : BaseController
    {
        private const string Recurso = "CentroCusto";
        private readonly ICentroCustoService _centroCustoService;
        private readonly IEmpresaService _empresaService;
        private readonly IClienteService _clienteService;
        private readonly ITipoCentroCustoService _tipoCentroCustoService;
        private readonly IUsuarioService _usuarioService;
        private readonly IMapper _mapper;
       
        public CentroCustoController(
            ICentroCustoService centroCustoService, IEmpresaService empresaService,
            IClienteService clienteService, ITipoCentroCustoService tipoCentroCustoService,
            IUsuarioService usuarioService, IConfiguration Configuration, IMapper mapper)
        {
            _centroCustoService = centroCustoService;
            _empresaService = empresaService;
            _clienteService = clienteService;
            _tipoCentroCustoService = tipoCentroCustoService;
            _usuarioService = usuarioService;
            _mapper = mapper;
            _configuration = Configuration;
        }

        [ClaimRequirement(Recurso, "Listar")]
        public IActionResult Index()
        {
            var lista = _centroCustoService.Listar().OrderBy(x => x.DataAlteracao).ToList();

            lista = BindOrdenacaoData<CentroCustoViewModel>(lista);
            return View(lista);
        }
        private void CarregaControles()
        {
            var empresas = _empresaService.Listar();
            ViewBag.Empresas = new SelectList(empresas, "id", "Nome", "Selecione");
            var clientes = _clienteService.Listar().OrderBy(c => c.Nome);
            ViewBag.Clientes = new SelectList(clientes, "id", "Nome", "Selecione");
            var TiposCentroCusto = _tipoCentroCustoService.Listar();
            ViewBag.TiposCentroCusto = new SelectList(TiposCentroCusto, "id", "CodigoDescricao", "Selecione");
            var Responsaveis = _usuarioService.Listar().Where(r => r.PerfilId != Guid.Parse(this.RetornaParametro("TipoConsultorId"))).OrderBy(x=> x.Nome).ToList();
            Responsaveis.Insert(0, new UsuarioViewModel() { Nome = "Selecione" });
            ViewBag.Responsaveis = Responsaveis;
        }
        private void CarregaControles(CentroCustoViewModel entidade)
        {
            var empresas = _empresaService.Listar();
            ViewBag.Empresas = new SelectList(empresas, "id", "Nome", entidade.EmpresaId);
            var clientes = _clienteService.Listar().OrderBy(c => c.Nome);
            ViewBag.Clientes = new SelectList(clientes, "id", "Nome", entidade.ClienteId);
            var TiposCentroCusto = _tipoCentroCustoService.Listar();
            ViewBag.TiposCentroCusto = new SelectList(TiposCentroCusto, "id", "CodigoDescricao", entidade.TipoCentroCustoId);
            var Responsaveis = _usuarioService.Listar().Where(r => r.PerfilId != Guid.Parse(this.RetornaParametro("TipoConsultorId"))).OrderBy(x => x.Nome).ToList();
            Responsaveis.Insert(0, new UsuarioViewModel() { Nome = "Selecione" });
            ViewBag.Responsaveis = Responsaveis;
        }
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo()
        {
            this.CarregaControles();
            return View();
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo(CentroCustoViewModel entidade)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    entidade.Usuario = this.UsuarioLogado;
                    _centroCustoService.Adicionar(entidade);
                    TempData["success"] = ScheduleResorce.RetornaValor("MensagemSucesso");
                    return RedirectToAction("Index");
                }
                catch (DomainException ex)
                {

                    ModelState.AddModelError("", ex.Message);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;

                }
            }
            

            this.CarregaControles();
            return View(entidade);
        }
        [ClaimRequirement(Recurso, "Editar")]
        public IActionResult Editar(Guid id)
        {
            if (!_centroCustoService.VerificaSeRegistroExisteNaBase(id))
            {
                TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                return RedirectToAction("Index");
            }


            var entidade = _centroCustoService.RetornaPorId(id);
            this.CarregaControles(entidade);
            return View(entidade);
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Editar")]
        public IActionResult Editar(CentroCustoViewModel entidade)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!_centroCustoService.VerificaSeRegistroExisteNaBase((Guid)entidade.id))
                    {
                        TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                    }
                    else
                    {                        
                        entidade.Usuario = this.UsuarioLogado;
                        _centroCustoService.Alterar(entidade);
                        TempData["success"] = ScheduleResorce.RetornaValor("MensagemSucesso");                      
                    }
                    return RedirectToAction("Index");
                }

            }
            catch (DomainException ex)
            {

                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;

            }
            if (entidade.ResponsavelId == Guid.Empty)
            {
                ModelState.AddModelError("", "O Campo responsável é Obrigatório");
            }
            this.CarregaControles(entidade);
            return View(entidade);
        }
        [ClaimRequirement(Recurso, "Excluir")]
        public IActionResult Excluir(Guid id)
        {
            try
            {
                if (!_centroCustoService.VerificaSeRegistroExisteNaBase(id))
                {
                    TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                }
                else
                {
                    _centroCustoService.Desativar(id,this.UsuarioLogado);
                    TempData["success"] = "Operação Realizada Com Sucesso!";
                }

            }
            catch (DomainException ex)
            {

                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;

            }

            return RedirectToAction("Index");

        }
    }
}
