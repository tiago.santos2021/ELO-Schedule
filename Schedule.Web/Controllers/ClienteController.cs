﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Web.Autorizacao;
using Schedule.Web.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Schedule.Web.Controllers
{
    [Authorize]
    public class ClienteController : BaseController
    {
        private const string Recurso = "Cliente";
        private readonly IClienteService _clienteService;
        private readonly IMapper _mapper;
        public ClienteController(IClienteService clienteService, IMapper mapper)

        {
            _clienteService = clienteService;
            _mapper = mapper;
        }

        [ClaimRequirement(Recurso, "Listar")]
        public IActionResult Index()
        {
            var lista = _clienteService.Listar().OrderBy(x => x.DataAlteracao).ToList(); ;

            lista = BindOrdenacaoData<ClienteViewModel>(lista);
            return View(lista);
        }
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo()
        {
            return View();
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo(ClienteViewModel entidade)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    entidade.Usuario = this.UsuarioLogado;
                    _clienteService.Adicionar(entidade);
                    TempData["success"] = ScheduleResorce.RetornaValor("MensagemSucesso");
                    return RedirectToAction("Index");
                }
                catch (DomainException ex)
                {

                    ModelState.AddModelError("", ex.Message);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;

                }
            }

            return View();
        }
        [ClaimRequirement(Recurso, "Editar")]
        public IActionResult Editar(Guid id)
        {
            if (!_clienteService.VerificaSeRegistroExisteNaBase(id))
            {
                TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                return RedirectToAction("Index");
            }

            var entidade = _clienteService.RetornaPorId(id);
            return View(entidade);
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Editar")]
        public IActionResult Editar(ClienteViewModel entidade)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!_clienteService.VerificaSeRegistroExisteNaBase((Guid)entidade.id))
                    {
                        TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                    }
                    else
                    {
                        entidade.Usuario = this.UsuarioLogado;
                        _clienteService.Alterar(entidade);
                        TempData["success"] = "Operação Realizada Com Sucesso!";
                    }

                    return RedirectToAction("Index");
                }
            }
            catch (DomainException ex)
            {

                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;

            }
            return View(entidade);
        }
        [ClaimRequirement(Recurso, "Excluir")]
        public IActionResult Excluir(Guid id)
        {
            try
            {
                if (!_clienteService.VerificaSeRegistroExisteNaBase(id))
                {
                    TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                }
                else
                {
                    _clienteService.Excluir(id);
                    TempData["success"] = "Operação Realizada Com Sucesso!";
                }
            }
            catch (DomainException ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
            }
            return RedirectToAction("Index");
        }
    }

}
