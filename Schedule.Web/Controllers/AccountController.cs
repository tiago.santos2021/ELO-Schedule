﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Interfaces.Services.Estrutura;
using Schedule.Application.ViewModel.Auth;
using Schedule.Util.Criptografia;
using System;
using System.Collections.Generic;
using System.Security.Claims;


namespace Schedule.Web.Controllers
{
    public class AccountController : Controller
    {

        private readonly IAccountService _serviceaccount;
        private readonly IMenuService _serviceMenu;
        private readonly IPermissaoService _permissaoService;

        private IConfiguration _configuration;
        public AccountController(IAccountService accountService, IMenuService service, IConfiguration configuration, IPermissaoService permissaoService)
        {
            _serviceaccount = accountService;
            _serviceMenu = service;
            _configuration = configuration;
            _permissaoService = permissaoService;
        }
        public IActionResult Login()
        {
            TempData["retornoLogin"] = null;
            return View();
        }
        public IActionResult RedefinirSenha()
        {
            TempData["retornoLogin"] = null;
            return View();
        }
        public IActionResult Logout()
        {


            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Account");
        }
        private List<Claim> RerornaListaPermissoesPorPerfil(Guid perfilId)
        {
            List<Claim> permissoes = new List<Claim>();
            var listaPermissoes = _permissaoService.RetornaListaDePermissoesPorPerfil(perfilId);
            foreach (var item in listaPermissoes)
            {
                permissoes.Add(new Claim(item.Tipo, item.Descricao));
            }

            return permissoes;
        }
        [HttpPost]
        public IActionResult Login(AuthViewModel model)
        {
            if (ModelState.IsValid)
            {
                var Usuario = _serviceaccount.GetUsuario(model);
                if (Usuario != null && Usuario.Ativo == true)
                {
                    Claim claim = new Claim(ClaimTypes.Name, Usuario.Nome);
                    Claim claimPerfil = new Claim(ClaimTypes.Role, Usuario.PerfilId.ToString());
                    Claim claimIdentificador = new Claim(ClaimTypes.Sid, Usuario.id.ToString());

                    ClaimsIdentity identity = new ClaimsIdentity(new[] { claim, claimPerfil, claimIdentificador }, CookieAuthenticationDefaults.AuthenticationScheme);
                    identity.AddClaims(RerornaListaPermissoesPorPerfil(Usuario.PerfilId));
                    ClaimsPrincipal principal = new ClaimsPrincipal(identity);
                    var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                    var listaMenus = _serviceMenu.RetornaListaDeMenusPorPerfil(Usuario.PerfilId);
                    HttpContext.Session.SetString("Menu", JsonConvert.SerializeObject(listaMenus));

                    HttpContext.Session.SetString("Menu", JsonConvert.SerializeObject(listaMenus));
                    var teste = !string.IsNullOrEmpty(  _configuration.GetSection("parametros")["AmbienteHomologacao"]) ? "1" : "0";
                    HttpContext.Session.SetString("AmbienteHomologacao", teste);



                    return Json(new { success = true, responseText = "" });
                }
                else if (Usuario != null && Usuario.Ativo == false)
                {
                    return Json(new { success = false, responseText = "Usuário Inativo" });
                }
                else
                {
                    return Json(new { success = false, responseText = "Usuário e/ou senha inválido(s)" });
                }

            }
            else
            {
                return Json(new { success = false, responseText = "Os Campos Login e Senha São Obrigatórios" });
            }

        }
        [HttpPost]
        public IActionResult EnviarEmailRedefinicaoSenha([FromBody] AuthViewModel data)
        {


            var retorno = _serviceaccount.EnviarEmailRedefinicaoSenha(data.Login, _configuration.GetSection("Keys")["UrlRedefinicaoSenha"]);
            return Json(new { success = retorno.Item1, responseText = retorno.Item2 });
        }
        [HttpPut]
        public IActionResult EditarSenha([FromBody] RedefinicaoSenhaViewModel data)
        {

            var senha = Criptografia.RetornarMD5(data.NovaSenha);
            var retorno = _serviceaccount.EditarSenha(data.Token, senha);
            return Json(new { success = retorno.Item1, responseText = retorno.Item2 });
        }

    }
}
