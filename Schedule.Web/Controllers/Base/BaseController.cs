﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Schedule.Application.ViewModel;
using Schedule.Application.ViewModel.Estrutura;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Schedule.Web.Controllers.Base
{
    public class BaseController : Controller
    {

        protected IConfiguration _configuration;
        public string RetornaParametro(string valor)
        {
            return Convert.ToString(_configuration.GetSection("parametros")[valor]);
        }
        public List<t> BindOrdenacaoData<t>(List<t> lista)
        {
            int ordem = 1;
            foreach (var item in lista)
            {
                item.GetType().GetProperty("OrdemData").SetValue(item, ordem, null);
                ordem++;
            }
            return lista;
        }
        public string UsuarioLogado { get { return User.Identity.Name; } }
        public string PerfilUsuarioLogado { get { return RetornaValorPerfil(); } }
        public string IdentificadorUsuarioLogado { get { return RetornaValorPerfilUsuarioLogador(); } }
        private string RetornaValorPerfil()
        {
            var name = User.Claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).SingleOrDefault();
            return name;
        }
        private string RetornaValorPerfilUsuarioLogador()
        {
            var name = User.Claims.Where(c => c.Type == ClaimTypes.Sid).Select(c => c.Value).SingleOrDefault();
            return name;
        }
        public override void OnActionExecuting(ActionExecutingContext context)
        {

            var routingValues = context.RouteData.Values;
            var ControllerAtual = (string)routingValues["controller"] ?? string.Empty;
            var ActionAtual = (string)routingValues["action"] ?? string.Empty;
            GerenciaSesso(ControllerAtual, ActionAtual);

            if (!string.IsNullOrWhiteSpace(HttpContext.Session.GetString("Menu")))
            {
                var listaMenus = JsonConvert.DeserializeObject<List<MenuViewModel>>(HttpContext.Session.GetString("Menu"));

                ViewBag.Menus = listaMenus;

                ViewBag.AmbienteHomologacao = HttpContext.Session.GetString("AmbienteHomologacao") == "1" ? true : false;
            }
            else
            {
                context.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Account", action = "Login" }));
                var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            }


        }

        #region Gerencia sessão
        private void GerenciaSesso(string ControllerAtual, string ActionAtual)
        {
            if (VerificaSeSessoExiste("PesquisaAgenda") && ( ControllerAtual != "GestaoAgenda" ||  (ActionAtual != "Index" && ActionAtual != "Novo" && ActionAtual != "Excluir")))
            {
                RemoverSessao("PesquisaAgenda");
            }

            if (VerificaSeSessoExiste("PesquisaConflito") && (ControllerAtual != "GestaoAgenda" || (ActionAtual != "ListaConflitos" && ActionAtual != "ResolverConflito")))
               
            {
                RemoverSessao("PesquisaConflito");
            }

        }
        public bool VerificaSeSessoExiste(string sessaoNome)
        {
            return !string.IsNullOrWhiteSpace(HttpContext.Session.GetString(sessaoNome));
        }
        public void AdicionaSesso<T>(string sessaoNome, T entidade)
        {
            HttpContext.Session.SetString(sessaoNome, JsonConvert.SerializeObject(entidade));
        }       
        public bool RemoverSessao(string sessaoNome)
        {
            HttpContext.Session.Remove(sessaoNome);
            return string.IsNullOrWhiteSpace(HttpContext.Session.GetString(sessaoNome));
        }
        public string RetonaSessaoPorNome(string sessaoNome)
        {
            return HttpContext.Session.GetString(sessaoNome);
        }

        #endregion
    }
}