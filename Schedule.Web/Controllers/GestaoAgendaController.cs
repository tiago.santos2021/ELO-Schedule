﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Interfaces.Services;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Application.ViewModel.Agenda;
using Schedule.Application.ViewModel.Agenda.Conflito;
using Schedule.Domain;
using Schedule.Web.Autorizacao;
using Schedule.Web.Controllers.Base;
using Schedule.Web.Helpers;
using Schedule.Web.Helpers.Html;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Schedule.Web.Controllers
{
    [Authorize]
    public class GestaoAgendaController : BaseController
    {
        private readonly IUsuarioService _usuarioService;
        private readonly IEmpresaService _empresaService;
        private readonly ICentroCustoService _centroCustoService;
        private readonly IPerfilService _perfilService;
        private IEspecialidadeService _especialidadeService;
        private readonly IAlocacaoService _alocacaoService;
        private const string Recurso = "Agenda";

        public GestaoAgendaController(IUsuarioService usuarioService, IEmpresaService empresaService, ICentroCustoService centroCustoService, IPerfilService perfilService, IEspecialidadeService especialidadeService, IAlocacaoService alocacaoService, IConfiguration Configuration)
        {
            _usuarioService = usuarioService;
            _empresaService = empresaService;
            _centroCustoService = centroCustoService;
            _perfilService = perfilService;
            _especialidadeService = especialidadeService;
            _alocacaoService = alocacaoService;
            this._configuration = Configuration;
        }

        #region metodos
        private List<UsuarioViewModel> RetornaListaConsultores(bool ativo = false)
        {
            var TipoConsultorId = Guid.Parse(this.RetornaParametro("TipoConsultorId"));
            var tipoGestaoComAlocacaoId = Guid.Parse(this.RetornaParametro("tipoGestaoComAlocacao"));
            var ListaConsultores = !ativo ? _usuarioService.ListarConsultores(new List<Guid>() { TipoConsultorId, tipoGestaoComAlocacaoId }) : _usuarioService.ListarConsultoresAtivos(new List<Guid>() { TipoConsultorId, tipoGestaoComAlocacaoId });
            return ListaConsultores;
        }


        private AgendaPesquisaViewModel PreencheValores(AgendaPesquisaViewModel SolicitacaoPesquisa)
        {

            SolicitacaoPesquisa.TodosConsultoresSelecionados = VerificaTodosConsultoresSelecionados(SolicitacaoPesquisa.ListaConsultoresId);
            SolicitacaoPesquisa.TodosCentroDeCustoSelecionados = VerificaTodosCentroDeCustoSelecionados(SolicitacaoPesquisa.ListaCentroCustos);
            SolicitacaoPesquisa.TipoCentroDeCustoInterno = Guid.Parse(this.RetornaParametro("TipoCentroDeCustoInterno"));


            return SolicitacaoPesquisa;

        }
        private void CarregaControlesPesquisa()
        {
            var empresas = _empresaService.Listar().OrderBy(x => x.Nome);
            ViewBag.Empresas = new SelectList(empresas, "id", "Nome", "Selecione");
            ViewBag.ListaConsultores = RetornaListaConsultores();
            var ListaEspecialidades = _especialidadeService.Listar().OrderBy(e => e.DescricaoEspecialidade).ToList();
            ViewBag.ListaEspecialidades = ListaEspecialidades;
            var centroCusto = _centroCustoService.Listar().OrderBy(x => x.Nome);
            ViewBag.TipoCentroCusto = new SelectList(centroCusto, "id", "Nome", "Selecione");

        }
        private void CarregaConsultorPesquisa()
        {
            List<UsuarioViewModel> ListaConsultores;
            if (Guid.Parse(this.RetornaParametro("TipoConsultorId")) == Guid.Parse(this.PerfilUsuarioLogado))
            {
                var usuario = new UsuarioViewModel() { id = Guid.Parse(this.IdentificadorUsuarioLogado), Nome = UsuarioLogado };
                ListaConsultores = new List<UsuarioViewModel>() { usuario };
                ViewBag.ListaConsultores = new SelectList(ListaConsultores, "id", "Nome", "Selecione");
                ViewBag.SelecionaConsultor = true;

            }
            else
            {
                ListaConsultores = RetornaListaConsultores();
                ViewBag.ListaConsultores = new SelectList(ListaConsultores, "id", "Nome", "Selecione");
            }
        }
        private bool VerificaTodosConsultoresSelecionados(List<Guid> ListaConsultoresSelecioandos)
        {
            bool resultado;

            if (Guid.Parse(this.RetornaParametro("TipoConsultorId")) == Guid.Parse(this.PerfilUsuarioLogado))
            {
                resultado = false;
            }
            else
            {
                var ListaConsultores = RetornaListaConsultores();
                resultado = ListaConsultores.Count() == ListaConsultoresSelecioandos.Count();
            }

            return resultado;
        }
        private bool VerificaTodosCentroDeCustoSelecionados(List<Guid> ListaCentroDeCustoSelecionados)
        {
            bool resultado;

            resultado = _centroCustoService.Listar().Count() == ListaCentroDeCustoSelecionados.Count();
            return resultado;
        }
        private void CarregaControlesFormulario(AlocacaoViewModel entidade = null)
        {
            var empresas = _empresaService.Listar().OrderBy(x => x.Nome);
            ViewBag.Empresas = new SelectList(empresas, "id", "Nome", "Selecione");
            ViewBag.ListaConsultores = this.CarregaConsultores(entidade != null ? entidade.ListaConsultoresId : null);
            var centroCusto = _centroCustoService.Listar().Where(x => x.Ativo == true).OrderBy(x => x.Nome);
            ViewBag.CentroCusto = new SelectList(centroCusto, "id", "Nome", "Selecione");
        }
        private List<ConsultorViewModel> CarregaConsultores(List<Guid> ConsultoresId = null)
        {
            List<ConsultorViewModel> lista = new List<ConsultorViewModel>();
            var ListaConsultores = RetornaListaConsultores();

            foreach (var item in ListaConsultores)
            {
                ConsultorViewModel consultor = new ConsultorViewModel();
                consultor.Nome = item.Nome;
                consultor.Id = (Guid)item.id;
                if (ConsultoresId != null && ConsultoresId.Count() > 0 && ConsultoresId.Contains(consultor.Id))
                {
                    consultor.Selecionado = true;
                }

                lista.Add(consultor);
            }
            return lista;
        }
        private AlocacaoViewModel PreencheCampos(AlocacaoViewModel entidade)
        {
            var empresa = _empresaService.Listar().Where(e => e.id == entidade.EmpresaId).FirstOrDefault();
            var centroCusto = _centroCustoService.Listar().Where(e => e.id == entidade.CentroCustoId).FirstOrDefault();

            //Preencher Listagem de centro de custo tipada
            if (entidade.ListaCentroCustos.Count > 0)
            {
                foreach (var cc in entidade.ListaCentroCustos)
                {
                    if (cc.ToString() != "00000000-0000-0000-0000-000000000000") //Código guid para RESERVA criado somente para exclusão de agenda.
                    {
                        var centroCustoDescricao = _centroCustoService.Listar().Where(e => e.id == cc).FirstOrDefault();
                        entidade.ListaCentroDeCustos.Add(new CentroDeCustoViewModel() { CentroCustoId = cc, CentroCustoDescricao = centroCustoDescricao.Nome });
                    }
                }
            }

            var ListaConsultores = RetornaListaConsultores();

            if (centroCusto != null)
            {
                entidade.CentroCustoDescricao = centroCusto.Nome;
            }

            if (empresa != null)
            {
                entidade.EmpresaDescricao = empresa.Nome;
            }

            entidade.DataInicio = Convert.ToDateTime(entidade.DataIniciotexto);
            entidade.DataFim = Convert.ToDateTime(entidade.DataFimtexto);

            foreach (var item in entidade.ListaConsultoresId)
            {
                var consultor = ListaConsultores.Where(x => x.id == item).FirstOrDefault();
                entidade.Consultores.Add(new ConsultorViewModel() { Id = item, Nome = consultor.Nome });
            }

            return entidade;
        }
        private void HabilitaInclusaoExclusaoAgenda()
        {      
            List<string> listaPerfis = new List<string>();
            listaPerfis.Add(this.RetornaParametro("TipoAcessoTotal"));
            listaPerfis.Add(this.RetornaParametro("PerfilDesenvolvedor"));
            listaPerfis.Add(this.RetornaParametro("PerfilGestaoComAlocacao"));

            if (listaPerfis.Any(x=> x.ToLower() == this.PerfilUsuarioLogado.ToLower()))
            {
                ViewBag.ExibiBotoes = true;
            }

        }
        private List<int> RetornaListaDiasSemanaAlocacao(AlocacaoViewModel entidade)
        {
            List<int> ListaDiasSemana = new List<int>();

            if (entidade.Segunda == true)
            {
                ListaDiasSemana.Add(1);
            }
            if (entidade.Terca == true)
            {
                ListaDiasSemana.Add(2);
            }
            if (entidade.Quarta == true)
            {
                ListaDiasSemana.Add(3);
            }
            if (entidade.Quinta == true)
            {
                ListaDiasSemana.Add(4);
            }
            if (entidade.Sexta == true)
            {
                ListaDiasSemana.Add(5);
            }
            if (entidade.Sabado == true)
            {
                ListaDiasSemana.Add(6);
            }
            if (entidade.Domingo == true)
            {
                ListaDiasSemana.Add(0);
            }


            return ListaDiasSemana;
        }

        #endregion


        #region Gestão de Alocação
        [ClaimRequirement(Recurso, "Listar")]
        public IActionResult Index(string mensagem)
        {
            AgendaPesquisaViewModel model = new AgendaPesquisaViewModel { Pesquisar = false };



            if (!string.IsNullOrEmpty(mensagem) && mensagem == "Sucesso")
            {
                TempData["success"] = ScheduleResorce.RetornaValor("MensagemSucesso");
            }

            if (VerificaSeSessoExiste("PesquisaAgenda"))
            {
                var Pesquisa = JsonConvert.DeserializeObject<AgendaPesquisaViewModel>(RetonaSessaoPorNome("PesquisaAgenda"));
                
                    model = Pesquisa;
                    model.Pesquisar = true;
                    RemoverSessao("PesquisaAgenda");
            }


            this.CarregaControlesPesquisa();
            this.CarregaConsultorPesquisa();
            this.HabilitaInclusaoExclusaoAgenda();


            return View(model);
        }

        [HttpPost]
        [ClaimRequirement(Recurso, "Listar")]
        public IActionResult Index(AgendaPesquisaViewModel SolicitacaoPesquisa)
        {
            string Resultado = "";
            try
            {
                var retorno = _alocacaoService.PesquisarAlocacao(this.PreencheValores(SolicitacaoPesquisa));
                if (retorno.Sucesso == true)
                {
                    Resultado = TabelaAlocacaoHtmlGenerator.GerarTabelaHtml(retorno);
                    AdicionaSesso("PesquisaAgenda", SolicitacaoPesquisa);
                }
                return Json(new { success = retorno.Sucesso, mensagem = retorno.Mensagem, erro = "", data = Resultado });
            }
            catch (DomainException ex)
            {
                return Json(new { success = false, mensagem = "", erro = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, mensagem = "", erro = ex.Message });
            }
        }

        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo()
        {
            this.CarregaControlesFormulario(null);
            return View();
        }

        [HttpPost]
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo(AlocacaoViewModel entidade)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    entidade.ListaDiasSemana = this.RetornaListaDiasSemanaAlocacao(entidade);
                    _ = entidade.Feriado == true ? entidade.DataFimtexto = entidade.DataIniciotexto : "";
                    _alocacaoService.ValidarInclusao(entidade);
                    entidade.Usuario = this.UsuarioLogado;
                    entidade = PreencheCampos(entidade);
                    var retorno = _alocacaoService.Incluir(entidade);
                   
                    return Json(new { success = true, erro = "" });



                }
                catch (DomainException ex)
                {
                    return Json(new { success = false, erro = ex.Message });
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, erro = ex.Message });
                }
            }
            return Json(new { success = true, erro = "" });
        }


        [ClaimRequirement(Recurso, "Excluir")]
        public IActionResult Excluir()
        {
            this.CarregaControlesFormulario(null);
            return View();
        }

        [HttpPost]
        [ClaimRequirement(Recurso, "Excluir")]
        public IActionResult Excluir(AlocacaoViewModel entidade)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _alocacaoService.ValidarExclusao(entidade);
                    entidade.Usuario = this.UsuarioLogado;
                    entidade = PreencheCampos(entidade);
                    var retorno = _alocacaoService.Deletar(entidade);
                  
                    return Json(new { success = true, erro = "" });
                }
                catch (DomainException ex)
                {
                    return Json(new { success = false, erro = ex.Message });
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, erro = ex.Message });
                }
            }
            return Json(new { success = true, erro = "" });
        }

        #endregion


        #region Conflitos

        [ClaimRequirement(Recurso, "Resolver Conflito")]
        public IActionResult ListaConflitos(string mensagem)
        {
            if (!string.IsNullOrEmpty(mensagem) && mensagem == "Sucesso")
            {
                TempData["success"] = ScheduleResorce.RetornaValor("MensagemSucesso");
            }

            PesquisaConflitoViewModel model = new PesquisaConflitoViewModel { Pesquisar = false };
         
            
            if (VerificaSeSessoExiste("PesquisaConflito"))
            {
                var Pesquisa = JsonConvert.DeserializeObject<PesquisaConflitoViewModel>(RetonaSessaoPorNome("PesquisaConflito"));
               
                    model = Pesquisa;
                    model.Pesquisar = true;
                    RemoverSessao("PesquisaConflito");
               

            }


            this.CarregaControlesPesquisa();
            this.CarregaConsultorPesquisa();
            return View(model);
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Resolver Conflito")]
        public IActionResult ListaConflitos(PesquisaConflitoViewModel SolicitacaoPesquisa)
        {

            try
            {
                var retorno = _alocacaoService.ListaConsultoresConflitos(SolicitacaoPesquisa);
                if (retorno.Sucesso == true)
                {
                    retorno.Html = TabelaConflitosHtmlGenerator.GerarTabelaHtml(retorno);
                    AdicionaSesso("PesquisaConflito", SolicitacaoPesquisa);
                }

                return Json(new { success = retorno.Sucesso, mensagem = retorno.Mensagem, erro = "", data = retorno });
            }
            catch (DomainException ex)
            {
                return Json(new { success = false, mensagem = "", erro = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, mensagem = "", erro = ex.Message });
            }
        }

        [HttpPost]
        [ClaimRequirement(Recurso, "Resolver Conflito")]
        public IActionResult ResolverConflito(ResolverConflitoViewModel data)
        {
            try
            {
                if (data != null)
                {
                    var lista = data.ListaExclusao.Select(x => (Guid)x.id).Distinct();
                    var retorno = _alocacaoService.ExcluirConflitos(lista.ToList());


                    return Json(new { success = true, mensagem = "", erro = "", data = retorno });
                }
                else
                {
                    return Json(new { success = false, mensagem = "", erro = "Selecione uma quantidade menor de itens." });
                }
            }
            catch (DomainException ex)
            {
                return Json(new { success = false, mensagem = "", erro = ex.Message });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, mensagem = "", erro = ex.Message });
            }
        }
        [ClaimRequirement(Recurso, "Resolver Conflito")]
        public IActionResult ResolverConflito(Guid id, DateTime DataInicial, DateTime DataFinal, string mensagem)
        {
            if (!string.IsNullOrEmpty(mensagem) && mensagem == "Sucesso")
            {
                TempData["success"] = ScheduleResorce.RetornaValor("MensagemSucesso");
            }
            var retorno = _alocacaoService.ListaConflitosPorConsultor(id, DataInicial, DataFinal);

            if (retorno != null)
            {
                retorno.DataFinal = DataFinal;
                retorno.DataInicial = DataInicial;
                retorno.UsuarioId = id;
                return View(retorno);
            }
            else
            {
                return RedirectToAction("ListaConflitos");

            }


        }
        #endregion


    }
}
