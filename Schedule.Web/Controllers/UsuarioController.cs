﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Interfaces.Services.Resorces;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Web.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Schedule.Web.Autorizacao;
using Schedule.Util.Criptografia;
using Schedule.Util.GeradorExcel;
using Schedule.Domain.Entities.Estrutura;

namespace Schedule.Web.Controllers
{
    [Authorize]
    public class UsuarioController : BaseController
    {
        private const string Recurso = "Usuario";
        private readonly IUsuarioService _usuarioService;
        private readonly IBaseUFService _baseUFService;
        private readonly IEmpresaService _empresaService;
        private readonly ISenioridadeService _senioridadeService;
        private readonly ITipoContratacaoService _tipoContratacaoService;
        private readonly IPerfilService _perfilService;
        private readonly IEspecialidadeService _especialidadeService;
        private readonly IMapper _mapper;

        
        public UsuarioController(IUsuarioService usuarioService, IBaseUFService baseUFService, IEmpresaService empresaService,
            ISenioridadeService senioridadeService, ITipoContratacaoService tipoContratacaoService, IConfiguration Configuration,
            IPerfilService perfilService, IEspecialidadeService especialidadeService,
        IMapper mapper)
        {
            _usuarioService = usuarioService;
            _mapper = mapper;
            _baseUFService = baseUFService;
            _empresaService = empresaService;
            _senioridadeService = senioridadeService;
            _tipoContratacaoService = tipoContratacaoService;
            _perfilService = perfilService;
            _especialidadeService = especialidadeService;
            _configuration = Configuration;
        }
       
        private void CarregaControles()
        {
            var estados = _baseUFService.Listar();
            ViewBag.Estados = new SelectList(estados, "Sigla", "Descricao", "Selecione");
            var empresas = _empresaService.Listar();
            ViewBag.Empresas = new SelectList(empresas, "id", "Nome", "Selecione");
            var Senioridade = _senioridadeService.Listar();
            ViewBag.Senioridade = new SelectList(Senioridade, "id", "Nome", "Selecione");
            var TipoContratacao = _tipoContratacaoService.Listar();
            ViewBag.TipoContratacao = new SelectList(TipoContratacao, "id", "Nome", "Selecione");
            var Tipoperfil = _perfilService.Listar();
            ViewBag.Tipoperfil = new SelectList(Tipoperfil, "id", "Nome", "Selecione");
           
            ViewBag.TipoConsultorId = _configuration.GetSection("parametros")["TipoConsultorId"];
            ViewBag.tipoGestaoComAlocacaoId = this.RetornaParametro("tipoGestaoComAlocacao") ;

            var ListaGestores = _usuarioService.ListarGestores(new List<Guid>() { Guid.Parse(this.RetornaParametro("TipoGestorId")), Guid.Parse(this.RetornaParametro("tipoGestaoComAlocacao")) });
            ViewBag.ListaGestores = new SelectList(ListaGestores, "id", "Nome", "Selecione");

        }
        private void CarregaControles(UsuarioViewModel entidade)
        {
            var estados = _baseUFService.Listar();
            ViewBag.Estados = new SelectList(estados, "Sigla", "Descricao", entidade.UF);
            var empresas = _empresaService.Listar();
            ViewBag.Empresas = new SelectList(empresas, "id", "Nome", entidade.EmpresaId);
            var Senioridade = _senioridadeService.Listar();
            ViewBag.Senioridade = new SelectList(Senioridade, "id", "Nome", entidade.SenioridadeId);
            var TipoContratacao = _tipoContratacaoService.Listar();
            ViewBag.TipoContratacao = new SelectList(TipoContratacao, "id", "Nome", entidade.TipoContratacaoId);
            var Tipoperfil = _perfilService.Listar();
            ViewBag.Tipoperfil = new SelectList(Tipoperfil, "id", "Nome", entidade.PerfilId);
  
            ViewBag.TipoConsultorId = this.RetornaParametro("TipoConsultorId");
            ViewBag.tipoGestaoComAlocacaoId = this.RetornaParametro("tipoGestaoComAlocacao"); 
            var ListaGestores = _usuarioService.ListarGestores(new List<Guid>() { Guid.Parse(this.RetornaParametro("TipoGestorId")), Guid.Parse(this.RetornaParametro("tipoGestaoComAlocacao")) });
            ViewBag.ListaGestores = new SelectList(ListaGestores, "id", "Nome", entidade.GestorImediatoId);

        }
        private void CarregaEspecialidades(List<Guid> ListaEspecialdiadesSelecionadas = null)
        {
            var ListaEspecialidades = _especialidadeService.Listar().OrderBy(e => e.DescricaoEspecialidade);
            if (ListaEspecialdiadesSelecionadas != null && ListaEspecialdiadesSelecionadas.Count() > 0)
            {
                foreach (var item in ListaEspecialidades)
                {
                    if (ListaEspecialdiadesSelecionadas.Contains((Guid)item.id))
                    {
                        item.Selecionada = true;
                    }
                }
            }


            ViewBag.ListaEspecialidades = ListaEspecialidades;

        }
        private bool VerificaTipoUusuario(UsuarioViewModel entidade)
        {
            return Guid.Parse(this.RetornaParametro("TipoConsultorId")) == entidade.PerfilId 
                || Guid.Parse(this.RetornaParametro("tipoGestaoComAlocacao")) == entidade.PerfilId;
        }      
        [ClaimRequirement(Recurso, "Listar")]
        public IActionResult Index()
        {
            var lista = _usuarioService.ListaUsuarioDetalhes().OrderBy(x => x.DataAlteracao).ToList(); 
            lista = BindOrdenacaoData<UsuarioViewModel>(lista);
            return View(lista);
        }
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo()
        {
            ViewBag.TipoUsuario = "None";
            this.CarregaControles();
            this.CarregaEspecialidades();
            return View();
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo(UsuarioViewModel entidade)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    entidade.Usuario = this.UsuarioLogado;
                    entidade.Senha = this.RetornaParametro("SenhaPadrao");
                    _usuarioService.Adicionar(entidade, this.VerificaTipoUusuario(entidade));
                    TempData["success"] = ScheduleResorce.RetornaValor("MensagemSucesso");
                    return RedirectToAction("Index");
                }
                catch (DomainException ex)
                {

                    ModelState.AddModelError("", ex.Message);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;

                }
            }
            ViewBag.TipoUsuario = this.VerificaTipoUusuario(entidade) == true ? "block" : "none";
            this.CarregaControles();
            this.CarregaEspecialidades(entidade.ListaEspecialidadesId);
           

            return View(entidade);
        }
        [ClaimRequirement(Recurso, "Editar")]
        public IActionResult Editar(Guid id)
        {
            if (!_usuarioService.VerificaSeRegistroExisteNaBase(id))
            {
                TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                return RedirectToAction("Index");
            }

            var entidade = _usuarioService.RetornaPorId(id);
            ViewBag.TipoUsuario = this.VerificaTipoUusuario(entidade) == true ? "block" : "none";
            
            this.CarregaControles(entidade);
            this.CarregaEspecialidades(entidade.ListaEspecialidadesId);
            
            return View(entidade);
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Editar")]
        public IActionResult Editar(UsuarioViewModel entidade)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!_usuarioService.VerificaSeRegistroExisteNaBase((Guid)entidade.id))
                    {
                        TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                    }
                    else
                    {
                        entidade.Usuario = this.UsuarioLogado;
                        _usuarioService.Alterar(entidade, this.VerificaTipoUusuario(entidade));
                        TempData["success"] = "Operação Realizada Com Sucesso!";
                    }
                 
                    return RedirectToAction("Index");
                }
            }
            catch (DomainException ex)
            {

                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;

            }
            ViewBag.TipoUsuario = this.VerificaTipoUusuario(entidade) == true ? "block" : "none";
            this.CarregaControles(entidade);
            this.CarregaEspecialidades(entidade.ListaEspecialidadesId);
            return View(entidade);
        }
        [ClaimRequirement(Recurso, "Excluir")]
        public IActionResult DesativarUsuario(Guid id)
        {

            try
            {
                if (!_usuarioService.VerificaSeRegistroExisteNaBase(id))
                {
                    TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                }
                else
                {
                    _usuarioService.DesativarUsuario(id, this.UsuarioLogado);
                    TempData["success"] = "Operação Realizada Com Sucesso!";
                }
            }
            catch (DomainException ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
            }
            return RedirectToAction("Index");

        }

        [HttpGet]
        public FileResult GerarExcel()
        {
            var lista = _usuarioService.ListaUsuarioDetalhes().OrderBy(x => x.DataAlteracao)
                .Select( x=> new UsuarioDetalhes() {
                   Empresa = x.EmpresaDescricao,
                   Login =  x.Login,
                   Nome = x.Nome,
                   Perfil = x.PerfilDescricao,
                   Status = x.Ativo == true ? "Ativo" : "Não ativo",
                   Especialidade = x.Especialidade,
                   Senioridade = x.Senioridade,
                   Contratacao = x.TipoContratacao,
                   DataInicioContrato = x.DataInicioContrato,
                   DataTerminoContrato = x.DataTerminoContrato,
                   Observacao = x.Observacao,
                  
                   
                }).ToList();

            var fileBytes = GeradorExcel.RelatorioUsuarios<UsuarioDetalhes>(lista, "fileName");

            return File(fileBytes, "application/xlsx", "fileName.xlsx");

        }



    }
}
