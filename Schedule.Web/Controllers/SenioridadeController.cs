﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Schedule.Application.Interfaces.Service;
using Schedule.Application.Resouces;
using Schedule.Application.ViewModel;
using Schedule.Domain;
using Schedule.Web.Autorizacao;
using Schedule.Web.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Schedule.Web.Controllers
{
    [Authorize]
    public class SenioridadeController : BaseController
    {
        private const string Recurso = "Senioridade";
        private readonly ISenioridadeService _senioridadeService;
        private readonly IMapper mapper;
        public SenioridadeController(ISenioridadeService senioridadeService, IMapper _mapper)

        {
            _senioridadeService = senioridadeService;
            mapper = _mapper;
        }
        [ClaimRequirement(Recurso, "Listar")]
        public IActionResult Index()
        {
          
            var lista = _senioridadeService.Listar().OrderBy(x => x.DataAlteracao).ToList(); ;
            lista = BindOrdenacaoData<SenioridadeViewModel>(lista);
            return View(lista);
        }
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo()
        {
            return View();
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Cadastrar")]
        public IActionResult Novo(SenioridadeViewModel entidade)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    entidade.Usuario = this.UsuarioLogado;
                    _senioridadeService.Adicionar(entidade);
                    TempData["success"] = ScheduleResorce.RetornaValor("MensagemSucesso");
                    return RedirectToAction("Index");
                }
                catch (DomainException ex)
                {

                    ModelState.AddModelError("", ex.Message);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;

                }
            }

            return View();
        }

        [ClaimRequirement(Recurso, "Editar")]
        public IActionResult Editar(Guid id)
        {
            if (!_senioridadeService.VerificaSeRegistroExisteNaBase(id))
            {
                TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                return RedirectToAction("Index");
            }
            var entidade = _senioridadeService.RetornaPorId(id);
            return View(entidade);
        }
        [HttpPost]
        [ClaimRequirement(Recurso, "Editar")]
        public IActionResult Editar(SenioridadeViewModel entidade)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!_senioridadeService.VerificaSeRegistroExisteNaBase((Guid)entidade.id))
                    {
                        TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                    }
                    else
                    {
                        entidade.Usuario = this.UsuarioLogado;
                        _senioridadeService.Alterar(entidade);
                        TempData["success"] = "Operação Realizada Com Sucesso!";
                    }
                    return RedirectToAction("Index");
                }
            }
            catch (DomainException ex)
            {

                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;

            }
            return View(entidade);
        }
        [ClaimRequirement(Recurso, "Excluir")]
        public IActionResult Excluir(Guid id)
        {
            try
            {
                if (!_senioridadeService.VerificaSeRegistroExisteNaBase(id))
                {
                    TempData["warning"] = ScheduleResorce.RetornaValor("RegistroNaoEncontrado");
                }
                else
                {
                    _senioridadeService.Excluir(id);
                    TempData["success"] = "Operação Realizada Com Sucesso!";
                }
            }
            catch (DomainException ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
            }
            return RedirectToAction("Index");
        }


    }
}
