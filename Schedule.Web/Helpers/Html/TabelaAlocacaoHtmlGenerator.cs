﻿using Schedule.Application.ViewModel.Agenda;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.Web.Helpers.Html
{
    public static class TabelaAlocacaoHtmlGenerator
    {
        public static string GerarTabelaHtml(PlanejamentoAlocacaoViewModel data)
        {
            var cabecalho = GerarCabecalho(data);
            var corpo = GerarCorpo(data);
            return cabecalho + corpo;

        }
        private static string GerarCabecalho(PlanejamentoAlocacaoViewModel data)
        {
            var retorno = string.Format(@"<thead style='top: 0'><tr> 
                                        <th class='thScheduleConsultor bg-dark text-white' >
                                           Consultor
                                         </th>
                                        <th class='thScheduleEspecialidade bg-dark text-white' style=' left: 0'>
                                            Especialidade
                                        </th>
                                        <th class='thSchedulePeriodo bg-dark text-white' style=' left: 0'>
                                            Período
                                        </th>
                                        <th class='thScheduleDiaSemana bg-dark text-white' style=' left: 0'>
                                            Dia
                                        </th> 
                                       {0 } </ tr></thead>", RetornaCabechalho(data));

            return retorno;
        }
        private static string RetornaCabechalho(PlanejamentoAlocacaoViewModel data)
        {
            //Adicona + um 2 para exibir a primeira Segunda do Período
            DateTime dataInicio = data.DataInicioCabecalho.AddDays(2);
            DateTime dataFinal = data.DataTerminoCabecalho.AddDays(2);
            StringBuilder cabecalho = new StringBuilder();
            while (dataInicio < dataFinal)
            {
                cabecalho.Append(string.Format("<th class='tdSchedulepadrao bg-dark text-white'>{0} </ th >", AgendaHelper.FormataDataCabecalho(@dataInicio)));
                dataInicio = dataInicio.AddDays(7);
            }

            return cabecalho.ToString();
        }
        private static string GerarCelulaConsultor(AlocacaoAgendaViewModel alocacao)
        {
            var celula = "";
            if(alocacao.PossuiConflito)
            {
                celula =  string.Format($@"<td data-toggle='tooltip' data-placement='top' title=' Atenção..Consultor Com Conflitos de Agenda' class=' {{1}} {{2}}' style=' left: 0' > *{{3}} </td>",
              alocacao.ConsultorNome, alocacao.ClasseEstiloTabela, alocacao.ClasseEstiloConflito, alocacao.ConsultorNomeFormatado);
            }
            else
            {
                celula = string.Format($@"<td data-toggle='tooltip' data-placement='top' title=' {{0}}' class=' {{1}}' style=' left: 0' > {{2}} </td>",
              alocacao.ConsultorNome, alocacao.ClasseEstiloTabela, alocacao.ConsultorNomeFormatado);
            }
                
              
            return celula;
        }
        private static string GerarCelulaEspecialidade(AlocacaoAgendaViewModel alocacao)
        {
            var celula = string.Format($@"<td data-toggle='tooltip' data-placement='top' title=' {{0}}' class=' {{1}}' style=' left: 0' > {{2}}</td>",
                alocacao.Especialidade, alocacao.ClasseEstiloTabela, alocacao.EspecialidadeFormatado);
            return celula;
        }
        private static string GerarCelulaPeriodo(AlocacaoAgendaViewModel alocacao, int Periodo)
        {
            var celula = string.Format($@"<td  class=' {{0}}' style=' left: 0' >  {{1}} </td>", alocacao.ClasseEstiloTabela, Periodo);
            return celula;
        }
        private static string GerarCelulaDia(AlocacaoAgendaViewModel alocacao, string DiaSemana)
        {
            var celula = string.Format($@"<td  class=' {{0}}' style=' left: 0' > {{1}} </td>", alocacao.ClasseEstiloTabela, DiaSemana);
            return celula;
        }
        private static string GerarLinha(AlocacaoAgendaViewModel alocacao, List<AgendaViewModel> lista, int Periodo, string Dia)
        {
            StringBuilder Linha = new StringBuilder();
            Linha.Append("<tr>");
            Linha.Append(GerarCelulaConsultor(alocacao));
            Linha.Append(GerarCelulaEspecialidade(alocacao));
            Linha.Append(GerarCelulaPeriodo(alocacao, Periodo));
            Linha.Append(GerarCelulaDia(alocacao, Dia));
            foreach (var item in lista)
            {
                var celula = string.Format($@"<td class='tdSchedulepadrao {{0}}'> {{1}}</td>", item.Classe, item.ValorExibicao);
                Linha.Append(celula);
            }

            Linha.Append("</tr>");

            return Linha.ToString();
        }
        private static string GerarCorpo(PlanejamentoAlocacaoViewModel data)
        {
            StringBuilder Corpo = new StringBuilder();
            Corpo.Append("<tbody>");

            foreach (var item in data.Alocacao)
            {
                Corpo.Append(GerarLinha(item, item.ListaSabados1, 1, "Sáb"));
                Corpo.Append(GerarLinha(item, item.ListaSabados2, 2, "Sáb"));

                Corpo.Append(GerarLinha(item, item.ListaDomingos1, 1, "Dom"));
                Corpo.Append(GerarLinha(item, item.ListaDomingos1, 2, "Dom"));

                Corpo.Append(GerarLinha(item, item.ListaSegundas1, 1, "Seg"));
                Corpo.Append(GerarLinha(item, item.ListaSegundas2, 2, "Seg"));

                Corpo.Append(GerarLinha(item, item.ListaTercas1, 1, "Ter"));
                Corpo.Append(GerarLinha(item, item.ListaTercas2, 2, "Ter"));

                Corpo.Append(GerarLinha(item, item.ListaQuartas1, 1, "Qua"));
                Corpo.Append(GerarLinha(item, item.ListaQuartas2, 2, "Qua"));

                Corpo.Append(GerarLinha(item, item.ListaQuintas1, 1, "Qui"));
                Corpo.Append(GerarLinha(item, item.ListaQuintas2, 2, "Qui"));

                Corpo.Append(GerarLinha(item, item.ListaSextas1, 1, "Sex"));
                Corpo.Append(GerarLinha(item, item.ListaSextas2, 2, "Sex"));
            }

            Corpo.Append("</tbody>");
            return Corpo.ToString();

        }

    }
}




