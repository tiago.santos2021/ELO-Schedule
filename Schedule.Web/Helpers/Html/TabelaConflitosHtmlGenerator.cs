﻿using Schedule.Application.ViewModel;
using Schedule.Application.ViewModel.Agenda.Conflito;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schedule.Web.Helpers.Html
{
    public static class TabelaConflitosHtmlGenerator
    {
        public static string GerarTabelaHtml(RespostaPesquisaConflitoViewModel data)
        {
            var cabecalho = GerarCabecalho();
            var corpo = GerarCorpo(data.UsuariosConflitos);
            return cabecalho + corpo;
        }
        private static string GerarCabecalho()
        {
            var retorno = string.Format(@"<thead>
                        <tr>
                            <td class='bg-secondary text-white'>Consultor</td>
                            <td class='bg-secondary text-white'>Responsável</td>
                            <td class='bg-secondary text-white'> </td>
                        </tr>
                    </thead>");

            return retorno;
        }
        private static string GerarCorpo(List<UsuarioViewModel> data)
        {
            StringBuilder Corpo = new StringBuilder();
            Corpo.Append("<tbody>");
            foreach (var item in data)
            {
                Corpo.Append(GerarLinha(item));

            }
            Corpo.Append("</tbody>");
            return Corpo.ToString();
        }
        private static string GerarLinha(UsuarioViewModel usuario)
        {
            StringBuilder Linha = new StringBuilder();
            Linha.Append("<tr>");
            var celula = string.Format($@"<td> {{0}}</td><td> {{1}}</td> {{2}}", usuario.Nome, usuario.GestorImediatoNome, GerarCelulaBotao(usuario));
            Linha.Append(celula);
            Linha.Append("</tr>");
            return Linha.ToString();
        }
        private static string GerarCelulaBotao(UsuarioViewModel usuario)
        {
            var celula = string.Format($@" <td><button data-id='{{0}}' class='ResolverConflito btn btn-xs btn-danger'>Resolver Conflito</button></td>", usuario.id);
            return celula;
        }
    }
}
