﻿using System;

namespace Schedule.Web.Helpers
{
    public class AgendaHelper
    {

        public static string FormataDataCabecalho(DateTime data)
        {

            string Dia = data.Day.ToString("D2");

            return Dia.ToString() + "-" + RetornaMesPorExtenso(data.Month);

        }
        private static string RetornaMesPorExtenso(int Mes)
        {
            var retorno = "";
            switch (Mes)
            {
                case 1:
                    retorno   = "Janeiro";
                    break;
                case 2:
                    retorno = "Fevereiro";
                    break;
                case 3:
                    retorno = "Março";
                    break;

                case 4:
                    retorno = "Abril";
                    break;
                case 5:
                    retorno = "Maio";
                    break;

                case 6:
                    retorno = "Junho";
                    break;
                case 7:
                    retorno = "Julho";
                    break;

                case 8:
                    retorno = "Agosto";
                    break;
                case 9:
                    retorno = "Setembro";
                    break;

                case 10:
                    retorno = "Outubro";
                    break;
                case 11:
                    retorno = "Novembro";
                    break;

                case 12:
                    retorno = "Dezembro";
                    break;


            }
            return retorno;


        }

    }
}
