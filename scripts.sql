
--1 Criação tabela feriado, inserts, no menu, e perfil menu-
create table Feriado (
	Id UNIQUEIDENTIFIER DEFAULT NEWID() PRIMARY KEY ,
	Descricao varchar(50),
	DataFeriado Datetime not null,
	Usuario varchar(100),
	DataAlteracao datetime not null
)


declare @idMenu UNIQUEIDENTIFIER
set @idMenu = newid()

INSERT INTO [dbo].[Menu] (
           [id]
		   ,[Titulo]
           ,[Descricao]
           ,[Controller]
           ,[Action]
           ,[Ordem]
           ,[SubOrdem]
           ,[Usuario]
           ,[DataAlteracao])
     VALUES
	 (
		 @idMenu, 
		'Feriados',
		'Cadastro de Feriados',
		'Feriados',
		'Index',
		4,
		1,
		'Teste',
		GETDATE()
	 )
--dar acesso apenas para adm
declare @idPerfil UNIQUEIDENTIFIER 
select @idPerfil = id from Perfil where Grupo = 02

INSERT INTO [dbo].[PerfilMenu]
           ([id]
           ,[Perfilid]
           ,[MenuId]
           ,[Usuario]
           ,[DataAlteracao])
     VALUES
           (
		   newid(),
		   @idPerfil,
		   @idMenu,
		   'Teste',
		   GETDATE())



--select * from menu

INSERT INTO [dbo].[Feriado]
           ([Id]
           ,[Descricao]
           ,[DataFeriado]
           ,[Usuario]
           ,[DataAlteracao])
     VALUES
           (
   newid(),
'pascoa' ,
'2021-03-21' ,
 'Teste'  ,
'44670'    )

INSERT INTO [dbo].[Feriado] ([Id],[Descricao],[DataFeriado],[Usuario],[DataAlteracao]) VALUES ( newid(), 'Tiradentes', '2022-04-21' ,'Teste' ,'2022-04-19' )
INSERT INTO [dbo].[Feriado] ([Id],[Descricao],[DataFeriado],[Usuario],[DataAlteracao]) VALUES ( newid(), 'Dia do Trabalho', '2022-05-01' ,'Teste' ,'2022-04-19' )
INSERT INTO [dbo].[Feriado] ([Id],[Descricao],[DataFeriado],[Usuario],[DataAlteracao]) VALUES ( newid(), 'Independência', '2022-09-07' ,'Teste' ,'2022-04-19' )
INSERT INTO [dbo].[Feriado] ([Id],[Descricao],[DataFeriado],[Usuario],[DataAlteracao]) VALUES ( newid(), 'Nossa Senhora Aparecida', '2022-10-12' ,'Teste' ,'2022-04-19' )
INSERT INTO [dbo].[Feriado] ([Id],[Descricao],[DataFeriado],[Usuario],[DataAlteracao]) VALUES ( newid(), 'Finados', '2022-11-02' ,'Teste' ,'2022-04-19' )
INSERT INTO [dbo].[Feriado] ([Id],[Descricao],[DataFeriado],[Usuario],[DataAlteracao]) VALUES ( newid(), 'Proclamação da República', '2022-11-15' ,'Teste' ,'2022-04-19' )
INSERT INTO [dbo].[Feriado] ([Id],[Descricao],[DataFeriado],[Usuario],[DataAlteracao]) VALUES ( newid(), 'Dia da Conciência Negra', '2022-11-20' ,'Teste' ,'2022-04-19' )
INSERT INTO [dbo].[Feriado] ([Id],[Descricao],[DataFeriado],[Usuario],[DataAlteracao]) VALUES ( newid(), 'Natal', '2022-12-15' ,'Teste' ,'2022-04-19' )

declare @idPerfilADM UNIQUEIDENTIFIER 
select @idPerfilADM = id from Perfil where Grupo = 02

declare @idPermissaoCadastrar UNIQUEIDENTIFIER = newid()
declare @idPermissaoListar    UNIQUEIDENTIFIER = newid()
declare @idPermissaoExcluir   UNIQUEIDENTIFIER = newid()

--SELECT * FROM Permissao

INSERT INTO [dbo].[Permissao]([id],[Tipo],[Descricao],[Usuario],[DataAlteracao]) VALUES( @idPermissaoCadastrar, 'Feriados','Cadastrar','ADM', GETDATE() )
INSERT INTO [dbo].[Permissao]([id],[Tipo],[Descricao],[Usuario],[DataAlteracao]) VALUES( @idPermissaoListar, 'Feriados','Listar','ADM', GETDATE() )
INSERT INTO [dbo].[Permissao]([id],[Tipo],[Descricao],[Usuario],[DataAlteracao]) VALUES( @idPermissaoExcluir, 'Feriados','Excluir','ADM', GETDATE() )

--select *from [dbo].[PerfilPermissao]


INSERT INTO [dbo].[PerfilPermissao](id, Perfilid, PermissaoId, Usuario, DataAlteracao) VALUES(newid(),@idPerfilADM,@idPermissaoCadastrar, 'ADM',GETDATE())
INSERT INTO [dbo].[PerfilPermissao](id, Perfilid, PermissaoId, Usuario, DataAlteracao) VALUES(newid(),@idPerfilADM,@idPermissaoListar, 'ADM',GETDATE())
INSERT INTO [dbo].[PerfilPermissao](id, Perfilid, PermissaoId, Usuario, DataAlteracao) VALUES(newid(),@idPerfilADM,@idPermissaoExcluir, 'ADM',GETDATE())


--BEGIN TRAN
--update Permissao set tipo = 'Feriados' where Tipo = 'Feriado'

--commit

--2 criacao do menu templates

declare @idMenuTemplate UNIQUEIDENTIFIER
set @idMenuTemplate = newid()

INSERT INTO [dbo].[Menu] (
           [id]
		   ,[Titulo]
           ,[Descricao]
           ,[Controller]
           ,[Action]
           ,[Ordem]
           ,[SubOrdem]
           ,[Usuario]
           ,[DataAlteracao])
     VALUES
	 (
		 @idMenuTemplate, 
		'Templates',
		'Templates de Relatórios',
		'Visao',
		'Templates',
		2,
		2,
		'Teste',
		GETDATE()
	 )
--dar acesso apenas para adm
declare @idPerfilADMT UNIQUEIDENTIFIER 
select @idPerfilADMT = id from Perfil where Grupo = 02

INSERT INTO [dbo].[PerfilMenu]
           ([id]
           ,[Perfilid]
           ,[MenuId]
           ,[Usuario]
           ,[DataAlteracao])
     VALUES
           (
		   newid(),
		   @idPerfilADMT,
		   @idMenuTemplate,
		   'Teste',
		   GETDATE())


select * from Menu order by ordem


begin tran

update menu set subordem = 0 where id = '96B60B87-B398-4C5B-B340-5D67582458AB'

commit


--sp_help menu

--use db_schedule

--select * from usuario where Nome = '449977705b5208d07a4ad68da3ff6ac5'

select * from menu order by Ordem, subordem


